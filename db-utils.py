import getopt
import sys
from datetime import datetime

from lib.utility.constants import CREATE, DROP, DROP_CREATE
from lib.db.sqlite_db import SQLiteDb

"""
Utility functions that can be applied to the sqlite database used to store processed/cleaned data.

This script assumes the existence of a single sqlite file to save data to. Some people might be 
interested in having an sqlite file per organisation. That is not the intended use here. If you 
need to filter on organisations you can do so using SQL. Every row in the database should have an
organisation identifier.
"""


def get_options_retrieve(argv):
    try:
        # Default is to create a database structure
        command = CREATE
        # Using year/month as directory as download processing of everything can go over a number
        # of days. Expected use of the script i bi-yearly.
        working_dir = str(datetime.today().year) + "-" + str(datetime.today().month)
        # Assume syllabus.sqlite as default file
        db_file = 'syllabus.sqlite'
        opts, args = getopt.getopt(argv, 'hc:d:w:')
        for opt, arg in opts:
            if opt == '-h':
                print('db-utils.py -c command -d <database-file> -w <working-dir>')
                sys.exit()
            if opt in '-c':
                command = arg
            if opt in '-d':
                db_file = arg
            if opt in '-w':
                working_dir = arg

        return command, db_file, working_dir

    except getopt.GetoptError:
        print('db-utils.py -c <command>  -d <database-file> -w <working-dir>')
        sys.exit(2)


def main(argv):
    command_to_process, db_file, working_dir = get_options_retrieve(argv)
    if command_to_process == DROP:
        syllabus_db = SQLiteDb(db_file)
        syllabus_db.drop_tables()
    elif command_to_process == CREATE:
        syllabus_db = SQLiteDb(db_file)
        syllabus_db.build_db_structure()
    elif command_to_process == DROP_CREATE:
        syllabus_db = SQLiteDb(db_file)
        syllabus_db.drop_tables()
        syllabus_db.build_db_structure()
    else:
        print("Unknown command [{}]".format(command_to_process))

if __name__ == '__main__':
    main(sys.argv[1:])
    sys.exit(0)
