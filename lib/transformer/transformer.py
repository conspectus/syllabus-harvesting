import json
import logging
import os
import regex as re
from os.path import exists, join

from lib.transformer.CristinLookup import CRISTINLookup
from lib.transformer.DBHLookup import DBHLookup
from lib.transformer.out.json2csv import Json2CSV
from lib.transformer.out.json2json import Json2Json
from lib.utility.constants import COURSES, RAW, READING_LISTS, CITATIONS, PROCESS, NOT_PROCESSED_READING_LIST, \
    PROCESSED_READING_LIST, COURSE_FILE, PROCESSED_CITATIONS, NOT_PROCESSED_CITATIONS, DO_NOT_PROCESS, ID, CSV, JSON, \
    UNKNOWN_OUTPUT_FORMAT, PROCESSED_COURSE, NOT_PROCESSED_COURSE, NOTHING_TO_PROCESS, DESC, VALUE, TERM, \
    CONSPECTUS_COURSE_NAME, CONSPECTUS_COURSE_CODE, CONSPECTUS_USE_UNIT_CODE, CODE, NAME, DBH_COURSE_NAME_EN, \
    COURSE_NAME_NOT_SAME, ORGANISATION, CONSPECTUS_TERM_NAME_1, CONSPECTUS_TERM_CODE_1, PROCESSING_COMMENT, \
    CITATION_NO_CODE, COURSE_LEGANTO_CODE, RESET_COURSE_CODE, LINK, COURSE_NO_CODE, MULTIPLE_SEMESTER, MISSING_TERM, \
    YEAR, CONSPECTUS_YEAR, PROCESSING, DOT_JSON, REGEX_PRE_CRISTIN_CODE_SECTION_YEAR_SEMESTER, CONSPECTUS_COURSE_YEAR, \
    CONSPECTUS_COURSE_SEMESTER, COURSE_CODE_NOT_PROCESSABLE, PROCESSED_FILES, CONSPECTUS_SECTION, \
    COURSE_CODE_NOT_PROCESSABLE_REASON, ASSUMED_TEST_COURSE
from downloader import get_id_from_link
from lib.utility.transformer_utility import do_checks, remove_course_values, \
    assign_course_code_year_semester, tidy_course_values, set_reading_list_unknown, process_citations

from lib.utility.leganto_constants import is_digit, assign_course_term, assign_course_dept

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)


class Transformer:

    def __init__(self, organisation, output_format, entity, working_dir):
        self.organisation = organisation
        self.cristin_lookup = CRISTINLookup(organisation)
        self.dbh_lookup = DBHLookup(organisation)
        self.working_dir = working_dir
        if output_format == CSV:
            self.out = Json2CSV(organisation, entity, working_dir)
        elif output_format == JSON:
            self.out = Json2Json(organisation, working_dir)
        else:
            raise ValueError(UNKNOWN_OUTPUT_FORMAT.format(output_format))

    def process_courses(self):
        logging.info(PROCESSING.format(COURSES, self.organisation))
        for file in self.get_files(COURSES):
            # noinspection PyTypeChecker
            if file.endswith(DOT_JSON):
                with open(os.path.join(self.working_dir, self.organisation, RAW, COURSES, file), 'r') as course_file:
                    course = json.loads(course_file.read())
                    try:
                        if course[ID] not in DO_NOT_PROCESS[self.organisation]:
                            # if course[ID] == only_process:
                            self.check_should_process(course)
                            self.init_conspectus(course)
                            self.pre_update_course(course)
                            self.update_course(course)
                            self.assign_cristin_values(course)
                            self.assign_dbh_values(course)
                            self.post_update_course(course)
                            do_checks(course)
                            self.out.write_course(course)
                            logging.info(PROCESSED_COURSE.format(self.organisation, course[CONSPECTUS_COURSE_CODE],
                                                                 course[YEAR], course[CONSPECTUS_COURSE_NAME]))
                        else:
                            logging.info(NOT_PROCESSED_COURSE.format(self.organisation, course[CODE], course[YEAR],
                                                                     course[NAME]))
                    except ValueError as ve:
                        logging.info(ve)

    def process_reading_list(self, course, reading_list):
        if CODE not in reading_list:
            raise ValueError(CITATION_NO_CODE.format(reading_list[ID]))
        if CODE not in course:
            raise ValueError(COURSE_NO_CODE.format(course[ID]))

        # In many instances there is a code attribute in a reading_list that contains a
        # floating point number instead of a course code (Sometimes the actual course code
        # is present)
        # TODO: Stop fixing, add fields that hold usable values
        if is_digit(reading_list[CODE]):
            reading_list[CODE] = course[CODE]

        reading_list[CONSPECTUS_COURSE_NAME] = course[CONSPECTUS_COURSE_NAME]
        if TERM in course.keys():
            if len(course[TERM]) > 3:
                logging.error(MULTIPLE_SEMESTER.format(course[ID]))
                raise ValueError(MULTIPLE_SEMESTER.format(course[ID]))
            else:
                for term_count in range(0, len(course[TERM])):
                    reading_list[CONSPECTUS_TERM_NAME_1[:-1] + str(term_count + 1)] = \
                        course[TERM][term_count][DESC]
                    reading_list[CONSPECTUS_TERM_CODE_1[:-1] + str(term_count + 1)] = \
                        course[TERM][term_count][VALUE]
        else:
            logging.info(MISSING_TERM.format(course[ID]))
            set_reading_list_unknown(reading_list)

        # TODO: Replace year with conspectus year
        reading_list[YEAR] = course[YEAR]
        reading_list[CONSPECTUS_YEAR] = course[YEAR]

    def process_reading_lists(self):
        logging.info(PROCESSING.format(READING_LISTS, self.organisation))
        for file in self.get_files(READING_LISTS):
            if file.endswith(DOT_JSON):
                with open(os.path.join(self.working_dir, self.organisation, RAW, READING_LISTS, file), 'r') \
                        as reading_list_file:
                    reading_list = json.loads(reading_list_file.read())
                    course_id = get_id_from_link(reading_list[LINK], COURSES)
                    logging.info('Course id ' + course_id + ' rid is ' + reading_list[LINK])
                    course = self.get_course(course_id)
                    if course != {}:
                        self.process_reading_list(course, reading_list)
                        logging.info(PROCESSED_READING_LIST.format(
                            self.organisation, reading_list[ID], reading_list[NAME]))
                    else:
                        logging.info(NOT_PROCESSED_READING_LIST.format(
                            self.organisation, reading_list[ID], reading_list[NAME]))

    def process_all_citations(self):
        logging.info(PROCESSING.format(CITATIONS, self.organisation))
        count = 1
        for file in self.get_files(CITATIONS):
            if file.endswith(DOT_JSON):
                with open(os.path.join(self.working_dir, self.organisation, RAW, CITATIONS, file), 'r') \
                        as citation_file:
                    citations = json.loads(citation_file.read())
                    course_id = get_id_from_link(citations[LINK], COURSES)
                    reading_list_id = get_id_from_link(citations[LINK], READING_LISTS)
                    course = self.get_course(course_id)
                    if course != {}:
                        if CODE not in citations:
                            raise ValueError(CITATION_NO_CODE.format(citations[ID]))
                        if CODE not in course:
                            raise ValueError(COURSE_NO_CODE.format(course[ID]))
                        if citations[CODE] != course[CODE]:
                            citations[CODE] = course[CODE]
                            logging.info(RESET_COURSE_CODE.format(citations[CODE], course[CODE]))
                        process_citations(course, reading_list_id, citations)
                        self.out.write_citations(citations)
                        logging.info(PROCESSED_CITATIONS.format(self.organisation, course[CODE], course[NAME]))
                    else:
                        logging.error(NOT_PROCESSED_CITATIONS)
                    count = count + 1
        logging.info(PROCESSED_FILES.format(str(count), CITATIONS, self.organisation))

    def assign_dbh_values(self, course):
        self.dbh_lookup.assign_dbh_values(course)

    def assign_course_name(self, course):
        match = re.match(r'^(' + course[CONSPECTUS_COURSE_CODE] + r')\s*-\s*(.+)(\(.*\))$', course[NAME])
        if match is not None:
            course[CONSPECTUS_COURSE_NAME] = match.group(2).strip()

    def update_course(self, course):
        assign_course_term(course)
        self.assign_course_name(course)
        assign_course_dept(course)
        remove_course_values(course)
        tidy_course_values(course)
        course[COURSE_LEGANTO_CODE] = course[CODE]
        assign_course_code_year_semester(course)
        self.process_course_code_year_semester(course)

    def get_course(self, course_id):
        """Note this gets course from the processed directory, not the raw directory!"""
        """Return an empty dict if the file does not exist, otherwise return the contents
        of the file as a JSON object"""
        json_file = join(self.working_dir, self.organisation, PROCESS, COURSES, COURSE_FILE.format(course_id))
        if exists(json_file):
            with open(json_file, 'r') as course_file:
                return json.loads(course_file.read())
        return {}

    def pre_update_course(self, course):
        # course[COURSE_NAME] = ''
        course[PROCESSING_COMMENT] = ''

    def post_update_course(self, course):
        if course[DBH_COURSE_NAME_EN] != '' and \
                course[CONSPECTUS_COURSE_NAME] != course[DBH_COURSE_NAME_EN]:
            logging.info(COURSE_NAME_NOT_SAME
                         .format(course[CONSPECTUS_COURSE_CODE], course[CONSPECTUS_COURSE_NAME],
                                 course[DBH_COURSE_NAME_EN]))
            course[CONSPECTUS_COURSE_NAME] = course[DBH_COURSE_NAME_EN]

    def assign_cristin_values(self, course):
        dep_details = self.cristin_lookup.get_cristin_details_from_leganto_course(course)
        if CONSPECTUS_USE_UNIT_CODE not in dep_details.keys():
            logging.error('conspectus_use_unit_code not in dep_details.keys()')
            raise ValueError('conspectus_use_unit_code not in dep_details.keys()')
        if dep_details[CONSPECTUS_USE_UNIT_CODE] is None or dep_details[CONSPECTUS_USE_UNIT_CODE] == '':
            print(json.dumps(course, indent=2))
            logging.error('conspectus_use_unit_code empty')
            raise ValueError('conspectus_use_unit_code empty')
        course.update(dep_details)
        # TODO : Remove
        # set_minimum_information(course, dep_details[UNIT_CODE])

    def check_should_process(self, course):
        if "EXLIBRIS_DEFAULT_COURSE" in course[CODE] or \
                "EMPTY_COURSE_CODE" in course[CODE] or \
                "SLETTES_AV_KOPINOR" in course[CODE]:
            raise ValueError(COURSE_CODE_NOT_PROCESSABLE_REASON
                             .format(course[ID], course[CODE], ASSUMED_TEST_COURSE))

    def init_conspectus(self, course):
        course[CONSPECTUS_COURSE_CODE] = ''
        course[CONSPECTUS_COURSE_NAME] = course[NAME]
        course[ORGANISATION] = self.organisation

    def get_files(self, entity):
        directory = os.path.join(self.working_dir, self.organisation, RAW, entity)
        if not os.path.isdir(directory):
            logging.info(NOTHING_TO_PROCESS.format(directory))
            return
        return os.listdir(directory)

    def process_course_code_year_semester(self, course):
        match = re.search(REGEX_PRE_CRISTIN_CODE_SECTION_YEAR_SEMESTER, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(2).strip()  # 2 == Course code
            course[CONSPECTUS_COURSE_YEAR] = match.group(4)  # 4 == Course year
            course[CONSPECTUS_COURSE_SEMESTER] = match.group(5)  # 5 == Course semester
            course[CONSPECTUS_SECTION] = match.group(6)  # 6 == Section
            return match
        else:
            raise ValueError(COURSE_CODE_NOT_PROCESSABLE
                             .format(course[ID], course[CODE], str(REGEX_PRE_CRISTIN_CODE_SECTION_YEAR_SEMESTER)))
