import json
import logging
import datetime

import pandas as pd
from os.path import join

from lib.utility.constants import RESOURCES, DBH_MAPPINGS, DBH_MAPPING_FILENAME, DBH_COLUMN_NAMES, DBH_COURSE_CODE, \
    DBH_YEAR, VALUE, DBH_SEMESTER_NAME, EMPTY_DBH_OBJECT, SECTION, CODE, YEAR, TERM, DBH_INSTITUTION_CODE_EN, \
    DBH_INSTITUTION_NAME_EN, DBH_DEPT_CODE_EN, DBH_DEPT_NAME_EN, DBH_DEPT_CODE_SSB_EN, DBH_YEAR_EN, DBH_SEMESTER_EN, \
    DBH_SEMESTER_NAME_EN, DBH_SEMESTER, DBH_DEPT_CODE_SSB, DBH_DEPT_NAME, DBH_DEPT_CODE, DBH_INSTITUTION_NAME, \
    DBH_INSTITUTION_CODE, DBH_COURSE_STUDY_PROGRAM_CODE_EN, DBH_COURSE_STUDY_PROGRAM_NAME_EN, DBH_COURSE_CODE_EN, \
    DBH_COURSE_NAME_EN, DBH_LEVEL_CODE_EN, DBH_LEVEL_NAME_EN, DBH_COURSE_CREDIT_EN, DBH_NUS_CODE_EN, DBH_STATUS_EN, \
    DBH_STATUS_NAME_EN, DBH_COURSE_STUDY_PROGRAM_CODE, DBH_COURSE_STUDY_PROGRAM_NAME, DBH_COURSE_NAME, DBH_LEVEL_CODE, \
    DBH_LEVEL_NAME, DBH_COURSE_CREDIT, DBH_NUS_CODE, DBH_STATUS, DBH_STATUS_NAME, DBH_NAME_EN, DBH_AREA_CODE_EN, \
    DBH_AREA_NAME_EN, DBH_AREA_CODE, DBH_AREA_NAME, DBH_TASK, DBH_NAME, DBH_LANGUAGE, DBH_LANGUAGE_EN, DBH_TASK_EN, \
    DBH_NO_DATA, NOT_FOUND_DBH_DATA, TRYING_DBH_DATA, FOUND_DBH_DATA, CONSPECTUS_COURSE_NAME, MULTIPLE_SEMESTER, \
    CONSPECTUS_COURSE_CODE, CONSPECTUS_COURSE_SEMESTER, CONSPECTUS_SECTION

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)


def convert_nor_eng(nor_dbh_dict):
    return {
        DBH_INSTITUTION_CODE_EN: nor_dbh_dict[DBH_INSTITUTION_CODE],
        DBH_INSTITUTION_NAME_EN: nor_dbh_dict[DBH_INSTITUTION_NAME],
        DBH_DEPT_CODE_EN: nor_dbh_dict[DBH_DEPT_CODE],
        DBH_DEPT_NAME_EN: nor_dbh_dict[DBH_DEPT_NAME],
        DBH_DEPT_CODE_SSB_EN: nor_dbh_dict[DBH_DEPT_CODE_SSB],
        DBH_YEAR_EN: nor_dbh_dict[DBH_YEAR],
        DBH_SEMESTER_EN: nor_dbh_dict[DBH_SEMESTER],
        DBH_SEMESTER_NAME_EN: nor_dbh_dict[DBH_SEMESTER_NAME],
        DBH_COURSE_STUDY_PROGRAM_CODE_EN: nor_dbh_dict[DBH_COURSE_STUDY_PROGRAM_CODE],
        DBH_COURSE_STUDY_PROGRAM_NAME_EN: nor_dbh_dict[DBH_COURSE_STUDY_PROGRAM_NAME],
        DBH_COURSE_CODE_EN: nor_dbh_dict[DBH_COURSE_CODE],
        DBH_COURSE_NAME_EN: nor_dbh_dict[DBH_COURSE_NAME],
        DBH_LEVEL_CODE_EN: nor_dbh_dict[DBH_LEVEL_CODE],
        DBH_LEVEL_NAME_EN: nor_dbh_dict[DBH_LEVEL_NAME],
        DBH_COURSE_CREDIT_EN: nor_dbh_dict[DBH_COURSE_CREDIT],
        DBH_NUS_CODE_EN: nor_dbh_dict[DBH_NUS_CODE],
        DBH_STATUS_EN: nor_dbh_dict[DBH_STATUS],
        DBH_STATUS_NAME_EN: nor_dbh_dict[DBH_STATUS_NAME],
        DBH_LANGUAGE_EN: nor_dbh_dict[DBH_LANGUAGE],
        DBH_NAME_EN: nor_dbh_dict[DBH_NAME],
        DBH_AREA_CODE_EN: nor_dbh_dict[DBH_AREA_CODE],
        DBH_AREA_NAME_EN: nor_dbh_dict[DBH_AREA_NAME],
        DBH_TASK_EN: nor_dbh_dict[DBH_TASK]
    }


def get_code_semester_year(course, section):
    print ("Processing " + course[CODE])
    code = course[CONSPECTUS_COURSE_CODE] + "-" + section
    year = course[YEAR]
    semester = course[CONSPECTUS_COURSE_SEMESTER]

    if semester == 'VÅR':
        semester = 'Vår'
    elif semester == 'HØST':
        semester = 'Høst'
    elif semester == 'SOMMER':
        semester = 'Sommer'

    return code, semester, year

class DBHLookup:

    def __init__(self, organisation):
        super().__init__()
        self.organisation = organisation
        self.df_dbh = pd.read_csv(join(RESOURCES, DBH_MAPPINGS,
                                       DBH_MAPPING_FILENAME.format(organisation)), dtype='unicode',
                                  sep=',', quotechar='"', names=DBH_COLUMN_NAMES)

    def assign_dbh_values(self, course):
        """Assign DBH values to the course. Also 1. Overwrite the conspectus_course_name with the DBH value if
        different. 2 Use the first semester is multiple semester are recorded 3. """

        section = '1'
        if course[SECTION] != '':
            section = course[SECTION]
        elif CONSPECTUS_SECTION in course.keys():
            section = course[CONSPECTUS_SECTION]

        code, semester, year = get_code_semester_year(course, section)
        dbh = self.get_dbh_object(code, semester, year)

        course.update(convert_nor_eng(dbh[0]))

    def get_dbh_object(self, code, semester, year):
        row = json.loads(self.df_dbh.loc[(self.df_dbh[DBH_COURSE_CODE] == code) &
                                         (self.df_dbh[DBH_YEAR] == year) &
                                         (self.df_dbh[DBH_SEMESTER_NAME] == semester)].to_json(orient='records'))

        if len(row) != 1:
            rows = json.loads(self.df_dbh.loc[(self.df_dbh[DBH_COURSE_CODE] == code)]
                              .sort_values(by=[DBH_YEAR], ascending=False)
                              .to_json(orient='records'))
            if len(rows) > 0:
                row = [rows[0]]
            else:
                row = [EMPTY_DBH_OBJECT]
        return row
