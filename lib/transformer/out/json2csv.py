import logging

from lib.transformer.out.OutWriter import OutWriter
from lib.utility.constants import PROCESS, PROCESSED_CITATIONS_CSV, CONSPECTUS_COURSE_CODE, READING_LIST_ID, CITATIONS, \
    CITATION, \
    ORGANISATION, NO_CITATIONS
from lib.file.CSVFile import CSVFile
from lib.utility.transformer_utility import merge_citation_with_parent

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)




class Json2CSV(OutWriter):

    def __init__(self, organisation, entity, working_dir):
        self.file = CSVFile(organisation, entity, working_dir, PROCESS)

    def write_course(self, course):
        self.file.write_course(course)

    def write_reading_list(self, reading_list):
        self.file.write_reading_list(reading_list)

    def write_citation(self, citation):
        self.file.write_citation(citation)

    def write_citations(self, citations):
        if CITATIONS in citations.keys():
            citations_count = 0
            if CITATION in citations[CITATIONS].keys():
                for citation in citations[CITATIONS][CITATION]:
                    merge_citation_with_parent(citation, citations)
                    citations_count = citations_count + 1
                    self.write_citation(citation)
                logging.info(PROCESSED_CITATIONS_CSV.format(
                    str(citations_count), citations[ORGANISATION], citations[CONSPECTUS_COURSE_CODE],
                    citations[READING_LIST_ID]))
            else:
                logging.error(NO_CITATIONS.format(
                    citations[ORGANISATION], citations[CONSPECTUS_COURSE_CODE],
                    citations[READING_LIST_ID]))