from lib.utility.constants import PROCESS, COURSE_LEGANTO_CODE, NAME
from lib.file.JsonFile import JsonFile


class Json2Json:

    def __init__(self, organisation, working_dir):
        self.file = JsonFile(organisation, working_dir, PROCESS)

    def write_course(self, course):
        self.file.write_course(course)

    def write_reading_list(self, reading_list):
        reading_list['blocked'] = False
        if 'id' not in reading_list.keys():
            print("Course with id {} has reading list with name {} that is unavailable".format(
                reading_list[COURSE_LEGANTO_CODE], reading_list[NAME]))
            reading_list['blocked'] = True
        self.file.write_reading_list(reading_list)

    def write_citation(self, citation):
        pass

    def write_citations(self, citations):
        self.file.write_citations(citations)
