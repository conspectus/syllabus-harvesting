from abc import ABC, abstractmethod


class OutWriter(ABC):
    @abstractmethod
    def write_course(self, course):
        pass

    @abstractmethod
    def write_reading_list(self, reading_list):
        pass

    @abstractmethod
    def write_citation(self, citation):
        pass

    @abstractmethod
    def write_citations(self, citations):
        pass
