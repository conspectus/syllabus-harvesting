from lib.db.sqlite_db import SQLiteDb
from lib.transformer.transformer import Transformer
from lib.utility.leganto_constants import check_for_unknown_reading_list_keys, \
    check_for_unknown_citation_keys


class Json2Db(Transformer):

    def __init__(self, db_file, organisation, org_db):
        super().__init__(organisation, org_db)
        self.syllabus_db = SQLiteDb(db_file)

    def process_course(self, course):
        super().process_course(course)
        self.syllabus_db.add_course(course, self.organisation)

    def process_reading_list(self, course, reading_list):
        super().process_reading_list(course, reading_list)
        self.syllabus_db.add_course_reading_list(course['id'], reading_list, self.organisation)
        check_for_unknown_reading_list_keys(reading_list)

    # def process_citation(self, course, reading_list_id, citation):
    #    super().process_citation(course, reading_list_id, citation)
    #    self.syllabus_db.add_course_citation(course, reading_list_id, citation, self.organisation)
    #    check_for_unknown_citation_keys(citation)

    def process_citations_object(self, course, reading_list_id, citations_object):
        super().process_citations_object(course, reading_list_id, citations_object)
        if 'citations' in citations_object:
            citations = citations_object['citations']
            citations_count = 0
            if 'citation' in citations:
                for citation in citations['citation']:
                    citations_count = citations_count + 1
                    self.process_single_citation(citation)
                    self.syllabus_db.add_course_citation(course, reading_list_id, citation,
                                                         self.organisation)
                    check_for_unknown_citation_keys(citation)
            print("Processed [{}] citations for [{}], course [{}], reading list[{}]".format(
                str(citations_count), self.organisation, course['id'], reading_list_id))
