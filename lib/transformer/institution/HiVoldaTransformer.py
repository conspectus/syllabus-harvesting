import regex as re

from lib.utility.leganto_constants import get_semester_name
from lib.transformer.transformer import Transformer
from lib.utility.constants import HIVOLDA, CONSPECTUS_COURSE_CODE, CONSPECTUS_COURSE_YEAR, CONSPECTUS_COURSE_SEMESTER, \
    CODE, REGEX_KLKU_PRE_CODE_YEAR_SEMESTER, COURSE_CODE_NOT_PROCESSABLE_REASON, ID, ASSUMED_DEFAULT_COURSE, \
    REGEX_CODE_YEAR_SEMESTER_NAME


class HIVoldaTransformer(Transformer):

    def __init__(self, output_format, entity, working_dir):
        super().__init__(HIVOLDA, output_format, entity, working_dir)

    def check_should_process(self, course):
        if course[CODE].lower().startswith("test"):
            raise ValueError(COURSE_CODE_NOT_PROCESSABLE_REASON
                             .format(course[ID], course[CODE], ASSUMED_DEFAULT_COURSE))
        super().check_should_process(course)

    def process_course_code_year_semester(self, course):
        # KU_KOH-110KFK_2022_HØST, KU_TIND_2021_HØST
        match = re.search(REGEX_KLKU_PRE_CODE_YEAR_SEMESTER, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(2)  # 2 == Course code
            course[CONSPECTUS_COURSE_YEAR] = match.group(3)  # 3 == Course year
            course[CONSPECTUS_COURSE_SEMESTER] = match.group(5)  # 5 == Course semester
            return
        # DKL116N-1 23V Progr i skolen
        match = re.search(REGEX_CODE_YEAR_SEMESTER_NAME, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(1)  # 1 == Course code
            course[CONSPECTUS_COURSE_YEAR] = '20̈́' + match.group(2)  # 2 == Course year
            course[CONSPECTUS_COURSE_SEMESTER] = get_semester_name(match.group(3))  # 3 == Course semester
            return
        # No known alternative approaches work, try the standard approach
        super().process_course_code_year_semester(course)
