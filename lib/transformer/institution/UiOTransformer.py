import regex as re

from lib.utility.leganto_constants import get_semester_name
from lib.transformer.transformer import Transformer
from lib.utility.constants import UiO, CONSPECTUS_COURSE_CODE, CONSPECTUS_COURSE_YEAR, CONSPECTUS_COURSE_SEMESTER, \
    CODE, REGEX_UIO_PRE_CODE_SEMESTER_YEAR


class UiOTransformer(Transformer):

    def __init__(self, output_format, entity, working_dir):
        super().__init__(UiO, output_format, entity, working_dir)

    def process_course_code_year_semester(self, course):
        # "EVU_11-TEOL4538V_2024V-2024H"
        # "EVU_11-TEOL4541V_2023H"
        match = re.search(REGEX_UIO_PRE_CODE_SEMESTER_YEAR, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(1)  # 1 == Course code
            course[CONSPECTUS_COURSE_YEAR] = match.group(2)  # 2 == Course year
            course[CONSPECTUS_COURSE_SEMESTER] = get_semester_name(match.group(3))  # 3 == Course semester
            return

        # No known alternative approaches work, try the standard approach
        super().process_course_code_year_semester(course)
