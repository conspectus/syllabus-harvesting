import regex as re

from lib.transformer.transformer import Transformer
from lib.utility.constants import INN, CONSPECTUS_COURSE_CODE, CONSPECTUS_COURSE_YEAR, CONSPECTUS_COURSE_SEMESTER, \
    CODE, REGEX_KLKU_PRE_CODE_YEAR_SEMESTER, COURSE_CODE_NOT_PROCESSABLE_REASON, ID, ASSUMED_TEST_COURSE, \
    ASSUMED_NOT_RELEVANT


class INNTransformer(Transformer):

    def __init__(self, output_format, entity, working_dir):
        super().__init__(INN, output_format, entity, working_dir)

    def check_should_process(self, course):
        if course[CODE].lower().startswith('test'):
            raise ValueError(COURSE_CODE_NOT_PROCESSABLE_REASON
                             .format(course[ID], course[CODE], ASSUMED_TEST_COURSE))
        if 'sandkasse' in course[CODE].lower():
            raise ValueError(COURSE_CODE_NOT_PROCESSABLE_REASON
                             .format(course[ID], course[CODE], ASSUMED_TEST_COURSE))

        # It was decided to simply ignore the following courses that have course.code as in the list
        # It is easier to ignore than to build a regex. It's not clear that these are relevant courses.
        list_do_not_process = [
            '2MMA171S-1 23H Matematikk 1, emne 1 Videreutdanning',
            'BOLK_INN_24',
            'Fellesrom kroppsøving',
            'Fellesrom Studiested Rena',
            'Forkurs HHS',
            'Førstelektorprogram',
            'HELDE4001',
            'Innføring i Leganto',
            'Leganto veiledning',
            'MEV3000-1',
            'NESBØ_HØST_2022',
            'Nettundervisning',
            'SKRI V24',
            'SP_BAPSY',
            'STØ1000',
            'TVLED2901 V24 ekstra',
            'VPL1101-22H'
            'Zoom']

        if course[CODE] in list_do_not_process:
            raise ValueError(COURSE_CODE_NOT_PROCESSABLE_REASON
                             .format(course[ID], course[CODE], ASSUMED_NOT_RELEVANT))
        super().check_should_process(course)

    def process_course_code_year_semester(self, course):
        # KU_MAHE_2023_HØST, KU_SAKRIKO1_2022_HØST, KL_HELSELED60_2024_VÅR_SKIEN
        match = re.search(REGEX_KLKU_PRE_CODE_YEAR_SEMESTER, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(2)  # 2 == Course code
            course[CONSPECTUS_COURSE_YEAR] = match.group(3)  # 3 == Course year
            course[CONSPECTUS_COURSE_SEMESTER] = match.group(5)  # 5 == Course semester
            return
        # No known alternative approaches work, try the standard approach
        super().process_course_code_year_semester(course)
