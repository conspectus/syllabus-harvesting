import regex as re

from lib.transformer.transformer import Transformer
from lib.utility.constants import TERM, VALUE, DESC, UiA, CONSPECTUS_COURSE_NAME, CONSPECTUS_COURSE_CODE, \
    REGEX_NAME_DASH_COURSE_CONSPECTUS_TERM_YEAR, REGEX_COURSE_DASH_NAME_PAR_ANY_PAR, REGEX_COURSE_SPACE_CODE, NAME, \
    CONSPECTUS_COURSE_YEAR, REGEX_PRE_CRISTIN_CODE_SECTION_YEAR_SEMESTER, COURSE_CODE_NOT_PROCESSABLE, ID, \
    CONSPECTUS_COURSE_SEMESTER, CODE


def update_course_name_if_match(regex, course, group_index):
    match = re.match(regex, course[NAME])
    if match is not None:
        course[CONSPECTUS_COURSE_NAME] = match.group(group_index).strip()


class UiATransformer(Transformer):

    def __init__(self, output_format, entity, working_dir):
        super().__init__(UiA, output_format, entity, working_dir)

    def pre_update_course(self, course):
        super(UiATransformer, self).pre_update_course(course)
        if TERM not in course.keys():
            match = re.match(r"^.+(HØST|VÅR).*$", course[CODE])
            if match is not None:
                course[TERM] = []
                semester = match.group(1)
                if semester == 'VÅR':
                    course[TERM].append({
                        VALUE: 'SPRING',
                        DESC: 'Spring'
                    })
                elif semester == 'HØST':
                    course[TERM].append({
                        VALUE: 'AUTUMN',
                        DESC: 'Autumn'
                    })
                else:
                    print(course[ID])
                    print("Semester is " + semester)
                    raise ValueError('Semester is neither h/v')

            else:
                match = re.match(r".*\d\d([V|H])$", course[CODE])
                if match is not None:
                    course[TERM] = []
                    semester = match.group(1)
                    if semester == 'V':
                        course[TERM].append({
                            VALUE: 'SPRING',
                            DESC: 'Spring'
                        })
                    elif semester == 'H':
                        course[TERM].append({
                            VALUE: 'AUTUMN',
                            DESC: 'Autumn'
                        })
                else:
                    print(course[ID])
                    print(course[CODE])
                    raise ValueError('No term not sure what to do')

    def update_course(self, course):
        super().update_course(course)
        course[CONSPECTUS_COURSE_NAME] = course[NAME].strip()
        # TODO: Consider moving Tidy up course names to own method
        update_course_name_if_match(REGEX_COURSE_DASH_NAME_PAR_ANY_PAR
                                    .format(course[CONSPECTUS_COURSE_CODE]), course, 2)
        update_course_name_if_match(REGEX_NAME_DASH_COURSE_CONSPECTUS_TERM_YEAR
                                    .format(course[CONSPECTUS_COURSE_CODE]), course, 1)
        update_course_name_if_match(REGEX_COURSE_SPACE_CODE
                                    .format(course[CONSPECTUS_COURSE_CODE]), course, 1)

    def post_update_course(self, course):
        super().post_update_course(course)

    def process_course_code_year_semester(self, course):
        match = re.search(REGEX_PRE_CRISTIN_CODE_SECTION_YEAR_SEMESTER, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(2)  # 2 == Course code
            course[CONSPECTUS_COURSE_YEAR] = match.group(4)  # 4 == Course year
            course[CONSPECTUS_COURSE_SEMESTER] = match.group(5)  # 5 == Course semester
        else:
            raise ValueError(COURSE_CODE_NOT_PROCESSABLE
                             .format(course[ID], course[CODE], str(REGEX_PRE_CRISTIN_CODE_SECTION_YEAR_SEMESTER)))
