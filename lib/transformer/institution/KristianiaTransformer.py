import regex as re

from lib.utility.leganto_constants import get_semester_name
from lib.transformer.transformer import Transformer
from lib.utility.constants import KRISTIANIA, CONSPECTUS_COURSE_CODE, CONSPECTUS_COURSE_YEAR, \
    CONSPECTUS_COURSE_SEMESTER, CODE, REGEX_KLKU_PRE_CODE_YEAR_SEMESTER, COURSE_CODE_NOT_PROCESSABLE_REASON, \
    ID, ASSUMED_DEFAULT_COURSE, REGEX_CODE_SEMESTER_YEAR, REGEX_PRE_CODE_SECTION_YEAR_SEMESTER, CONSPECTUS_SECTION, \
    REGEX_SIS, UNKNOWN, START_DATE
from lib.utility.leganto_utility import get_semester_from_term, guess_semester


class KristianiaTransformer(Transformer):

    def __init__(self, output_format, entity, working_dir):
        super().__init__(KRISTIANIA, output_format, entity, working_dir)

    def check_should_process(self, course):
        if course[CODE].lower().startswith("test"):
            raise ValueError(COURSE_CODE_NOT_PROCESSABLE_REASON
                             .format(course[ID], course[CODE], ASSUMED_DEFAULT_COURSE))
        super().check_should_process(course)

    def process_course_code_year_semester(self, course):
        # BHPED_H19
        match = re.search(REGEX_CODE_SEMESTER_YEAR, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(1)  # 1 == Course code
            course[CONSPECTUS_COURSE_SEMESTER] = get_semester_name(match.group(2))  # 2 == Course semester
            course[CONSPECTUS_COURSE_YEAR] = '20̈́' + match.group(3)  # 3 == Course year
            return

        # KL_BFTV_2019_HØST_19FTV-REGI, KL_MLE-R_2022_VÅR_MV22MLEHE1, KU_BIN_2021_HØST
        match = re.search(REGEX_KLKU_PRE_CODE_YEAR_SEMESTER, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(2)  # 2 == Course code
            course[CONSPECTUS_COURSE_YEAR] = match.group(3)  # 3 == Course year
            course[CONSPECTUS_COURSE_SEMESTER] = match.group(5)  # 5 == Course semester
            return

        # 1615_3DG308_1_2021_HØST_1, 1615_3DG309_1_2021_HØST_1
        regex_pre_1615 = REGEX_PRE_CODE_SECTION_YEAR_SEMESTER.replace('(UA|UE)', '^1615')
        match = re.search(regex_pre_1615, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(1)  # 2 == Course code
            course[CONSPECTUS_COURSE_YEAR] = match.group(3)  # 4 == Course year
            course[CONSPECTUS_COURSE_SEMESTER] = match.group(4)  # 4 == Course semester
            course[CONSPECTUS_SECTION] = match.group(2)  # 2 == Section
            return

        match = re.search(REGEX_SIS, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(1)  # 1 == Course code
            course[CONSPECTUS_COURSE_YEAR] = course[START_DATE].split('-')[0]
            course[CONSPECTUS_COURSE_SEMESTER] = get_semester_from_term(course)
            if course[CONSPECTUS_COURSE_SEMESTER] == UNKNOWN:
                guess_semester(course)
            return

        # No known alternative approaches work, try the standard approach
        super().process_course_code_year_semester(course)
