import regex as re

from lib.transformer.transformer import Transformer
from lib.utility.constants import NTNU, CONSPECTUS_COURSE_NAME, CONSPECTUS_COURSE_CODE, \
    REGEX_NAME_DASH_COURSE_CONSPECTUS_TERM_YEAR, REGEX_COURSE_DASH_NAME_PAR_ANY_PAR, REGEX_COURSE_SPACE_CODE, NAME, \
    CONSPECTUS_COURSE_YEAR, CONSPECTUS_COURSE_SEMESTER, CODE, REGEX_MERGE_COURSES_SEMESTER_YEAR, \
    REGEX_CRISTIN_CODE_SECTION_YEAR_SEMESTER, REGEX_PRE_CODE_SECTION_YEAR_SEMESTER, ORGS, KNOWN_LEGANTO_CODE
from lib.utility.leganto_constants import get_semester_name


def update_course_name_if_match(regex, course, group_index):
    match = re.match(regex, course[NAME])
    if match is not None:
        course[CONSPECTUS_COURSE_NAME] = match.group(group_index).strip()


def join_course_codes(match, count):
    course_code = ''
    for i in range(1, count):
        if match.group(i) is not None:
            course_code = course_code + match.group(i) + '-'
    # Remove the trailing dash
    if len(course_code) > 1:
        course_code = course_code[:-1]
    return course_code


class NTNUTransformer(Transformer):

    def __init__(self, output_format, entity, working_dir):
        super().__init__(NTNU, output_format, entity, working_dir)

    def update_course(self, course):
        super().update_course(course)
        course[CONSPECTUS_COURSE_NAME] = course[NAME].strip()
        # TODO: Consider moving Tidy up course names to own method
        update_course_name_if_match(REGEX_COURSE_DASH_NAME_PAR_ANY_PAR
                                    .format(course[CONSPECTUS_COURSE_CODE]), course, 2)
        update_course_name_if_match(REGEX_NAME_DASH_COURSE_CONSPECTUS_TERM_YEAR
                                    .format(course[CONSPECTUS_COURSE_CODE]), course, 1)
        update_course_name_if_match(REGEX_COURSE_SPACE_CODE
                                    .format(course[CONSPECTUS_COURSE_CODE]), course, 1)

    def post_update_course(self, course):
        super().post_update_course(course)

    def process_course_code_year_semester(self, course):
        # MERGE_SYT3002_SYG3002_SYA3002_H21
        match = re.search(REGEX_MERGE_COURSES_SEMESTER_YEAR, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = join_course_codes(match, 5)  # 1-4 == Course code
            course[CONSPECTUS_COURSE_SEMESTER] = get_semester_name(match.group(5))  # 4 == Course semester
            course[CONSPECTUS_COURSE_YEAR] = '20' + match.group(6)  # 5 == Course year
            return

        # 194_(MGLU2504)_1_(HØST)_(2020)_1
        if course[CDOE].startswith(ORGS[self.organisation][KNOWN_LEGANTO_CODE]):
            match = re.search(REGEX_CRISTIN_CODE_SECTION_YEAR_SEMESTER, course[CODE])
            if match is not None:
                course[CONSPECTUS_COURSE_CODE] = match.group(1)  # 1 == Course code
                course[CONSPECTUS_COURSE_SEMESTER] = get_semester_name(match.group(3))  # 3 == Course semester
                course[CONSPECTUS_COURSE_YEAR] = match.group(4)  # 4 == Course year
                return
        # UE_(FI5206)_1_(2023)_(HØST)_1
        if not course[CDOE].startswith('UE_'+ORGS[self.organisation][KNOWN_LEGANTO_CODE]) and \
            not course[CDOE].startswith('UA_' + ORGS[self.organisation][KNOWN_LEGANTO_CODE]):
            match = re.search(REGEX_PRE_CODE_SECTION_YEAR_SEMESTER, course[CODE])
            if match is not None:
                course[CONSPECTUS_COURSE_CODE] = match.group(2)  # 2 == Course code
                course[CONSPECTUS_COURSE_YEAR] = match.group(4)  # 4 == Course year
                course[CONSPECTUS_COURSE_SEMESTER] = match.group(5)  # 5 == Course semester
                course[CONSPECTUS_SECTION] = match.group(3)  # 3 == Section
                return
        # No known alternative approaches work, try the standard approach
        super().process_course_code_year_semester(course)
