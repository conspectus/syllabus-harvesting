import regex as re

from lib.transformer.transformer import Transformer
from lib.utility.constants import HVL, CONSPECTUS_COURSE_NAME, CONSPECTUS_COURSE_CODE, NAME, \
    CONSPECTUS_COURSE_YEAR, CONSPECTUS_COURSE_SEMESTER


def update_course_name_if_match(regex, course, group_index):
    match = re.match(regex, course[NAME])
    if match is not None:
        course[CONSPECTUS_COURSE_NAME] = match.group(group_index).strip()


class HVLTransformer(Transformer):

    def __init__(self, output_format, entity, working_dir):
        super().__init__(HVL, output_format, entity, working_dir)

    def pre_update_course(self, course):
        super(HVLTransformer, self).pre_update_course(course)

    def update_course(self, course):
        super().update_course(course)

    def post_update_course(self, course):
        super().post_update_course(course)

    def process_course_code_year_semester(self, course):
        match = re.search(r'(UA|UE)_201_(.*)_([a-zA-Z]|\d)_(\d\d\d\d)_(VÅR|HØST)_\d+_?\d*-?\d*', course['code'])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(2)  # 2 == Course code
            course[CONSPECTUS_COURSE_YEAR] = match.group(4)  # 4 == Course year
            course[CONSPECTUS_COURSE_SEMESTER] = match.group(5)  # 5 == Course semester
