import regex as re

from lib.transformer.transformer import Transformer
from lib.utility.constants import LDH, CONSPECTUS_COURSE_CODE, CONSPECTUS_COURSE_YEAR, CONSPECTUS_COURSE_SEMESTER, \
    CODE, REGEX_PRE_CODE_SECTION_YEAR_SEMESTER, CONSPECTUS_SECTION, UNKNOWN
from lib.utility.leganto_utility import guess_semester, get_semester_from_term


class LDHTransformer(Transformer):

    def __init__(self, output_format, entity, working_dir):
        super().__init__(LDH, output_format, entity, working_dir)

    def process_course_code_year_semester(self, course):

        if not course[CODE].startswith('UE_230_') and \
                not course[CODE].startswith('UA_230_'):
            match = re.search(REGEX_PRE_CODE_SECTION_YEAR_SEMESTER, course[CODE])
            if match is not None:
                course[CONSPECTUS_COURSE_CODE] = match.group(2)  # 2 == Course code
                course[CONSPECTUS_COURSE_YEAR] = match.group(3)  # 3 == Course year
                course[CONSPECTUS_COURSE_SEMESTER] = match.group(4)  # 4 == Course semester
                course[CONSPECTUS_SECTION] = match.group(2)  # 2 == Section
                return

            match = re.search(r'(.*)(\d\d\d\d)$', course[CODE])
            if match is not None:
                course[CONSPECTUS_COURSE_CODE] = match.group(1)  # 1 == Course code
                course[CONSPECTUS_COURSE_YEAR] = match.group(2)  # 2 == Course year
                course[CONSPECTUS_COURSE_SEMESTER] = get_semester_from_term(course)
                if course[CONSPECTUS_COURSE_SEMESTER] == UNKNOWN:
                    course[CONSPECTUS_COURSE_SEMESTER] = guess_semester(course)
                return

        # No known alternative approaches work, try the standard approach
        super().process_course_code_year_semester(course)
