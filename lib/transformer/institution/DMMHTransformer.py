import regex as re

from lib.transformer.transformer import Transformer
from lib.utility.constants import DMMH, CONSPECTUS_COURSE_CODE, CONSPECTUS_COURSE_YEAR, CONSPECTUS_COURSE_SEMESTER, \
    CODE, END_DATE, UNKNOWN, REGEX_DMMH_CODE_A, REGEX_DMMH_CODE_B, REGEX_DMMH_CODE_C, \
    COURSE_CODE_NOT_PROCESSABLE, ID
from lib.utility.leganto_utility import get_semester_from_term, guess_semester


class DMMHTransformer(Transformer):

    def __init__(self, output_format, entity, working_dir):
        super().__init__(DMMH, output_format, entity, working_dir)

    def process_course_code_year_semester(self, course):
        match = re.search(REGEX_DMMH_CODE_A, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(1)  # 1 == Course code
            course[CONSPECTUS_COURSE_YEAR] = match.group(3)  # 3 == Course year
            course[CONSPECTUS_COURSE_SEMESTER] = match.group(4)  # 4 == Course semester
            return

        match = re.search(REGEX_DMMH_CODE_B, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(1)  # 1 == Course code
            course[CONSPECTUS_COURSE_YEAR] = match.group(3)  # 3 == Course year
            course[CONSPECTUS_COURSE_SEMESTER] = match.group(5)  # 5 == Course semester
            return

        match = re.search(REGEX_DMMH_CODE_C, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(1)  # 1 == Course code
            course[CONSPECTUS_COURSE_YEAR] = course[END_DATE].split('-')[0]
            course[CONSPECTUS_COURSE_SEMESTER] = get_semester_from_term(course)
            if course[CONSPECTUS_COURSE_SEMESTER] == UNKNOWN:
                guess_semester(course)
            return

        raise ValueError(COURSE_CODE_NOT_PROCESSABLE
                         .format(course[ID], course[CODE], str(REGEX_DMMH_CODE_C)))
