import regex as re

from lib.transformer.transformer import Transformer
from lib.utility.constants import MF, CONSPECTUS_COURSE_CODE, CONSPECTUS_COURSE_YEAR, CONSPECTUS_COURSE_SEMESTER, \
    CODE, REGEX_KLKU_PRE_CODE_YEAR_SEMESTER, COURSE_CODE_NOT_PROCESSABLE_REASON, ID, ASSUMED_TEST_COURSE


class MFTransformer(Transformer):

    def __init__(self, output_format, entity, working_dir):
        super().__init__(MF, output_format, entity, working_dir)

    def check_should_process(self, course):
        if course[CODE].lower().startswith("test"):
            raise ValueError(COURSE_CODE_NOT_PROCESSABLE_REASON
                             .format(course[ID], course[CODE], ASSUMED_TEST_COURSE))
        super().check_should_process(course)

    def process_course_code_year_semester(self, course):
        # KU_MAHE_2023_HØST, KU_SAKRIKO1_2022_HØST, KL_HELSELED60_2024_VÅR_SKIEN"
        match = re.search(REGEX_KLKU_PRE_CODE_YEAR_SEMESTER, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(2).strip()  # 2 == Course code
            course[CONSPECTUS_COURSE_YEAR] = match.group(3)  # 3 == Course year
            course[CONSPECTUS_COURSE_SEMESTER] = match.group(5)  # 5 == Course semester
            return
        # No known alternative approaches work, try the standard approach
        super().process_course_code_year_semester(course)
