import regex as re
import json
from os.path import join

from lib.transformer.transformer import Transformer
from lib.utility.constants import VALUE, DESC, USN, CONSPECTUS_COURSE_NAME, CONSPECTUS_COURSE_CODE, RESOURCES, \
    COURSE_MAPPING_FILE, COURSE_MAPPINGS, NAME, CONSPECTUS_COURSE_YEAR, CONSPECTUS_COURSE_SEMESTER, UNKNOWN, END_DATE, \
    CODE
from lib.utility.leganto_utility import guess_semester, get_semester_from_term


def find_group(match, param):
    for i in range(1, param):
        if match.group(i) is not None:
            return match.group(i)
    return None


class USNTransformer(Transformer):

    def __init__(self, output_format, entity, working_dir):
        super().__init__(USN, output_format, entity, working_dir)

    def process_course_code_year_semester(self, course):
        if course[CODE] == 'L\ufffdRMILJ\ufffd':
            course[CODE] = 'LÆRMILJØ'
        if course[CODE].endswith('KPRO1001'):
            course[CODE] = 'ØKPRO1001'
        elif course[CODE].endswith('SYK100'):
            course[CODE] = 'ØSYK100'
        elif course[CODE].endswith('101') and course[CODE].startswith('KR'):
            course[CODE] = 'KRØ 101'
        elif course[CODE].endswith('K2000') and course[CODE].startswith('FE'):
            course[CODE] = 'FE-EØK2000'
        elif course[CODE].endswith('1200') and course[CODE].startswith('MU - UT'):
            course[CODE] = 'MU-UTØ1200'

        if not course[CODE].startswith('UA_') and not course[CODE].startswith('UE_'):
            if course[CODE] == 'G2-MH2100':
                print("stop")
            if ' ' in course[CODE] or '-' in course[CODE]:
                regex_code_split = r'^(\d*\p{L}+\s\d+)|(\d*\p{L}+-\p{L}*\d+-?\p{L}*)|(\p{L}+\d+-\d+)|(\d+-\p{' \
                                   r'L}*\d+)|(\p{L}+\d+-\p{L}+\d*) '
                match = re.search(regex_code_split, course[CODE])
                if match is not None:
                    course[CONSPECTUS_COURSE_CODE] = find_group(match, 6)  # 5 groups in total 1 to 5
                    course[CONSPECTUS_COURSE_YEAR] = course[END_DATE].split('-')[0]
                    course[CONSPECTUS_COURSE_SEMESTER] = get_semester_from_term(course)
                    if course[CONSPECTUS_COURSE_SEMESTER] == UNKNOWN:
                        guess_semester(course)
                    return

            regex_code_split = r'(\p{L}+\d*\p{L}+\d*)|(\d+\p{L}*)|(\p{L}+\d*)'
            match = re.search(regex_code_split, course[CODE])
            if match is not None:
                course[CONSPECTUS_COURSE_CODE] = find_group(match, 4)  # 3 groups in total 1 to 4
                course[CONSPECTUS_COURSE_YEAR] = course[END_DATE].split('-')[0]
                course[CONSPECTUS_COURSE_SEMESTER] = get_semester_from_term(course)
                if course[CONSPECTUS_COURSE_SEMESTER] == UNKNOWN:
                    guess_semester(course)
                return
        # No known alternative approaches work, try the standard approach
        super().process_course_code_year_semester(course)

    def assign_course_name(self, course):
        # Match the following types of course names
        # Specialization Project - ING501 HØST 2023
        # The European Union - Institutions and Politics - ST-202 VÅR 2024
        match = re.match(r'(.*)-\s*(' + course[CONSPECTUS_COURSE_CODE] + r').*\d+$', course[NAME])
        if match is not None:
            course[CONSPECTUS_COURSE_NAME] = match.group(1).strip()
            return

        # Match the following types of course names
        # Discourse Analysis EN-467
        match = re.match(r'(.*)' + course[CONSPECTUS_COURSE_CODE] + '$', course[NAME])
        if match is not None:
            course[CONSPECTUS_COURSE_NAME] = match.group(1).strip()
            return

        super().assign_course_name(course)

    def pre_update_course(self, course):
        super(USNTransformer, self).pre_update_course(course)
        with open(join(RESOURCES, COURSE_MAPPINGS, COURSE_MAPPING_FILE.format(USN)), 'r') as mappings_file:
            course_mappings = json.loads(mappings_file.read())
            course_code = course['code']
            match = re.search(r"U[A|E]_\d+_([A-Za-z]*\s*\d*-*[A-Za-z]*\d*-*[A-Za-z]*\d*)_(.+)_(.+)_(.+)",
                              course['code'])
            if match is not None:
                course_code = match.group(1)

            if course_code not in course_mappings.keys():
                if 'academic_department' in course:
                    if 'value' in course['academic_department']:
                        if course['academic_department']['value'] == '' or \
                                course['academic_department']['value'] == 'UNDEFINED':
                            course['academic_department']['value'] = '222_0_0'
                    if 'desc' in course['academic_department']:
                        if course['academic_department']['desc'] == '':
                            course['academic_department']['desc'] = 'Universitetet i Sørøst-Norge'
                else:
                    course['academic_department']['value'] = '222_0_0'
                    course['academic_department']['desc'] = 'Universitetet i Sørøst-Norge'
                return

            if 'academic_department' in course:
                if 'value' in course['academic_department']:
                    if course['academic_department']['value'] == '':
                        course['academic_department']['value'] = course_mappings[course_code][0][VALUE]
                else:
                    course['academic_department']['value'] = course_mappings[course_code][0][VALUE]

                if 'desc' in course['academic_department']:
                    if course['academic_department']['desc'] == '':
                        course['academic_department']['desc'] = course_mappings[course_code][0][DESC]
                else:
                    course['academic_department']['desc'] = course_mappings[course_code][0][DESC]
            else:
                course['academic_department'] = {}
                course['academic_department']['value'] = course_mappings[course_code][0][VALUE]
                course['academic_department']['desc'] = course_mappings[course_code][0][DESC]

    def update_course(self, course):
        # Call super after the above happened
        super().update_course(course)

    def post_update_course(self, course):
        super().post_update_course(course)
