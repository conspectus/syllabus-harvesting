import regex as re

from lib.transformer.transformer import Transformer
from lib.utility.constants import VID, CONSPECTUS_COURSE_CODE, CONSPECTUS_COURSE_YEAR, CONSPECTUS_COURSE_SEMESTER, \
    CODE, REGEX_KLKU_PRE_CODE_YEAR_SEMESTER, UNKNOWN, START_DATE
from lib.utility.leganto_utility import get_semester_from_term, guess_semester


class VIDTransformer(Transformer):

    def __init__(self, output_format, entity, working_dir):
        super().__init__(VID, output_format, entity, working_dir)
        # I am not writing a regex for the following. Hopefully VID will have a more standardised
        # approach in the future. These appear to be manually set up
        self.manual_match = {
            'BASOB1020 22-H': 'BASOB1020',
            'BASP2030_Praksisforb_V23': 'BASOB1020',
            'BASP2101_1_2022_VÅR_1_ENG': 'BASP2101',
            'BASP3020/3030 Felles Intro Bergen 23H': 'BASP3020/3030',
            'BASP KULL 2021 BERGEN': 'BASP',
            'BASP kull 2022 Bergen': 'BASP',
            'BISB 2019': 'BISB',
            'BSPSFORD_MAN_20H': 'BSPSFORD',
            'Emne 5400 2023-V': '5400',
            'HPED1000 21H': 'HPED1000',
            'INTROMACOMM5140_2021_HØST': '5140',
            'LVRPP-BA': 'LVRPP-BA',
            'LVRPP-MA': 'LVRPP-MA',
            'MAKSP-BG5300 2021-H': 'BG5300',
            'Masos-417_Bereavement support': 'MASOS-417',
            'Masos-417_Bereavement support (2023-H)': 'MASOS-417',
            'MAVERD-434-1 23V Vit teo forsk met': 'MAVERD-434',
            'MAVERD_543_KONG_20H': 'MAVERD-543',
            'MAVERD_543_KONG_21H': 'MAVERD-543',
            'SP_PHD DVP': 'PHD DVP',
            'SP_VERNDEL': 'VERNDEL',
            'SP_VERNHEL': 'VERNHEL',
            'SP_VSAMK40504': 'VSAMK40504',
            'SP_VUKLOK': 'VUKLOK',
            'VER_VALGEMNER_20V': 'VALGEMNER',
            'VER_VALGEMNER_20V2': 'VALGEMNER',
            'VER_VALGEMNER_21V': 'VALGEMNER',
            'VFF': 'VFF',
            'VUPBU-PROSJEKT': 'VUPBU-PROSJEKT'
        }

    def process_course_code_year_semester(self, course):

        if course[CODE] in self.manual_match.keys():
            course[CONSPECTUS_COURSE_CODE] = self.manual_match[course[CODE]]
            course[CONSPECTUS_COURSE_YEAR] = course[START_DATE].split('-')[0]
            course[CONSPECTUS_COURSE_SEMESTER] = get_semester_from_term(course)
            if course[CONSPECTUS_COURSE_SEMESTER] == UNKNOWN:
                guess_semester(course)
            return

        # KL_VUKS_H_2020
        regex_klku_pre_code_year = r'^(KL|KK|KU)_(.*)_(\d\d\d\d)$'
        match = re.search(regex_klku_pre_code_year, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(2)  # 2 == Course code
            course[CONSPECTUS_COURSE_YEAR] = match.group(3)  # 3 == Course year
            course[CONSPECTUS_COURSE_SEMESTER] = get_semester_from_term(course)
            if course[CONSPECTUS_COURSE_SEMESTER] == UNKNOWN:
                guess_semester(course)
            return

        # KU_MAFAM_2021_HØST
        match = re.search(REGEX_KLKU_PRE_CODE_YEAR_SEMESTER, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(2)  # 2 == Course code
            course[CONSPECTUS_COURSE_YEAR] = match.group(3)  # 3 == Course year
            course[CONSPECTUS_COURSE_SEMESTER] = match.group(5)  # 5 == Course semester
            return

        # No known alternative approaches work, try the standard approach
        super().process_course_code_year_semester(course)
