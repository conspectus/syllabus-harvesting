import regex as re

from lib.transformer.transformer import Transformer
from lib.utility.constants import NMBU, CODE, CONSPECTUS_COURSE_CODE, CONSPECTUS_COURSE_YEAR, \
    CONSPECTUS_COURSE_SEMESTER, END_DATE, UNKNOWN
from lib.utility.leganto_utility import get_semester_from_term, guess_semester


def process_nmbu_course(match, course):
    course[CONSPECTUS_COURSE_CODE] = match.group(1).strip()  # 1 == Course code
    course[CONSPECTUS_COURSE_YEAR] = course[END_DATE].split('-')[0]
    course[CONSPECTUS_COURSE_SEMESTER] = get_semester_from_term(course)
    if course[CONSPECTUS_COURSE_SEMESTER] == UNKNOWN:
        guess_semester(course)


class NMBUTransformer(Transformer):

    def __init__(self, output_format, entity, working_dir):
        super().__init__(NMBU, output_format, entity, working_dir)

    def process_course_code_year_semester(self, course):
        # Two types if codes in nmbu, UE_... and variants of course code e.g.,
        # VETFORS361-4, ZOOL210
        if len(course[CODE]) < 15:
            regex_code_single = r'(\w+\d\d\d)'
            match = re.search(regex_code_single, course[CODE])
            if match is not None:
                process_nmbu_course(match, course)
                return

            regex_code_dash = r'(\w+\d*-\w+\d*-?\w*)'
            match = re.search(regex_code_dash, course[CODE])
            if match is not None:
                process_nmbu_course(match, course)
                return

            regex_code_single = r'(\w+\d*)'
            match = re.search(regex_code_single, course[CODE])
            if match is not None:
                process_nmbu_course(match, course)
                return

        # No known alternative approaches work, try the standard approach
        super().process_course_code_year_semester(course)
