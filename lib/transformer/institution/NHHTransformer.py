import regex as re

from lib.transformer.transformer import Transformer
from lib.utility.constants import NHH, CONSPECTUS_COURSE_CODE, CONSPECTUS_COURSE_YEAR, CONSPECTUS_COURSE_SEMESTER, \
    CODE, END_DATE, UNKNOWN
from lib.utility.leganto_utility import guess_semester, get_semester_from_term


class NHHTransformer(Transformer):

    def __init__(self, output_format, entity, working_dir):
        super().__init__(NHH, output_format, entity, working_dir)

    def process_course_code_year_semester(self, course):
        # INNOVASJON-BRONNOYSUND, INNOVASJON-MOLDE, INNOVASJON-MOLDE-24
        regex_innovasjon_code_year = r'^(INNOVASJON-\w+)-?\d?\d?'
        match = re.search(regex_innovasjon_code_year, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(1)  # 1 == Course code
            course[CONSPECTUS_COURSE_YEAR] = course[END_DATE].split('-')[0]
            course[CONSPECTUS_COURSE_SEMESTER] = get_semester_from_term(course)
            if course[CONSPECTUS_COURSE_SEMESTER] == UNKNOWN:
                guess_semester(course)
            return

        regex_innovasjon_code_year = r'^(DESIGNTHINKING)(\d\d\d\d)$'
        match = re.search(regex_innovasjon_code_year, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(1)  # 1 == Course code
            course[CONSPECTUS_COURSE_YEAR] = match.group(2)
            course[CONSPECTUS_COURSE_SEMESTER] = get_semester_from_term(course)
            if course[CONSPECTUS_COURSE_SEMESTER] == UNKNOWN:
                guess_semester(course)
            return

        # NHHEBRYTNING21 -> (NHHE)BRYTNING(21), NHHEREKTOR22, (NHHE)UELEDELSE3A(21)
        regex_nhhe_code_year = r'^N?H?E?E?(.+)(2\d)$'
        match = re.search(regex_nhhe_code_year, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(1)  # 1 == Course code
            if course[CONSPECTUS_COURSE_CODE].endswith('20'):
                course[CONSPECTUS_COURSE_CODE] = course[CONSPECTUS_COURSE_CODE][:-2]
            course[CONSPECTUS_COURSE_YEAR] = '20' + match.group(2)  # 2 == Course year
            course[CONSPECTUS_COURSE_SEMESTER] = get_semester_from_term(course)
            if course[CONSPECTUS_COURSE_SEMESTER] == UNKNOWN:
                guess_semester(course)
            return

        # No known alternative approaches work, try the standard approach
        super().process_course_code_year_semester(course)
