import regex as re

from lib.utility.leganto_constants import get_semester_name
from lib.transformer.transformer import Transformer
from lib.utility.constants import UiB, CONSPECTUS_COURSE_NAME, CONSPECTUS_COURSE_CODE, NAME, CONSPECTUS_COURSE_YEAR, \
    CONSPECTUS_COURSE_SEMESTER, CODE, REGEX_YEAR_SEMESTER_CODE_SECTION, CONSPECTUS_SECTION


def update_course_name_if_match(regex, course, group_index):
    match = re.match(regex, course[NAME])
    if match is not None:
        course[CONSPECTUS_COURSE_NAME] = match.group(group_index).strip()


class UiBTransformer(Transformer):

    def __init__(self, output_format, entity, working_dir):
        super().__init__(UiB, output_format, entity, working_dir)

    def process_course_code_year_semester(self, course):
        # 2021H-EXPHIL-JUSEM-0
        match = re.search(REGEX_YEAR_SEMESTER_CODE_SECTION, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_YEAR] = match.group(1)  # 1 == Course year
            course[CONSPECTUS_COURSE_SEMESTER] = get_semester_name(match.group(2))  # 2 == Course semester
            course[CONSPECTUS_COURSE_CODE] = match.group(3)  # 3 == Course code
            course[CONSPECTUS_SECTION] = match.group(4)  # 4 == Section
            return

        # See if super can handle it. Example UE_000_HIS202_1_2021_HØST_1, currently (2024 only 1, but adding this
        # so that later changes will be handled)
        super(UiBTransformer, self).process_course_code_year_semester(course)
