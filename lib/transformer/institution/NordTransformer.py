import regex as re

from lib.transformer.transformer import Transformer
from lib.utility.constants import NORD, CONSPECTUS_COURSE_CODE, CONSPECTUS_COURSE_YEAR, CONSPECTUS_COURSE_SEMESTER, \
    CODE, REGEX_KLKU_PRE_CODE_YEAR_SEMESTER


class NordTransformer(Transformer):

    def __init__(self, output_format, entity, working_dir):
        super().__init__(NORD, output_format, entity, working_dir)

    # Nord has some outlier courses that it is not clear how they should be processed
    # SP_ÅRLUFTLED, SP_BASOA_LEVANGER, SP_EVU-LEFIPE, SP_EVU-NOAS, SP_LEDKFB, SP_MASOSVIT
    # These will end up being ignored.
    def process_course_code_year_semester(self, course):
        match = re.search(REGEX_KLKU_PRE_CODE_YEAR_SEMESTER, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(2)  # 2 == Course code
            course[CONSPECTUS_COURSE_YEAR] = match.group(3)  # 3 == Course year
            course[CONSPECTUS_COURSE_SEMESTER] = match.group(5)  # 5 == Course semester
            return

        # No known alternative approaches work, try the standard approach
        super().process_course_code_year_semester(course)
