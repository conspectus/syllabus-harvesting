import regex as re

from lib.utility.leganto_constants import get_semester_name
from lib.transformer.transformer import Transformer
from lib.utility.constants import UiS, CONSPECTUS_COURSE_NAME, CONSPECTUS_COURSE_CODE, NAME, CONSPECTUS_COURSE_YEAR, \
    REGEX_PRE_CRISTIN_CODE_SECTION_YEAR_SEMESTER, COURSE_CODE_NOT_PROCESSABLE, ID, CONSPECTUS_COURSE_SEMESTER, CODE, \
    REGEX_CODE_SECTION_YEAR_SEMESTER, REGEX_PRE_CRISTIN_CODE_SECTION_SEMESTER_YEAR


def update_course_name_if_match(regex, course, group_index):
    match = re.match(regex, course[NAME])
    if match is not None:
        course[CONSPECTUS_COURSE_NAME] = match.group(group_index).strip()


class UiSTransformer(Transformer):

    def __init__(self, output_format, entity, working_dir):
        super().__init__(UiS, output_format, entity, working_dir)

    def pre_update_course(self, course):
        super(UiSTransformer, self).pre_update_course(course)

    def update_course(self, course):
        super().update_course(course)

    def post_update_course(self, course):
        super().post_update_course(course)

    def process_course_code_year_semester(self, course):
        # Do not call super(). Handling everything here.
        match_found = False
        # Handle e.g. UE_217_BSYPKI1_BSYPMEI1_BSYPPS1_BSYPKO1_1_2021_HOST_1
        updated_course_code = course[CODE].replace('_HOST', '_HØST')
        # Handle e.g. UE_217_BBAP20_BSOP20_1_2024_VAR_1 but avoid changing e.g. UE_217_VAR100_2_2020_HØST_1
        updated_course_code = updated_course_code.replace('_VAR_', '_VÅR_')
        # Handle e.g. UE_217_MGL2200matematikk _1_2022_VÅR_1
        updated_course_code = updated_course_code.replace(' _', '')

        # Match e.g. "UE_217_MPH140_1_V21_Traumatology in Pre Hospital Critical Care_2021_VÅR_1"
        match = re.search(REGEX_PRE_CRISTIN_CODE_SECTION_SEMESTER_YEAR, updated_course_code)
        if match is not None:
            match_found = True
            course[CONSPECTUS_COURSE_CODE] = match.group(2)  # 2 == Course code
            course[CONSPECTUS_COURSE_SEMESTER] = get_semester_name(match.group(4))  # 4 == Course semester
            course[CONSPECTUS_COURSE_YEAR] = '20' + match.group(5)  # 5 == Course year, only two digits

        # Match e.g. MGL2100_1_2022_HØST_1_MATEMATIKK
        match = re.search(REGEX_CODE_SECTION_YEAR_SEMESTER, updated_course_code)
        if match is not None:
            match_found = True
            course[CONSPECTUS_COURSE_CODE] = match.group(1)  # 1 == Course code
            course[CONSPECTUS_COURSE_YEAR] = match.group(3)  # 3 == Course year
            course[CONSPECTUS_COURSE_SEMESTER] = get_semester_name(match.group(4))  # 4 == Course semester

        # Regular match
        match = re.search(REGEX_PRE_CRISTIN_CODE_SECTION_YEAR_SEMESTER, updated_course_code)
        if match is not None:
            match_found = True
            course[CONSPECTUS_COURSE_CODE] = match.group(2)  # 2 == Course code
            course[CONSPECTUS_COURSE_YEAR] = match.group(4)  # 4 == Course year
            course[CONSPECTUS_COURSE_SEMESTER] = match.group(5)  # 5 == Course semester
            # Handle the course name UE_217_MGL2200_matematikk_1_2023_VÅR_1 being mixed up in the course code
            if '_' in course[CONSPECTUS_COURSE_CODE]:
                # It could be a merged set of courses, but normally these have upper case letters
                split_course_code = course[CONSPECTUS_COURSE_CODE].split('_')
                # Check to see if the second (1) split object has a first character that starts with a lower case letter
                if split_course_code[1][0].is_lower():
                    course[CONSPECTUS_COURSE_CODE] = split_course_code

        if not match_found:
            raise ValueError(COURSE_CODE_NOT_PROCESSABLE.format(course[ID], course[CODE],
                                                                str(REGEX_PRE_CRISTIN_CODE_SECTION_YEAR_SEMESTER)))
