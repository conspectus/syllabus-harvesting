import regex as re

from lib.transformer.transformer import Transformer
from lib.utility.constants import (FORSVARET, CONSPECTUS_COURSE_CODE, CONSPECTUS_COURSE_YEAR,
                                   CONSPECTUS_COURSE_SEMESTER, CODE, REGEX_FORSVARET_CODE, UNKNOWN, YEAR, START_DATE)
from lib.utility.leganto_utility import get_semester_from_term, guess_semester


class ForsvaretTransformer(Transformer):

    def __init__(self, output_format, entity, working_dir):
        super().__init__(FORSVARET, output_format, entity, working_dir)

    def process_course_code_year_semester(self, course):
        #
        #
        # currently no course[CODE] startswith 'UA_1627', but they might in the future so leaving the check here
        if not course[CODE].startswith('UE_1627') and \
                not course[CODE].startswith('UA_1627'):
            match = re.search(REGEX_FORSVARET_CODE, course[CODE])
            if match is not None:
                course[CONSPECTUS_COURSE_CODE] = match.group(1)  # 1 == Course code
                course[CONSPECTUS_COURSE_YEAR] = course[YEAR]
                if course[CONSPECTUS_COURSE_YEAR] == '':
                    course[CONSPECTUS_COURSE_YEAR] = course[START_DATE].split('-')[0]
                course[CONSPECTUS_COURSE_SEMESTER] = get_semester_from_term(course)
                if course[CONSPECTUS_COURSE_SEMESTER] == UNKNOWN:
                    guess_semester(course)
                course[CONSPECTUS_COURSE_CODE] = course[CONSPECTUS_COURSE_CODE].replace(' ', '')
                return
        # No known alternative approaches work, try the standard approach
        super().process_course_code_year_semester(course)
