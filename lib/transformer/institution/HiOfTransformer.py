import regex as re

from lib.transformer.transformer import Transformer
from lib.utility.constants import HIOF, CONSPECTUS_COURSE_CODE, CONSPECTUS_COURSE_YEAR, CONSPECTUS_COURSE_SEMESTER, \
    CODE, REGEX_KLKU_PRE_CODE_YEAR_SEMESTER, COURSE_CODE_NOT_PROCESSABLE_REASON, ID, ASSUMED_TEST_COURSE


class HIOFTransformer(Transformer):

    def __init__(self, output_format, entity, working_dir):
        super().__init__(HIOF, output_format, entity, working_dir)

    def check_should_process(self, course):
        if course[CODE].lower().startswith("man_netped") or \
                course[CODE].lower().startswith("man_nettped"):
            raise ValueError(COURSE_CODE_NOT_PROCESSABLE_REASON
                             .format(course[ID], course[CODE], ASSUMED_TEST_COURSE))
        super().check_should_process(course)

    def process_course_code_year_semester(self, course):
        # KU_PEDVEILM_2023_HØST, KU_PPDY_2021_HØST
        match = re.search(REGEX_KLKU_PRE_CODE_YEAR_SEMESTER, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(2)  # 2 == Course code
            course[CONSPECTUS_COURSE_YEAR] = match.group(3)  # 3 == Course year
            course[CONSPECTUS_COURSE_SEMESTER] = match.group(5)  # 5 == Course semester
            return
        # No known alternative approaches work, try the standard approach
        super().process_course_code_year_semester(course)
