import regex as re

from lib.transformer.transformer import Transformer
from lib.utility.constants import NLA, CONSPECTUS_COURSE_CODE, CONSPECTUS_COURSE_YEAR, CONSPECTUS_COURSE_SEMESTER, \
    CODE, REGEX_PRE_CODE_SECTION_YEAR_SEMESTER, CONSPECTUS_SECTION


class NLATransformer(Transformer):

    def __init__(self, output_format, entity, working_dir):
        super().__init__(NLA, output_format, entity, working_dir)

    def process_course_code_year_semester(self, course):
        if 'UA_254' not in course[CODE] and 'UE_254' not in course[CODE]:
            match = re.search(REGEX_PRE_CODE_SECTION_YEAR_SEMESTER, course[CODE])
            if match is not None:
                course[CONSPECTUS_COURSE_CODE] = match.group(2)  # 2 == Course code
                course[CONSPECTUS_COURSE_YEAR] = match.group(4)  # 4 == Course year
                course[CONSPECTUS_COURSE_SEMESTER] = match.group(5)  # 5 == Course semester
                course[CONSPECTUS_SECTION] = match.group(3)  # 3 == Section
                return
        # No known alternative approaches work, try the standard approach
        super().process_course_code_year_semester(course)
