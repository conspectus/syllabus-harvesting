import regex as re

from lib.transformer.transformer import Transformer
from lib.utility.constants import AHO, CONSPECTUS_COURSE_CODE, CONSPECTUS_COURSE_YEAR, CONSPECTUS_COURSE_SEMESTER, \
    CODE, REGEX_AHO_CODE, END_DATE, UNKNOWN
from lib.utility.leganto_utility import guess_semester, get_semester_from_term


class AHOTransformer(Transformer):

    def __init__(self, output_format, entity, working_dir):
        super().__init__(AHO, output_format, entity, working_dir)

    def process_course_code_year_semester(self, course):
        # "40 140(2020)", 70 600 (2020), 54321
        # "40 521/40 522"
        match = re.search(REGEX_AHO_CODE, course[CODE])
        if match is not None:
            course[CONSPECTUS_COURSE_CODE] = match.group(1)  # 1 == Course code
            course[CONSPECTUS_COURSE_YEAR] = course[END_DATE].split('-')[0]
            course[CONSPECTUS_COURSE_SEMESTER] = get_semester_from_term(course)
            if course[CONSPECTUS_COURSE_SEMESTER] == UNKNOWN:
                guess_semester(course)
            # There is a single instance of space missing in a course code
            if course[CONSPECTUS_COURSE_CODE] == '60150':
                course[CONSPECTUS_COURSE_CODE] = '60 150'
            return

        # No known alternative approaches work, try the standard approach
        super().process_course_code_year_semester(course)
