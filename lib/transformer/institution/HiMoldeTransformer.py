import regex as re

from lib.transformer.transformer import Transformer
from lib.utility.constants import HIMOLDE, CONSPECTUS_COURSE_CODE, CONSPECTUS_COURSE_YEAR, CONSPECTUS_COURSE_SEMESTER, \
    CODE, REGEX_HIMOLDE_CODE, END_DATE, UNKNOWN, REGEX_HIMOLDE_CODE_SINGLE_WORD
from lib.utility.leganto_utility import guess_semester, get_semester_from_term


class HiMoldeTransformer(Transformer):

    def __init__(self, output_format, entity, working_dir):
        super().__init__(HIMOLDE, output_format, entity, working_dir)

    def process_course_code_year_semester(self, course):

        # 15 because a course code that is less than 15 characters looks like BOLK_2020, BØK311, BØK205 18V. If it is
        # greater than 15 chracters it is probably a standard code like UA_211_ADM100_1_2022_HØST_1_K
        if len(course[CODE]) < 15:
            match = re.search(REGEX_HIMOLDE_CODE, course[CODE])
            if match is not None:
                course[CONSPECTUS_COURSE_CODE] = match.group(1).strip()  # 1 == Course code
                course[CONSPECTUS_COURSE_YEAR] = course[END_DATE].split('-')[0]
                course[CONSPECTUS_COURSE_SEMESTER] = get_semester_from_term(course)
                if course[CONSPECTUS_COURSE_SEMESTER] == UNKNOWN:
                    guess_semester(course)
                return
            match = re.search(REGEX_HIMOLDE_CODE_SINGLE_WORD, course[CODE])
            if match is not None:
                course[CONSPECTUS_COURSE_CODE] = match.group(1)  # 1 == Course code
                course[CONSPECTUS_COURSE_YEAR] = course[END_DATE].split('-')[0]
                course[CONSPECTUS_COURSE_SEMESTER] = get_semester_from_term(course)
                if course[CONSPECTUS_COURSE_SEMESTER] == UNKNOWN:
                    guess_semester(course)
                return
        # No known alternative approaches work, try the standard approach
        super().process_course_code_year_semester(course)

