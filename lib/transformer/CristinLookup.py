import json

import pandas as pd

from lib.utility.constants import CRISTIN_LEGANTO_MAPPING_FILE, LEGANTO_DEPT_NAME, ORGS, \
    UNIT_NAME_EN, SUB_UNIT_NAME_NB, UNIT_NAME_NB, SUB_UNIT_NAME_EN, SUB_SUB_UNIT_NAME_NB, SUB_SUB_UNIT_NAME_EN, \
    UNIT_CODE, SUB_UNIT_CODE, SUB_SUB_UNIT_CODE, PROCESSING_COMMENT, PROPOSED_CRISTIN_CODE, LEGANTO_DEPT_CODE, \
    ACADEMIC_DEPARTMENT, VALUE, DESC, CONSPECTUS_USE_UNIT_CODE, CONSPECTUS_USE_UNIT_NAME_NB, \
    CONSPECTUS_USE_UNIT_NAME_EN, UNKNOWN

EMPTY_CRISTIN_OBJECT = {
    LEGANTO_DEPT_CODE: '',
    LEGANTO_DEPT_NAME: '',
    PROPOSED_CRISTIN_CODE: '',
    SUB_SUB_UNIT_CODE: '',
    SUB_UNIT_CODE: '',
    UNIT_CODE: '',
    SUB_SUB_UNIT_NAME_EN: '',
    SUB_SUB_UNIT_NAME_NB: '',
    SUB_UNIT_NAME_EN: '',
    SUB_UNIT_NAME_NB: '',
    UNIT_NAME_EN: '',
    UNIT_NAME_NB: '',
    PROCESSING_COMMENT: ''
}


def get_value(series):
    for key in series:
        return series[key]


def process_cristin_details(row, course):
    # Make a default object
    cristin_details = {}
    for key in row:
        cristin_details[key] = get_value(row[key])

    if DESC in course[ACADEMIC_DEPARTMENT].keys():
        cristin_details[LEGANTO_DEPT_NAME] = course[ACADEMIC_DEPARTMENT][DESC]

    if cristin_details[CONSPECTUS_USE_UNIT_CODE] == cristin_details[UNIT_CODE]:
        cristin_details[CONSPECTUS_USE_UNIT_NAME_NB] = cristin_details[UNIT_NAME_NB]
        cristin_details[CONSPECTUS_USE_UNIT_NAME_EN] = cristin_details[UNIT_NAME_EN]
    elif cristin_details[CONSPECTUS_USE_UNIT_CODE] == cristin_details[SUB_UNIT_CODE]:
        cristin_details[CONSPECTUS_USE_UNIT_NAME_NB] = cristin_details[SUB_UNIT_NAME_NB]
        cristin_details[CONSPECTUS_USE_UNIT_NAME_EN] = cristin_details[SUB_UNIT_NAME_EN]
    elif cristin_details[CONSPECTUS_USE_UNIT_CODE] == cristin_details[SUB_SUB_UNIT_CODE]:
        cristin_details[CONSPECTUS_USE_UNIT_NAME_NB] = cristin_details[SUB_SUB_UNIT_NAME_NB]
        cristin_details[CONSPECTUS_USE_UNIT_NAME_EN] = cristin_details[SUB_SUB_UNIT_NAME_EN]
    else:
        cristin_details[CONSPECTUS_USE_UNIT_NAME_NB] = UNKNOWN
        cristin_details[CONSPECTUS_USE_UNIT_NAME_EN] = UNKNOWN

    return cristin_details


class CRISTINLookup:

    def __init__(self, organisation):
        self.organisation = organisation
        self.df_cristin = pd.read_csv(CRISTIN_LEGANTO_MAPPING_FILE.format(organisation))

    def get_cristin_details_from_institute_name(self, institute_name, course):

        if institute_name.startswith('Fa') or institute_name.startswith('Ha') or \
                institute_name == '' or institute_name.startswith('Sen'):
            return
        cristin_data = json.loads(self.df_cristin.loc[
            self.df_cristin[SUB_SUB_UNIT_NAME_NB] == institute_name
            ].to_json())
        if cristin_data[LEGANTO_DEPT_CODE] == {}:
            cristin_data = json.loads(self.df_cristin.loc[
                self.df_cristin[SUB_UNIT_NAME_NB] == institute_name
                ].to_json())

        cristin_object = process_cristin_details(cristin_data, course)
        return cristin_object

    def get_cristin_details_from_leganto_course(self, course):
        cristin_object = EMPTY_CRISTIN_OBJECT

        if ACADEMIC_DEPARTMENT in course.keys() and \
                VALUE in course[ACADEMIC_DEPARTMENT].keys():
            cristin_object = process_cristin_details(
                json.loads(
                    self.df_cristin.loc[
                        self.df_cristin[LEGANTO_DEPT_CODE] == course[ACADEMIC_DEPARTMENT][VALUE]].to_json()),
                course)

        if cristin_object[UNIT_CODE] == '':
            cristin_object = process_cristin_details(
                json.loads(
                    self.df_cristin.loc[
                        self.df_cristin[CONSPECTUS_USE_UNIT_CODE] == ORGS[self.organisation][VALUE]].to_json()),
                course)

        # Otherwise, return an empty object
        return cristin_object
