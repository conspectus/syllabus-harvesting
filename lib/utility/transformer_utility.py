import logging
import regex as re

from lib.utility.constants import CONSPECTUS_COURSE_CODE, TERM, DESC, VALUE, CODE, CONSPECTUS_TERM_NAME_1, UNKNOWN, \
    YEAR, CONSPECTUS_TERM_NAME_2, CONSPECTUS_TERM_CODE_2, CONSPECTUS_TERM_CODE_1, CONSPECTUS_TERM_NAME_3, \
    CONSPECTUS_TERM_CODE_3, DBH_INSTITUTION_CODE_EN, DBH_INSTITUTION_NAME_EN, DBH_DEPT_CODE_EN, DBH_DEPT_NAME_EN, \
    DBH_DEPT_CODE_SSB_EN, DBH_YEAR_EN, DBH_SEMESTER_EN, DBH_SEMESTER_NAME_EN, DBH_COURSE_STUDY_PROGRAM_CODE_EN, \
    DBH_COURSE_STUDY_PROGRAM_NAME_EN, DBH_COURSE_CODE_EN, DBH_COURSE_NAME_EN, DBH_LEVEL_CODE_EN, DBH_LEVEL_NAME_EN, \
    DBH_COURSE_CREDIT_EN, DBH_NUS_CODE_EN, DBH_STATUS_EN, DBH_STATUS_NAME_EN, DBH_LANGUAGE_EN, DBH_NAME_EN, \
    DBH_AREA_CODE_EN, DBH_AREA_NAME_EN, DBH_TASK_EN, LEGANTO_DEPT_CODE, CONSPECTUS_USE_UNIT_CODE, LEGANTO_DEPT_NAME, \
    UNIT_CODE, \
    UNIT_NAME_NB, UNIT_NAME_EN, SUB_UNIT_CODE, SUB_UNIT_NAME_NB, SUB_UNIT_NAME_EN, SUB_SUB_UNIT_CODE, \
    SUB_SUB_UNIT_NAME_NB, SUB_SUB_UNIT_NAME_EN, ORGANISATION, CONSPECTUS_COURSE_NAME, PROCESSING_COMMENT, COURSE_YEAR, \
    CONSPECTUS_YEAR, CONSPECTUS_SEMESTER_CODE_1, CONSPECTUS_SEMESTER_CODE_2, CONSPECTUS_SEMESTER_CODE_3, \
    ACADEMIC_DEPARTMENT_CODE, ACADEMIC_DEPARTMENT_NAME, READING_LIST_ID, COURSE_ID, COURSE_LEGANTO_CODE, \
    CONSPECTUS_COURSE_YEAR, CONSPECTUS_COURSE_SEMESTER, CONSPECTUS_USE_UNIT_NAME_NB, CONSPECTUS_USE_UNIT_NAME_EN
from lib.utility.leganto_constants import semester_to_english, merge_citation_object_with_course
from lib.utility.string_utility import tidy_string

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)


def assign_course_code(course, match):
    if match is not None and len(match.groups()) >= 6:
        course_code = match.group(3)
        course[CONSPECTUS_COURSE_CODE] = course_code


def assign_course_year(course, match):
    if course[YEAR] is None:
        course[YEAR] = match.group(5)
    elif len(course[YEAR]) < 1:
        course[YEAR] = match.group(5)


def assign_semester(course, match):
    # Term is missing so pull it from the regex from leganto code
    if TERM not in course.keys():
        course_semester = match.group(6).lower()
        course[TERM] = []
        if course_semester == 'vår':
            course[TERM].append({DESC: course_semester, VALUE: 'spring'})
        elif course_semester == 'høst':
            course[TERM].append({DESC: course_semester, VALUE: 'autumn'})
        elif course_semester == 'som':
            course[TERM].append({DESC: course_semester, VALUE: 'summer'})
    else:
        # Make sure only english words are used for semester values
        for term_count in range(0, len(course[TERM])):
            course[TERM][term_count][DESC] = semester_to_english(course[TERM][
                                                                     term_count][DESC])
            course[TERM][term_count][VALUE] = semester_to_english(course[TERM][
                                                                      term_count][VALUE])


def assign_course_code_year_semester(course):
    course[CONSPECTUS_COURSE_CODE] = course[CODE]
    # Sometimes (rarely) searchable_id is not present so try with the code value
    list_searchable_id = [course['course_leganto_code']]

    if course['course_leganto_code'].count("_") < 6 and 'searchable_id' in course.keys():
        list_searchable_id = course['searchable_id']

    for i in range(len(list_searchable_id)):
        searchable_id = list_searchable_id[i]
        # Some cases where additional _1, _2 are present
        if searchable_id.count("_") > 5:
            match = None
            if searchable_id.count("_") == 6:
                match = re.search(r"(.+)_(.+)_(.+)_(.+)_(.+)_(.+)_(.+)", searchable_id)
            elif searchable_id.count("_") == 7:
                match = re.search(r"(.+)_(.+)_(.+)_(.+)_(.+)_(.+)_(.+)_(.+)", searchable_id)

            if match is not None:
                assign_course_code(course, match)
                assign_course_year(course, match)
                assign_semester(course, match)
            else:
                logging.info("**** Problem processing searchable_id " + searchable_id)


def remove_values(leganto_object, key):
    if key in leganto_object.keys():
        del [key]


def remove_course_values(course):
    # Remove links as target json file should not contain any
    # reference to leganto internal links
    remove_values(course, 'link')



def remove_citation_values(citation):
    # Remove links as target json file should not contain any
    # reference to leganto internal links
    remove_values(citation, 'link')


def tidy_course_values(course):
    # Remove any double, single quotes or whitespace at start or end of string
    if course['name'] is not None:
        course['name'] = tidy_string(course['name'])
    # Prefer to have an empty string than None
    else:
        course['name'] = ''

    # Some course years have ".0" e.g. 2024.0
    if course[YEAR].endswith(".0"):
        course[YEAR] = course[YEAR][:-2]
    if course[YEAR].endswith("."):
        course[YEAR] = course[YEAR][:-1]


def process_citations(course, reading_list_id, citations):
    merge_citation_object_with_course(course, reading_list_id, citations)
    citations.update()
    remove_citation_values(citations)


def set_reading_list_unknown(reading_list):
    reading_list[CONSPECTUS_TERM_NAME_1] = UNKNOWN
    reading_list[CONSPECTUS_TERM_CODE_1] = UNKNOWN
    reading_list[CONSPECTUS_TERM_NAME_2] = UNKNOWN
    reading_list[CONSPECTUS_TERM_CODE_2] = UNKNOWN
    reading_list[CONSPECTUS_TERM_NAME_3] = UNKNOWN
    reading_list[CONSPECTUS_TERM_CODE_3] = UNKNOWN


def do_checks(course):
    pass


# Allow for the ability to have common functionality when processing an individual citation.
# Currently, we delete any URLs that point to internal resources
def process_citation(citation):
    pass

def merge_citation_with_parent(citation, citations):
    citation[ORGANISATION] = citations[ORGANISATION]
    citation[CONSPECTUS_COURSE_NAME] = citations[CONSPECTUS_COURSE_NAME]
    citation[READING_LIST_ID] = citations[READING_LIST_ID]
    citation[COURSE_LEGANTO_CODE] = citations[COURSE_LEGANTO_CODE]
    citation[COURSE_ID] = citations[COURSE_ID]
    if PROCESSING_COMMENT in citations.keys():
        citation[PROCESSING_COMMENT] = citations[PROCESSING_COMMENT]
    else:
        citation[PROCESSING_COMMENT] = ''
    citation[CONSPECTUS_TERM_NAME_1] = citations[CONSPECTUS_TERM_NAME_1]
    citation[CONSPECTUS_TERM_CODE_1] = citations[CONSPECTUS_TERM_CODE_1]
    citation[CONSPECTUS_TERM_NAME_2] = citations[CONSPECTUS_TERM_NAME_2]
    citation[CONSPECTUS_TERM_CODE_2] = citations[CONSPECTUS_TERM_CODE_2]
    citation[CONSPECTUS_TERM_NAME_3] = citations[CONSPECTUS_TERM_NAME_3]
    citation[CONSPECTUS_TERM_CODE_3] = citations[CONSPECTUS_TERM_CODE_3]
    citation[CONSPECTUS_SEMESTER_CODE_1] = citations[CONSPECTUS_SEMESTER_CODE_1]
    citation[CONSPECTUS_SEMESTER_CODE_2] = citations[CONSPECTUS_SEMESTER_CODE_2]
    citation[CONSPECTUS_SEMESTER_CODE_3] = citations[CONSPECTUS_SEMESTER_CODE_3]
    citation[CONSPECTUS_COURSE_YEAR] = citations[CONSPECTUS_COURSE_YEAR]
    citation[CONSPECTUS_COURSE_SEMESTER] = citations[CONSPECTUS_COURSE_SEMESTER]
    citation[ACADEMIC_DEPARTMENT_CODE] = citations[ACADEMIC_DEPARTMENT_CODE]
    citation[ACADEMIC_DEPARTMENT_NAME] = citations[ACADEMIC_DEPARTMENT_NAME]
    citation[CONSPECTUS_YEAR] = citations[COURSE_YEAR]
    citation[PROCESSING_COMMENT] = citations[PROCESSING_COMMENT]
    citation[CONSPECTUS_COURSE_CODE] = citations[CONSPECTUS_COURSE_CODE]
    citation[LEGANTO_DEPT_CODE] = citations[LEGANTO_DEPT_CODE]
    citation[CONSPECTUS_USE_UNIT_CODE] = citations[CONSPECTUS_USE_UNIT_CODE]
    citation[CONSPECTUS_USE_UNIT_NAME_NB] = citations[CONSPECTUS_USE_UNIT_NAME_NB]
    citation[CONSPECTUS_USE_UNIT_NAME_EN] = citations[CONSPECTUS_USE_UNIT_NAME_EN]
    citation[LEGANTO_DEPT_NAME] = citations[LEGANTO_DEPT_NAME]
    citation[UNIT_CODE] = citations[UNIT_CODE]
    citation[UNIT_NAME_NB] = citations[UNIT_NAME_NB]
    citation[UNIT_NAME_EN] = citations[UNIT_NAME_EN]
    citation[SUB_UNIT_CODE] = citations[SUB_UNIT_CODE]
    citation[SUB_UNIT_NAME_NB] = citations[SUB_UNIT_NAME_NB]
    citation[SUB_UNIT_NAME_EN] = citations[SUB_UNIT_NAME_EN]
    citation[SUB_SUB_UNIT_CODE] = citations[SUB_SUB_UNIT_CODE]
    citation[SUB_SUB_UNIT_NAME_NB] = citations[SUB_SUB_UNIT_NAME_NB]
    citation[SUB_SUB_UNIT_NAME_EN] = citations[SUB_SUB_UNIT_NAME_EN]
    citation[DBH_INSTITUTION_CODE_EN] = citations[DBH_INSTITUTION_CODE_EN]
    citation[DBH_INSTITUTION_NAME_EN] = citations[DBH_INSTITUTION_NAME_EN]
    citation[DBH_DEPT_CODE_EN] = citations[DBH_DEPT_CODE_EN]
    citation[DBH_DEPT_NAME_EN] = citations[DBH_DEPT_NAME_EN]
    citation[DBH_DEPT_CODE_SSB_EN] = citations[DBH_DEPT_CODE_SSB_EN]
    citation[DBH_YEAR_EN] = citations[DBH_YEAR_EN]
    citation[DBH_SEMESTER_EN] = citations[DBH_SEMESTER_EN]
    citation[DBH_SEMESTER_NAME_EN] = citations[DBH_SEMESTER_NAME_EN]
    citation[DBH_COURSE_STUDY_PROGRAM_CODE_EN] = citations[DBH_COURSE_STUDY_PROGRAM_CODE_EN]
    citation[DBH_COURSE_STUDY_PROGRAM_NAME_EN] = citations[DBH_COURSE_STUDY_PROGRAM_NAME_EN]
    citation[DBH_COURSE_CODE_EN] = citations[DBH_COURSE_CODE_EN]
    citation[DBH_COURSE_NAME_EN] = citations[DBH_COURSE_NAME_EN]
    citation[DBH_LEVEL_CODE_EN] = citations[DBH_LEVEL_CODE_EN]
    citation[DBH_LEVEL_NAME_EN] = citations[DBH_LEVEL_NAME_EN]
    citation[DBH_COURSE_CREDIT_EN] = citations[DBH_COURSE_CREDIT_EN]
    citation[DBH_NUS_CODE_EN] = citations[DBH_NUS_CODE_EN]
    citation[DBH_STATUS_EN] = citations[DBH_STATUS_EN]
    citation[DBH_STATUS_NAME_EN] = citations[DBH_STATUS_NAME_EN]
    citation[DBH_LANGUAGE_EN] = citations[DBH_LANGUAGE_EN]
    citation[DBH_NAME_EN] = citations[DBH_NAME_EN]
    citation[DBH_AREA_CODE_EN] = citations[DBH_AREA_CODE_EN]
    citation[DBH_AREA_NAME_EN] = citations[DBH_AREA_NAME_EN]
    citation[DBH_TASK_EN] = citations[DBH_TASK_EN]
