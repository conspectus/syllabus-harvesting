

def tidy_string(value):
    value = value.strip()
    if value.startswith('"') or value.startswith("'"):
        value = value[1:len(value)]
    if value.endswith('"') or value.endswith("'"):
        value = value[:-1]
    return value
