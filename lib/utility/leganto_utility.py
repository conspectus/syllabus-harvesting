import regex as re

from lib.utility.constants import get_string_value_sub_dict, CONSPECTUS_USE_UNIT_CODE, UNIT_CODE, UNIT_NAME_NB, UNIT_NAME_EN, \
    SUB_UNIT_CODE, SUB_UNIT_NAME_NB, SUB_UNIT_NAME_EN, SUB_SUB_UNIT_CODE, SUB_SUB_UNIT_NAME_NB, SUB_SUB_UNIT_NAME_EN, \
    ID, NB, EN, REGEX_GROUP_GET_ID, LABELS, SUB_SUB_SUB_UNIT_CODE, SUB_SUB_SUB_UNIT_NAME_NB, SUB_SUB_SUB_UNIT_NAME_EN, \
    TERM, UNKNOWN, VALUE, START_DATE, CONSPECTUS_COURSE_SEMESTER
from lib.utility.leganto_constants import fix_dept_value, org_information, from_value_get_name_uit


def find_org_value_information(organisation):
    for organisation_idx in org_information:
        if organisation in organisation_idx.keys():
            return organisation_idx[organisation]['id']
    raise ValueError('Cannot continue as [' + organisation + '] is unknown')


def find_org_desc_information(organisation):
    for organisation_idx in org_information:
        if organisation in organisation_idx.keys():
            return organisation_idx[organisation]['name']
    raise ValueError('Cannot continue as [' + organisation + '] is unknown')


def get_org_object_unit(org_data):
    return get_org_object(get_id_from_url(org_data[ID]),
                          get_id_from_url(org_data[ID]),
                          get_unit_name(org_data, NB),
                          get_unit_name(org_data, EN))


def get_id_from_url(url):
    re_groups = re.search(REGEX_GROUP_GET_ID, url)
    return re_groups.group(1)


def get_unit_name(unit, language):
    if LABELS in unit.keys():
        if language in unit[LABELS].keys():
            return unit[LABELS][language]
    return ''


def get_org_object_sub_unit(org_data, sub_unit_data):
    org_object = get_org_object(get_id_from_url(sub_unit_data[ID]),
                                get_id_from_url(org_data[ID]),
                                get_unit_name(org_data, NB),
                                get_unit_name(org_data, EN),
                                get_id_from_url(sub_unit_data[ID]),
                                get_unit_name(sub_unit_data, NB),
                                get_unit_name(sub_unit_data, EN))
    return org_object


def get_org_object_sub_sub_unit(org_data, sub_unit_data, sub_sub_unit_data):
    org_object = get_org_object(get_id_from_url(sub_sub_unit_data[ID]),
                                get_id_from_url(org_data[ID]),
                                get_unit_name(org_data, NB),
                                get_unit_name(org_data, EN),
                                get_id_from_url(sub_unit_data[ID]),
                                get_unit_name(sub_unit_data, NB),
                                get_unit_name(sub_unit_data, EN),
                                get_id_from_url(sub_sub_unit_data[ID]),
                                get_unit_name(sub_sub_unit_data, NB),
                                get_unit_name(sub_sub_unit_data, EN))
    return org_object


def get_org_object_sub_sub_sub_unit(org_data, sub_unit_data, sub_sub_unit_data, sub_sub_sub_unit_data):
    org_object = get_org_object(get_id_from_url(sub_sub_unit_data[ID]),
                                get_id_from_url(org_data[ID]),
                                get_unit_name(org_data, NB),
                                get_unit_name(org_data, EN),
                                get_id_from_url(sub_unit_data[ID]),
                                get_unit_name(sub_unit_data, NB),
                                get_unit_name(sub_unit_data, EN),
                                get_id_from_url(sub_sub_unit_data[ID]),
                                get_unit_name(sub_sub_unit_data, NB),
                                get_unit_name(sub_sub_unit_data, EN),
                                get_id_from_url(sub_sub_sub_unit_data[ID]),
                                get_unit_name(sub_sub_sub_unit_data, NB),
                                get_unit_name(sub_sub_sub_unit_data, EN))
    return org_object


def get_org_object(conspectus_use_unit_code, unit_code, unit_name_nb, unit_name_en,
                   sub_unit_code='', sub_unit_name_nb='', sub_unit_name_en='',
                   sub_sub_unit_code='', sub_sub_unit_name_nb='', sub_sub_unit_name_en='',
                   sub_sub_sub_unit_code='', sub_sub_sub_unit_name_nb='', sub_sub_sub_unit_name_en=''):
    return {
        CONSPECTUS_USE_UNIT_CODE: conspectus_use_unit_code,
        UNIT_CODE: unit_code,
        UNIT_NAME_NB: unit_name_nb,
        UNIT_NAME_EN: unit_name_en,
        SUB_UNIT_CODE: sub_unit_code,
        SUB_UNIT_NAME_NB: sub_unit_name_nb,
        SUB_UNIT_NAME_EN: sub_unit_name_en,
        SUB_SUB_UNIT_CODE: sub_sub_unit_code,
        SUB_SUB_UNIT_NAME_NB: sub_sub_unit_name_nb,
        SUB_SUB_UNIT_NAME_EN: sub_sub_unit_name_en,
        SUB_SUB_SUB_UNIT_CODE: sub_sub_sub_unit_code,
        SUB_SUB_SUB_UNIT_NAME_NB: sub_sub_sub_unit_name_nb,
        SUB_SUB_SUB_UNIT_NAME_EN: sub_sub_sub_unit_name_en
    }


def get_semester_from_term(course):
    if TERM in course.keys():
        if len(course[TERM]) > 0:
            if course[TERM][0][VALUE].lower().startswith('a'):
                return 'HØST'
            elif course[TERM][0][VALUE].lower().startswith('s'):
                return 'VÅR'
            elif course[TERM][0][VALUE].lower().startswith('y'):
                return 'ÅRLIG'
    return UNKNOWN


def guess_semester(course):
    # Guess semester based on start dates
    month = int(course[START_DATE].split('-')[1])
    if (month >= 1 and month <= 3) or month >= 11:
        course[CONSPECTUS_COURSE_SEMESTER] = 'Vår'
    elif month >= 4 and month <= 10:
        course[CONSPECTUS_COURSE_SEMESTER] = 'Høst'