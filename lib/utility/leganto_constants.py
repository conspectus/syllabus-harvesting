""" Utility functions to improve readability. The code here is related to the processing of
   leganto sources"""
import json
import logging
from hashlib import sha256
from xml.sax.saxutils import escape

from lib.utility.constants import get_string_value_dict, get_numeric_value_dict, get_string_value_sub_dict, \
    get_boolean_value_dict, get_string_value_sub_sub_dict, get_boolean_value_sub_dict, \
    CONSPECTUS_TERM_CODE_1, CONSPECTUS_TERM_CODE_2, CONSPECTUS_TERM_CODE_3, CONSPECTUS_SEMESTER_CODE_1, \
    CONSPECTUS_SEMESTER_CODE_2, CONSPECTUS_SEMESTER_CODE_3, \
    DBH_INSTITUTION_CODE_EN, DBH_INSTITUTION_NAME_EN, DBH_DEPT_CODE_EN, DBH_DEPT_NAME_EN, DBH_DEPT_CODE_SSB_EN, \
    DBH_YEAR_EN, DBH_SEMESTER_EN, DBH_SEMESTER_NAME_EN, DBH_COURSE_STUDY_PROGRAM_CODE_EN, \
    DBH_COURSE_STUDY_PROGRAM_NAME_EN, DBH_COURSE_CODE_EN, DBH_COURSE_NAME_EN, DBH_LEVEL_CODE_EN, DBH_LEVEL_NAME_EN, \
    DBH_COURSE_CREDIT_EN, DBH_NUS_CODE_EN, DBH_STATUS_EN, DBH_STATUS_NAME_EN, DBH_NAME_EN, DBH_AREA_CODE_EN, \
    DBH_AREA_NAME_EN, DBH_TASK_EN, DBH_LANGUAGE_EN, CODE, CONSPECTUS_COURSE_CODE, MISSING_TERM, TERM, \
    VALUE, MULTIPLE_SEMESTER, CONSPECTUS_TERM_NAME_1, CONSPECTUS_TERM_NAME_2, CONSPECTUS_TERM_NAME_3, DESC, YEAR, \
    PROCESSING_COMMENT, LEGANTO_DEPT_CODE, LEGANTO_DEPT_NAME, COURSE_LEGANTO_CODE, CONSPECTUS_COURSE_NAME, COURSE_YEAR, \
    COURSE_CODE_NAME, NAME, ID, ORGANISATION, READING_LIST_ID, ACADEMIC_DEPARTMENT, COURSE_ID, CONSPECTUS_YEAR, \
    ACADEMIC_DEPARTMENT_CODE, ACADEMIC_DEPARTMENT_NAME, CONSPECTUS_COURSE_YEAR, CONSPECTUS_COURSE_SEMESTER, OSLOMET, \
    USN, UiB, NORD, UiO, AHO, START_DATE, END_DATE, DMMH, LEGANTO_AUTUMN, LEGANTO_SPRING, UNKNOWN, INN, HVL, LDH, MF, \
    NHH, NLA, NMBU, NTNU, VID, UiT, UiS, UiA, CONSPECTUS_USE_UNIT_NAME_NB, CONSPECTUS_USE_UNIT_NAME_EN
from lib.utility.constants import UNIT_CODE, CONSPECTUS_USE_UNIT_CODE, SUB_UNIT_CODE, SUB_SUB_UNIT_CODE, UNIT_NAME_NB, \
    UNIT_NAME_EN, SUB_UNIT_NAME_NB, SUB_UNIT_NAME_EN, SUB_SUB_UNIT_NAME_NB, SUB_SUB_UNIT_NAME_EN


def get_course_citation_column_names():
    """Get list of column names applicable for a citation insertion."""
    return 'conspectus_organisation, citation_id, status_value, status_desc, copyrights_status_value, ' \
           'copyrights_status_desc, secondary_type_value, secondary_type_desc, type_value, ' \
           'type_desc, metadata_title, metadata_author, metadata_publisher, ' \
           'metadata_publication_date, metadata_edition, metadata_isbn, metadata_issn, ' \
           'metadata_mms_id, metadata_additional_person_name, metadata_place_of_publication, ' \
           'metadata_call_number, metadata_note, metadata_journal_title, metadata_article_title, ' \
           'metadata_issue, metadata_editor, metadata_chapter, metadata_chapter_title, ' \
           'metadata_chapter_author, ' \
           'metadata_year, metadata_pages, metadata_source, metadata_series_title_number, ' \
           'metadata_pmid, metadata_doi, metadata_volume,  metadata_start_page, ' \
           'metadata_end_page,  metadata_start_page2, metadata_end_page2, ' \
           'metadata_author_initials, metadata_part, public_note, ' \
           'source1, source2, source3, source4, source5, source6, source7, source8, source9, ' \
           'source10, last_modified_date, section_info_id, section_info_name, ' \
           'section_info_description, section_info_visibility, section_info_tags_total_rec_cnt, ' \
           'section_info_section_tags_link, section_info_section_locked, ' \
           'file_link, fk_reading_list_id, fk_course_id, conspectus_use_unit_code, cristin_unit_code, cristin_sub_unit_code, ' \
           'cristin_sub_sub_unit_code, cristin_unit_name_nb, cristin_sub_unit_name_nb, ' \
           'cristin_sub_sub_unit_name_nb, cristin_unit_name_en, cristin_sub_unit_name_en, ' \
           'cristin_sub_sub_unit_name_en, processing_comment semester_code_1, semester_code_2, ' \
           'conspectus_semester_code_3, conspectus_term_code_1, term_name_1, conspectus_term_code_2, term_name_2, conspectus_term_code_3, ' \
           "term_name_3, year"


def get_course_column_names():
    """Get list of column names applicable for a course insertion."""
    return 'conspectus_organisation, course_id, conspectus_course_code, course_name, section, ' \
           'academic_department_code, academic_department_name, conspectus_term_code_1, term_name_1, ' \
           'conspectus_term_code_2, term_name_2, conspectus_term_code_3, term_name_3, ' \
           'start_date, end_date, weekly_hours, participants, year, created_date, ' \
           'last_modified_date, rolled_from, course_leganto_code, ' \
           'conspectus_use_unit_code, cristin_unit_code, cristin_sub_unit_code, cristin_sub_sub_unit_code, cristin_unit_name_nb, cristin_sub_unit_name_nb, ' \
           'cristin_sub_sub_unit_name_nb, cristin_unit_name_en, cristin_sub_unit_name_en, ' \
           'cristin_sub_sub_unit_name_en, processing_comment status'


def get_course_searchable_id_column_names():
    """Get list of column names applicable for a search id insertion."""
    return 'conspectus_organisation, fk_course_id, searchable_id'


def get_counters_column_names():
    """Get list of column names applicable for counters for an organisation ."""
    return 'conspectus_organisation, courses, reading_lists, citations, blocked_reading_list'


def get_course_reading_list_column_names():
    """Get list of column names applicable for a reading list insertion."""
    return 'conspectus_organisation, reading_list_id, conspectus_course_code, name, reading_list_order, description, ' \
           'last_modified_date, status_value, status_desc, visibility_value, visibility_desc, ' \
           'publishing_status_value, publishing_status_desc, conspectus_term_code_1, term_name_1, ' \
           'conspectus_term_code_2, term_name_2, conspectus_term_code_3, term_name_3, ' \
           'year, fk_course_id, blocked'


def get_course_value_placeholder():
    """Get list of placeholder for course."""
    return (
            '?, ' +  # organisation,
            '?, ' +  # id/course_id,
            '?, ' +  # name/course_code
            '?, ' +  # code/course_name
            '?, ' +  # section/section
            '?, ' +  # academic_department.value/academic_department_code
            '?, ' +  # academic_department.desc/academic_department_name
            '?, ' +  # term[0].value/conspectus_term_code_1
            '?, ' +  # term[0].desc/term_name_1
            '?, ' +  # term[1].value/conspectus_term_code_2
            '?, ' +  # term[1].desc/term_name_2
            '?, ' +  # term[2].value/conspectus_term_code_3
            '?, ' +  # term[3].desc/term_name_3
            '?, ' +  # start_date/start_date
            '?, ' +  # end_date/end_date
            '?, ' +  # weekly_hours/weekly_hours
            '?, ' +  # participants/participants
            '?, ' +  # year/year
            '?, ' +  # created_date/created_date
            '?, ' +  # last_modified_date/last_modified_date
            '?, ' +  # rolled_from/rolled_from
            '?, ' +  # course_leganto_code
            '?, ' +  # conspectus_use_unit_code
            '?, ' +  # unit_code
            '?, ' +  # sub_unit_code
            '?, ' +  # sub_sub_unit_code
            '?, ' +  # unit_name_nb
            '?, ' +  # sub_unit_name_nb
            '?, ' +  # sub_sub_unit_name_nb
            '?, ' +  # unit_name_en
            '?, ' +  # sub_unit_name_en
            '?, ' +  # sub_sub_unit_name_en
            '?, ' +  # processing_comment
            '?')  # status
    # //@formatter:off
    # //@formatter:on


def get_reading_list_value_placeholder():
    """Get list of placeholder for reading list."""
    # //@formatter:off
    return (
            '?, ' +  # organisation,
            '?, ' +  # id/reading_list_id,
            '?, ' +  # code/course_code
            '?, ' +  # name/course_name
            '?, ' +  # order/order
            '?, ' +  # description/description
            '?, ' +  # last_modified_date/last_modified_date
            '?, ' +  # status_value
            '?, ' +  # status_desc
            '?, ' +  # visibility_value
            '?, ' +  # visibility_desc
            '?, ' +  # publishing_status_value
            '?, ' +  # publishing_status_desc
            '?, ' +  # conspectus_term_code_1
            '?, ' +  # term_name_1
            '?, ' +  # conspectus_term_code_2
            '?, ' +  # term_name_2
            '?, ' +  # conspectus_term_code_3
            '?, ' +  # term_name_3
            '?, ' +  # year
            '?, ' +  # fk_course_id
            '?')  # blocked
    # //@formatter:on


def get_citation_value_placeholder():
    """Get list of placeholder for a citation."""
    # //@formatter:off
    return (
            '?, ' +  # organisation,
            '?, ' +  # id/citation_id,
            '?, ' +  # status.value/status_value
            '?, ' +  # status.desc/status_desc
            '?, ' +  # copyrights_status.value/copyrights_status_value
            '?, ' +  # copyrights_status.desc/copyrights_status_desc
            '?, ' +  # secondary_type.value/secondary_type_value
            '?, ' +  # secondary_type.desc/secondary_type_desc
            '?, ' +  # type.value/type_value
            '?, ' +  # type.desc/type_desc
            '?, ' +  # metadata.title/metadata_title
            '?, ' +  # metadata.author/metadata_author
            '?, ' +  # metadata.publisher/metadata_publisher
            '?, ' +  # metadata.publication_date/metadata_publication_date
            '?, ' +  # metadata.edition/metadata_edition
            '?, ' +  # metadata.isbn/metadata_isbn
            '?, ' +  # metadata.issn/metadata_issn
            '?, ' +  # metadata.mms_id/metadata_mms_id
            '?, ' +  # metadata.additional_person_name/metadata_additional_person_name
            '?, ' +  # metadata.place_of_publication/metadata_place_of_publication
            '?, ' +  # metadata.call_number/metadata_call_number
            '?, ' +  # metadata.note/metadata_note
            '?, ' +  # metadata.journal_title/metadata_journal_title
            '?, ' +  # metadata.article_title/metadata_article_title
            '?, ' +  # metadata.issue/metadata_issue
            '?, ' +  # metadata.editor/metadata_editor
            '?, ' +  # metadata.chapter/metadata_chapter
            '?, ' +  # metadata.chapter_title/metadata_chapter_title
            '?, ' +  # metadata.chapter_author/metadata_chapter_author
            '?, ' +  # metadata.year/metadata_year
            '?, ' +  # metadata.pages/metadata_pages
            '?, ' +  # metadata.source/metadata_source
            '?, ' +  # metadata.series_title_number/metadata_series_title_number
            '?, ' +  # metadata.pmid/metadata_pmid
            '?, ' +  # metadata.doi/metadata_doi
            '?, ' +  # metadata.volume/metadata_volume
            '?, ' +  # metadata.start_page/metadata_start_page
            '?, ' +  # metadata.end_page/metadata_end_page
            '?, ' +  # metadata.start_page2/metadata_start_page2
            '?, ' +  # metadata.end_page2/metadata_end_page2
            '?, ' +  # metadata.author_initials/metadata_author_initials
            '?, ' +  # metadata.part/metadata_part
            '?, ' +  # public_note/public_note
            '?, ' +  # source1/source1
            '?, ' +  # source2/source2
            '?, ' +  # source3/source3
            '?, ' +  # source4/source4
            '?, ' +  # source5/source5
            '?, ' +  # source6/source6
            '?, ' +  # source7/source7
            '?, ' +  # source8/source8
            '?, ' +  # source9/source9
            '?, ' +  # source10/source10
            '?, ' +  # last_modified_date/last_modified_date
            '?, ' +  # section_info.id/section_info_id
            '?, ' +  # section_info.name/section_info_name
            '?, ' +  # section_info.description/section_info_description
            '?, ' +  # section_info.visibility/section_info_visibility
            '?, ' +  # section_info.section_tags.total_record_count/section_info_tags_total_rec_cnt
            '?, ' +  # section_info.section_tags.link/section_info_section_tags_link
            '?, ' +  # section_info.section_locked/section_info_section_locked
            '?, ' +  # file_link/file_link
            '?, ' +  # fk_reading_list_id
            '?, ' +  # fk_course_id
            '?, ' +  # conspectus_use_unit_code
            '?, ' +  # unit_code
            '?, ' +  # sub_unit_code
            '?, ' +  # sub_sub_unit_code
            '?, ' +  # unit_name_nb
            '?, ' +  # sub_unit_name_nb
            '?, ' +  # sub_sub_unit_name_nb
            '?, ' +  # unit_name_en
            '?, ' +  # sub_unit_name_en
            '?, ' +  # sub_sub_unit_name_en
            '?, ' +  # processing_comment
            '?, ' +  # semester_code_1
            '?, ' +  # semester_code_2
            '?, ' +  # semester_code_3
            '?, ' +  # conspectus_term_code_1
            '?, ' +  # term_name_1
            '?, ' +  # conspectus_term_code_2
            '?, ' +  # term_name_2
            '?, ' +  # conspectus_term_code_3
            '?, ' +  # term_name_3
            '?')  # year
    # //@formatter:on


def get_course_counter_initial_values(organisation):
    """Get initial values for counters."""
    return organisation, '0'


def get_course_values(organisation, course):
    """Get list of values applicable for a course insertion."""
    if 'created_date' in course:
        course['created_date'] = date_remove_z(course['created_date'])
    if 'last_modified_date' in course:
        course['last_modified_date'] = date_remove_z(course['last_modified_date'])
    if START_DATE in course:
        course[START_DATE] = date_remove_z(course[START_DATE])
    if 'end_date' in course:
        course['end_date'] = date_remove_z(course['end_date'])

    conspectus_term_code_1 = ''
    term_name_1 = ''
    conspectus_term_code_2 = ''
    term_name_2 = ''
    conspectus_term_code_3 = ''
    term_name_3 = ''

    assign_course_term(course)
    course_name = get_string_value_dict(course, 'name')
    course_name = repr(course_name.replace('\xc2\x96', '-'))
    # Some courses start and end with a single quote (ntnu issue)
    course_name = course_name.lstrip("'")
    course_name = course_name.rstrip("'")

    return (organisation,
            get_string_value_dict(course, 'id'),
            get_string_value_dict(course, CONSPECTUS_COURSE_CODE),
            course_name,
            get_string_value_dict(course, 'section'),
            get_string_value_sub_dict(course, ACADEMIC_DEPARTMENT, VALUE),
            get_string_value_sub_dict(course, ACADEMIC_DEPARTMENT, 'desc'),
            conspectus_term_code_1,
            term_name_1,
            conspectus_term_code_2,
            term_name_2,
            conspectus_term_code_3,
            term_name_3,
            get_string_value_dict(course, START_DATE),
            get_string_value_dict(course, 'end_date'),
            get_numeric_value_dict(course, 'weekly_hours'),
            get_numeric_value_dict(course, 'participants'),
            get_string_value_dict(course, YEAR),
            get_string_value_dict(course, 'created_date'),
            get_string_value_dict(course, 'last_modified_date'),
            get_string_value_dict(course, 'rolled_from'),
            get_string_value_dict(course, COURSE_LEGANTO_CODE),
            get_string_value_dict(course, CONSPECTUS_USE_UNIT_CODE),
            get_string_value_dict(course, UNIT_CODE),
            get_string_value_dict(course, SUB_UNIT_CODE),
            get_string_value_dict(course, SUB_SUB_UNIT_CODE),
            get_string_value_dict(course, UNIT_NAME_NB),
            get_string_value_dict(course, SUB_UNIT_NAME_NB),
            get_string_value_dict(course, SUB_SUB_UNIT_NAME_NB),
            get_string_value_dict(course, UNIT_NAME_EN),
            get_string_value_dict(course, SUB_UNIT_NAME_EN),
            get_string_value_dict(course, SUB_SUB_UNIT_NAME_EN),
            get_string_value_dict(course, PROCESSING_COMMENT),
            get_string_value_dict(course, 'status'))


def get_course_searchable_id_values(organisation, course_id, searchable_id):
    """Get list of values applicable for a searchable id insertion."""
    return organisation, course_id, searchable_id


def get_course_term_values(organisation, course_id, term):
    """Get list of values applicable for a term insertion."""
    return (organisation, course_id, get_string_value_dict(term, VALUE),
            get_string_value_dict(term, 'desc'))


def get_reading_list_note_values(organisation, reading_list_id, note):
    """Get list of values applicable for a note / reading list insertion."""
    return (organisation, reading_list_id, get_string_value_dict(note, 'content'),
            get_string_value_dict(note, 'creation_date'),
            get_string_value_dict(note, 'type'), get_checksum_note(note))


def get_course_reading_list_values(organisation, course_id, reading_list):
    """Get list of values applicable for a reading list / course insertion."""
    return (organisation,
            get_string_value_dict(reading_list, 'id'),
            get_string_value_dict(reading_list, CONSPECTUS_COURSE_CODE),
            get_string_value_dict(reading_list, 'name'),
            get_string_value_dict(reading_list, 'order'),
            get_string_value_dict(reading_list, 'description'),
            get_string_value_dict(reading_list, 'last_modified_date'),
            get_string_value_sub_dict(reading_list, 'status', VALUE),
            get_string_value_sub_dict(reading_list, 'status', 'desc'),
            get_string_value_sub_dict(reading_list, 'visibility', VALUE),
            get_string_value_sub_dict(reading_list, 'visibility', 'desc'),
            get_string_value_sub_dict(reading_list, 'publishingStatus', VALUE),
            get_string_value_sub_dict(reading_list, 'publishingStatus', 'desc'),
            get_string_value_dict(reading_list, 'conspectus_term_code_1'),
            get_string_value_dict(reading_list, CONSPECTUS_TERM_NAME_1),
            get_string_value_dict(reading_list, CONSPECTUS_TERM_CODE_2),
            get_string_value_dict(reading_list, CONSPECTUS_TERM_NAME_2),
            get_string_value_dict(reading_list, CONSPECTUS_TERM_CODE_3),
            get_string_value_dict(reading_list, 'conspectus_term_name_4'),
            get_string_value_dict(reading_list, YEAR),
            course_id,
            get_boolean_value_dict(reading_list, 'blocked'))


def get_course_citation_values_as_tuple(organisation, course, reading_list_id, citation):
    """Get list of values applicable for a citation / reading list / course insertion."""

    citation['last_modified_date'] = date_remove_z(get_string_value_dict(citation,
                                                                         'last_modified_date'))
    return (organisation,
            get_string_value_dict(citation, 'id'),
            get_string_value_sub_dict(citation, 'status', VALUE),
            get_string_value_sub_dict(citation, 'status', 'desc'),
            get_string_value_sub_dict(citation, 'copyrights_status', VALUE),
            get_string_value_sub_dict(citation, 'copyrights_status', 'desc'),
            get_string_value_sub_dict(citation, 'secondary_type', VALUE),
            get_string_value_sub_dict(citation, 'secondary_type', 'desc'),
            get_string_value_sub_dict(citation, 'type', VALUE),
            get_string_value_sub_dict(citation, 'type', 'desc'),
            get_string_value_sub_dict(citation, 'metadata', 'title'),
            get_string_value_sub_dict(citation, 'metadata', 'author'),
            get_string_value_sub_dict(citation, 'metadata', 'publisher'),
            get_string_value_sub_dict(citation, 'metadata', 'publication_date'),
            get_string_value_sub_dict(citation, 'metadata', 'edition'),
            get_string_value_sub_dict(citation, 'metadata', 'isbn'),
            get_string_value_sub_dict(citation, 'metadata', 'issn'),
            get_string_value_sub_dict(citation, 'metadata', 'mms_id'),
            get_string_value_sub_dict(citation, 'metadata', 'additional_person_name'),
            get_string_value_sub_dict(citation, 'metadata', 'place_of_publication'),
            get_string_value_sub_dict(citation, 'metadata', 'call_number'),
            get_string_value_sub_dict(citation, 'metadata', 'note'),
            get_string_value_sub_dict(citation, 'metadata', 'journal_title'),
            get_string_value_sub_dict(citation, 'metadata', 'article_title'),
            get_string_value_sub_dict(citation, 'metadata', 'issue'),
            get_string_value_sub_dict(citation, 'metadata', 'editor'),
            get_string_value_sub_dict(citation, 'metadata', 'chapter'),
            get_string_value_sub_dict(citation, 'metadata', 'chapter_title'),
            get_string_value_sub_dict(citation, 'metadata', 'chapter_author'),
            get_string_value_sub_dict(citation, 'metadata', YEAR),
            get_string_value_sub_dict(citation, 'metadata', 'pages'),
            get_string_value_sub_dict(citation, 'metadata', 'source'),
            get_string_value_sub_dict(citation, 'metadata', 'series_title_number'),
            get_string_value_sub_dict(citation, 'metadata', 'pmid'),
            get_string_value_sub_dict(citation, 'metadata', 'doi'),
            get_string_value_sub_dict(citation, 'metadata', 'volume'),
            get_string_value_sub_dict(citation, 'metadata', 'start_page'),
            get_string_value_sub_dict(citation, 'metadata', 'end_page'),
            get_string_value_sub_dict(citation, 'metadata', 'start_page2'),
            get_string_value_sub_dict(citation, 'metadata', 'end_page2'),
            get_string_value_sub_dict(citation, 'metadata', 'author_initials'),
            get_string_value_sub_dict(citation, 'metadata', 'part'),
            get_string_value_dict(citation, 'public_note'),
            get_string_value_dict(citation, 'source1'),
            get_string_value_dict(citation, 'source2'),
            get_string_value_dict(citation, 'source3'),
            get_string_value_dict(citation, 'source4'),
            get_string_value_dict(citation, 'source5'),
            get_string_value_dict(citation, 'source6'),
            get_string_value_dict(citation, 'source7'),
            get_string_value_dict(citation, 'source8'),
            get_string_value_dict(citation, 'source9'),
            get_string_value_dict(citation, 'source10'),
            get_string_value_dict(citation, 'last_modified_date'),
            get_string_value_sub_dict(citation, 'section_info', 'id'),
            get_string_value_sub_dict(citation, 'section_info', 'name'),
            get_string_value_sub_dict(citation, 'section_info', 'description'),
            get_boolean_value_sub_dict(citation, 'section_info', 'visibility'),
            get_string_value_sub_sub_dict(
                citation, 'section_info', 'section_tags', 'total_record_count'),
            get_string_value_sub_sub_dict(citation, 'section_info', 'section_tags', 'link'),
            get_boolean_value_sub_dict(citation, 'section_info', 'section_locked'),
            get_string_value_dict(citation, 'file_link'),
            reading_list_id,
            course['id'],
            citation[CONSPECTUS_USE_UNIT_CODE],
            citation[UNIT_CODE],
            citation[SUB_UNIT_CODE],
            citation[SUB_SUB_UNIT_CODE],
            citation[UNIT_NAME_NB],
            citation[SUB_UNIT_NAME_NB],
            citation[SUB_SUB_UNIT_NAME_NB],
            citation[UNIT_NAME_EN],
            citation[SUB_UNIT_NAME_EN],
            citation[SUB_SUB_UNIT_NAME_EN],
            citation[PROCESSING_COMMENT],
            citation[CONSPECTUS_SEMESTER_CODE_1],
            citation[CONSPECTUS_SEMESTER_CODE_2],
            citation[CONSPECTUS_SEMESTER_CODE_3],
            citation[CONSPECTUS_TERM_CODE_1],
            citation[CONSPECTUS_TERM_NAME_1],
            citation[CONSPECTUS_TERM_CODE_2],
            citation[CONSPECTUS_TERM_NAME_2],
            citation[CONSPECTUS_TERM_CODE_3],
            citation[CONSPECTUS_TERM_NAME_3],
            course[YEAR])


def esc_escape(value):
    """Escape a given value. Will need more work as more examples found"""
    value = value.replace("[", "\\[")
    return value.replace("'", r"\'")


def date_remove_z(value):
    """Remove Z, 'Zulu' from date as mysql can't handle it"""
    if value is not None and value[-1] == "Z":
        return value[:-1]
    return value


def check_for_unknown_course_keys(course):
    course_keys = list(course)
    for key in course_keys:
        if key not in known_course_keys:
            print('Found unknown key while processing a course [' + key + ']')
    if ACADEMIC_DEPARTMENT in course:
        academic_department_keys = list(course[ACADEMIC_DEPARTMENT])
        for key in academic_department_keys:
            if key not in known_course_academic_department_keys:
                print('Found unknown key while processing a course:academic_department[' +
                      key + ']')


def check_for_unknown_reading_list_keys(reading_list):
    reading_list_keys = list(reading_list)
    for key in reading_list_keys:
        if key not in known_reading_list_keys:
            print('Found unknown key while processing a reading_list [' + key + ']')
    if 'status' in reading_list:
        status_keys = list(reading_list['status'])
        for key in status_keys:
            if key not in known_reading_list_status_keys:
                print('Found unknown key while processing a reading_list:status[' + key + ']')
    if 'syllabus' in reading_list:
        syllabus_keys = list(reading_list['syllabus'])
        for key in syllabus_keys:
            if key not in known_reading_list_syllabus_keys:
                print('Found unknown key while processing a reading_list:syllabus[' + key + ']')
    if 'visibility' in reading_list:
        visibility_keys = list(reading_list['visibility'])
        for key in visibility_keys:
            if key not in known_reading_list_visibility_keys:
                print('Found unknown key while processing a reading_list:visibility[' + key + ']')
    if 'publishingStatus' in reading_list:
        publishing_status_keys = reading_list['publishingStatus']
        for key in publishing_status_keys:
            if key not in known_reading_list_publishing_status_keys:
                print('Found unknown key while processing a reading_list:publishing_status[' +
                      key + ']')


def check_for_unknown_citation_keys(citation):
    citation_keys = list(known_citation_keys)
    for key in citation_keys:
        if key not in known_citation_keys:
            print('Found unknown key while processing a citation [' + key + ']')
    if 'status' in citation:
        status_keys = list(citation['status'])
        for key in status_keys:
            if key not in known_citation_status_keys:
                print('Found unknown key while processing a citation:status[' + key + ']')
    if 'copyrights_status' in citation:
        copyrights_status_keys = list(citation['copyrights_status'])
        for key in copyrights_status_keys:
            if key not in known_citation_copyrights_status_keys:
                print('Found unknown key while processing a citation:copyrights_status[' +
                      key + ']')
    if 'type' in citation:
        type_keys = list(citation['type'])
        for key in type_keys:
            if key not in known_citation_type_keys:
                print('Found unknown key while processing a citation:type[' + key + ']')
    if 'metadata' in citation:
        metadata_keys = citation['metadata']
        for key in metadata_keys:
            if key not in known_citation_metadata_keys:
                print('Found unknown key while processing a citation:metadata[' +
                      key + ']')
    if 'section_info' in citation:
        section_info_keys = citation['section_info']
        for key in section_info_keys:
            if key not in known_citation_section_info_keys:
                print('Found unknown key while processing a citation:section_info[' +
                      key + ']')


def get_csv_course_column_names():
    """Get list of column names applicable for a csv description of course headers ."""
    return [ORGANISATION, "course_id", CONSPECTUS_COURSE_CODE, CONSPECTUS_COURSE_NAME, "section",
            "academic_department_code", "academic_department_name", "start_date", "end_date",
            "weekly_hours", "participants", "year", "created_date", "last_modified_date",
            "rolled_from", "course_leganto_code",
            CONSPECTUS_TERM_CODE_1, CONSPECTUS_TERM_NAME_1, CONSPECTUS_SEMESTER_CODE_1, CONSPECTUS_TERM_CODE_2,
            CONSPECTUS_TERM_NAME_2, CONSPECTUS_SEMESTER_CODE_2, CONSPECTUS_TERM_CODE_3, CONSPECTUS_TERM_NAME_3,
            CONSPECTUS_SEMESTER_CODE_3, CONSPECTUS_COURSE_YEAR, CONSPECTUS_COURSE_SEMESTER, CONSPECTUS_USE_UNIT_CODE,
            CONSPECTUS_USE_UNIT_NAME_NB, CONSPECTUS_USE_UNIT_NAME_EN, UNIT_CODE, SUB_UNIT_CODE, SUB_SUB_UNIT_CODE,
            UNIT_NAME_NB, SUB_UNIT_NAME_NB, SUB_SUB_UNIT_NAME_NB,
            UNIT_NAME_EN, SUB_UNIT_NAME_EN, SUB_SUB_UNIT_NAME_EN,
            PROCESSING_COMMENT, "status", DBH_INSTITUTION_CODE_EN,
            DBH_INSTITUTION_NAME_EN, DBH_DEPT_CODE_EN, DBH_DEPT_NAME_EN, DBH_DEPT_CODE_SSB_EN, DBH_YEAR_EN,
            DBH_SEMESTER_EN, DBH_SEMESTER_NAME_EN, DBH_COURSE_STUDY_PROGRAM_CODE_EN, DBH_COURSE_STUDY_PROGRAM_NAME_EN,
            DBH_COURSE_CODE_EN, DBH_COURSE_NAME_EN, DBH_LEVEL_CODE_EN, DBH_LEVEL_NAME_EN, DBH_COURSE_CREDIT_EN,
            DBH_NUS_CODE_EN, DBH_STATUS_EN, DBH_STATUS_NAME_EN, DBH_LANGUAGE_EN, DBH_NAME_EN, DBH_AREA_CODE_EN,
            DBH_AREA_NAME_EN, DBH_TASK_EN
            ]


def get_csv_reading_list_column_names():
    """Get list of column names applicable for a csv description of reading_list headers ."""
    return [ORGANISATION, "reading_list_id", CONSPECTUS_COURSE_CODE, "name", "reading_list_order",
            "description", "last_modified_date", "status_value", "status_desc",
            "visibility_value", "visibility_desc", "publishing_status_value",
            "publishing_status_desc", "blocked", "fk_course_id"]


def get_csv_citation_column_names():
    """Get list of column names applicable for a csv description of citation headers ."""
    return [ORGANISATION, CONSPECTUS_COURSE_CODE, CONSPECTUS_COURSE_NAME, CONSPECTUS_USE_UNIT_CODE,
            CONSPECTUS_USE_UNIT_NAME_NB, CONSPECTUS_USE_UNIT_NAME_EN, UNIT_CODE, SUB_UNIT_CODE,
            SUB_SUB_UNIT_CODE,
            UNIT_NAME_NB, SUB_UNIT_NAME_NB, SUB_SUB_UNIT_NAME_NB,
            UNIT_NAME_EN, SUB_UNIT_NAME_EN, SUB_SUB_UNIT_NAME_EN,
            PROCESSING_COMMENT, 'semester_year', 'semester_1', 'semester_2', 'semester_3',
            CONSPECTUS_SEMESTER_CODE_1, CONSPECTUS_SEMESTER_CODE_2, CONSPECTUS_SEMESTER_CODE_3,
            CONSPECTUS_COURSE_YEAR, CONSPECTUS_COURSE_SEMESTER,
            ACADEMIC_DEPARTMENT_NAME, ACADEMIC_DEPARTMENT_CODE, 'citation_id', 'status_value', 'status_desc',
            'copyrights_status_value', 'copyrights_status_desc', 'secondary_type_value',
            'secondary_type_desc', 'type_value', 'type_desc', 'metadata_title',
            'metadata_author', 'metadata_publisher', 'metadata_publication_date',
            'metadata_edition', 'metadata_isbn', 'metadata_issn', 'metadata_mms_id',
            'metadata_additional_person_name', 'metadata_place_of_publication',
            'metadata_call_number', 'metadata_note', 'metadata_journal_title',
            'metadata_article_title', 'metadata_issue', 'metadata_editor', 'metadata_chapter',
            'metadata_chapter_title', 'metadata_chapter_author', 'metadata_year',
            'metadata_pages', 'metadata_source', 'metadata_series_title_number', 'metadata_pmid',
            'metadata_doi', 'metadata_volume', 'metadata_start_page', 'metadata_end_page',
            'metadata_start_page2', 'metadata_end_page2', 'metadata_author_initials',
            'metadata_part', 'public_note', 'source1', 'source2',
            'source3', 'source4', 'source5', 'source6', 'source7', 'source8', 'source9',
            'source10', 'last_modified_date', 'section_info_id', 'section_info_name',
            'section_info_description', 'section_info_visibility',
            'section_info_tags_total_rec_cnt', 'section_info_section_tags_link',
            'section_info_section_locked', 'file_link', 'fk_reading_list_id',
            'fk_course_id', COURSE_LEGANTO_CODE, DBH_INSTITUTION_CODE_EN,
            DBH_INSTITUTION_NAME_EN, DBH_DEPT_CODE_EN, DBH_DEPT_NAME_EN, DBH_DEPT_CODE_SSB_EN, DBH_YEAR_EN,
            DBH_SEMESTER_EN, DBH_SEMESTER_NAME_EN, DBH_COURSE_STUDY_PROGRAM_CODE_EN, DBH_COURSE_STUDY_PROGRAM_NAME_EN,
            DBH_COURSE_CODE_EN, DBH_COURSE_NAME_EN, DBH_LEVEL_CODE_EN, DBH_LEVEL_NAME_EN, DBH_COURSE_CREDIT_EN,
            DBH_NUS_CODE_EN, DBH_STATUS_EN, DBH_STATUS_NAME_EN, DBH_LANGUAGE_EN, DBH_NAME_EN, DBH_AREA_CODE_EN,
            DBH_AREA_NAME_EN, DBH_TASK_EN
            ]


def get_csv_values_course(course):
    return {ORGANISATION: course[ORGANISATION],
            'course_id': get_string_value_dict(course, 'id'),
            CONSPECTUS_COURSE_CODE: get_string_value_dict(course, CONSPECTUS_COURSE_CODE),
            CONSPECTUS_COURSE_NAME: get_string_value_dict(course, CONSPECTUS_COURSE_NAME),
            'section': get_string_value_dict(course, 'section'),
            ACADEMIC_DEPARTMENT_CODE: get_string_value_sub_dict(course, ACADEMIC_DEPARTMENT,
                                                                  VALUE),
            ACADEMIC_DEPARTMENT_NAME: get_string_value_sub_dict(course, ACADEMIC_DEPARTMENT,
                                                                  'desc'),
            START_DATE: date_remove_z(get_string_value_dict(course, START_DATE)),
            'end_date': date_remove_z(get_string_value_dict(course, 'end_date')),
            'weekly_hours': get_numeric_value_dict(course, 'weekly_hours'),
            'participants': get_numeric_value_dict(course, 'participants'),
            YEAR: get_string_value_dict(course, YEAR),
            'created_date': date_remove_z(get_string_value_dict(course, 'created_date')),
            'last_modified_date': date_remove_z(get_string_value_dict(course,
                                                                      'last_modified_date')),
            'rolled_from': get_string_value_dict(course, 'rolled_from'),
            COURSE_LEGANTO_CODE: get_string_value_dict(course, COURSE_LEGANTO_CODE),
            CONSPECTUS_TERM_CODE_1: get_string_value_dict(course, CONSPECTUS_TERM_CODE_1),
            CONSPECTUS_TERM_NAME_1: get_string_value_dict(course, CONSPECTUS_TERM_NAME_1),
            CONSPECTUS_SEMESTER_CODE_1: get_string_value_dict(course, CONSPECTUS_SEMESTER_CODE_1),
            CONSPECTUS_TERM_CODE_2: get_string_value_dict(course, CONSPECTUS_TERM_CODE_2),
            CONSPECTUS_TERM_NAME_2: get_string_value_dict(course, CONSPECTUS_TERM_NAME_2),
            CONSPECTUS_SEMESTER_CODE_2: get_string_value_dict(course, CONSPECTUS_SEMESTER_CODE_2),
            CONSPECTUS_TERM_CODE_3: get_string_value_dict(course, CONSPECTUS_TERM_CODE_3),
            CONSPECTUS_TERM_NAME_3: get_string_value_dict(course, CONSPECTUS_TERM_NAME_3),
            CONSPECTUS_SEMESTER_CODE_3: get_string_value_dict(course, CONSPECTUS_SEMESTER_CODE_3),
            CONSPECTUS_COURSE_YEAR: get_string_value_dict(course, CONSPECTUS_COURSE_YEAR),
            CONSPECTUS_COURSE_SEMESTER: get_string_value_dict(course, CONSPECTUS_COURSE_SEMESTER),
            CONSPECTUS_USE_UNIT_CODE: get_string_value_dict(course, CONSPECTUS_USE_UNIT_CODE),
            CONSPECTUS_USE_UNIT_NAME_NB: get_string_value_dict(course, CONSPECTUS_USE_UNIT_NAME_NB),
            CONSPECTUS_USE_UNIT_NAME_EN: get_string_value_dict(course, CONSPECTUS_USE_UNIT_NAME_EN),
            UNIT_CODE: get_string_value_dict(course, UNIT_CODE),
            SUB_UNIT_CODE: get_string_value_dict(course, SUB_UNIT_CODE),
            SUB_SUB_UNIT_CODE: get_string_value_dict(course, SUB_SUB_UNIT_CODE),
            UNIT_NAME_NB: get_string_value_dict(course, UNIT_NAME_NB),
            SUB_UNIT_NAME_NB: get_string_value_dict(course, SUB_UNIT_NAME_NB),
            SUB_SUB_UNIT_NAME_NB: get_string_value_dict(course, SUB_SUB_UNIT_NAME_NB),
            UNIT_NAME_EN: get_string_value_dict(course, UNIT_NAME_EN),
            SUB_UNIT_NAME_EN: get_string_value_dict(course, SUB_UNIT_NAME_EN),
            SUB_SUB_UNIT_NAME_EN: get_string_value_dict(course, SUB_SUB_UNIT_NAME_EN),
            PROCESSING_COMMENT: get_string_value_dict(course, PROCESSING_COMMENT),
            'status': get_string_value_dict(course, 'status'),
            DBH_INSTITUTION_CODE_EN: get_string_value_dict(course, DBH_INSTITUTION_CODE_EN),
            DBH_INSTITUTION_NAME_EN: get_string_value_dict(course, DBH_INSTITUTION_NAME_EN),
            DBH_DEPT_CODE_EN: get_string_value_dict(course, DBH_DEPT_CODE_EN),
            DBH_DEPT_NAME_EN: get_string_value_dict(course, DBH_DEPT_NAME_EN),
            DBH_DEPT_CODE_SSB_EN: get_string_value_dict(course, DBH_DEPT_CODE_SSB_EN),
            DBH_YEAR_EN: get_string_value_dict(course, DBH_YEAR_EN),
            DBH_SEMESTER_EN: get_string_value_dict(course, DBH_SEMESTER_EN),
            DBH_SEMESTER_NAME_EN: get_string_value_dict(course, DBH_SEMESTER_NAME_EN),
            DBH_COURSE_STUDY_PROGRAM_CODE_EN: get_string_value_dict(course, DBH_COURSE_STUDY_PROGRAM_CODE_EN),
            DBH_COURSE_STUDY_PROGRAM_NAME_EN: get_string_value_dict(course, DBH_COURSE_STUDY_PROGRAM_NAME_EN),
            DBH_COURSE_CODE_EN: get_string_value_dict(course, DBH_COURSE_CODE_EN),
            DBH_COURSE_NAME_EN: get_string_value_dict(course, DBH_COURSE_NAME_EN),
            DBH_LEVEL_CODE_EN: get_string_value_dict(course, DBH_LEVEL_CODE_EN),
            DBH_LEVEL_NAME_EN: get_string_value_dict(course, DBH_LEVEL_NAME_EN),
            DBH_COURSE_CREDIT_EN: get_string_value_dict(course, DBH_COURSE_CREDIT_EN),
            DBH_NUS_CODE_EN: get_string_value_dict(course, DBH_NUS_CODE_EN),
            DBH_STATUS_EN: get_string_value_dict(course, DBH_STATUS_EN),
            DBH_STATUS_NAME_EN: get_string_value_dict(course, DBH_STATUS_NAME_EN),
            DBH_LANGUAGE_EN: get_string_value_dict(course, DBH_LANGUAGE_EN),
            DBH_NAME_EN: get_string_value_dict(course, DBH_NAME_EN),
            DBH_AREA_CODE_EN: get_string_value_dict(course, DBH_AREA_CODE_EN),
            DBH_AREA_NAME_EN: get_string_value_dict(course, DBH_AREA_NAME_EN),
            DBH_TASK_EN: get_string_value_dict(course, DBH_TASK_EN)
            }


def get_csv_values_reading_list(reading_list):
    return {ORGANISATION: reading_list[ORGANISATION],
            'reading_list_id': get_string_value_dict(reading_list, 'id'),
            CONSPECTUS_COURSE_CODE: get_string_value_dict(reading_list, CONSPECTUS_COURSE_CODE),
            'name': get_string_value_dict(reading_list, 'name'),
            'reading_list_order': get_string_value_dict(reading_list, 'order'),
            'description': get_string_value_dict(reading_list, 'description'),
            'last_modified_date': date_remove_z(get_numeric_value_dict(reading_list,
                                                                       'last_modified_date')),
            'status_value': get_string_value_sub_dict(reading_list, 'status', VALUE),
            'status_desc': get_string_value_sub_dict(reading_list, 'status', 'desc'),
            'visibility_value': get_string_value_sub_dict(reading_list, 'visibility', VALUE),
            'visibility_desc': get_string_value_sub_dict(reading_list, 'visibility', 'desc'),
            'publishing_status_value': get_string_value_sub_dict(reading_list, 'publishingStatus',
                                                                 VALUE),
            'publishing_status_desc': get_string_value_sub_dict(reading_list, 'publishingStatus',
                                                                'desc'),
            'blocked': get_boolean_value_dict(reading_list, 'blocked'),
            'fk_course_id': reading_list[COURSE_ID]}


def merge_citation_object_with_course(course, reading_list_id, citation):
    citation[COURSE_ID] = course[ID]
    citation[ORGANISATION] = course[ORGANISATION]
    citation[CONSPECTUS_TERM_CODE_1] = course[CONSPECTUS_TERM_CODE_1]
    citation[CONSPECTUS_TERM_NAME_1] = course[CONSPECTUS_TERM_NAME_1]
    citation[CONSPECTUS_TERM_CODE_2] = course[CONSPECTUS_TERM_CODE_2]
    citation[CONSPECTUS_TERM_NAME_2] = course[CONSPECTUS_TERM_NAME_2]
    citation[CONSPECTUS_TERM_CODE_3] = course[CONSPECTUS_TERM_CODE_3]
    citation[CONSPECTUS_TERM_NAME_3] = course[CONSPECTUS_TERM_NAME_3]
    citation[ACADEMIC_DEPARTMENT_CODE] = course[ACADEMIC_DEPARTMENT_CODE]
    citation[ACADEMIC_DEPARTMENT_NAME] = course[ACADEMIC_DEPARTMENT_NAME]
    citation[CONSPECTUS_SEMESTER_CODE_1] = course[CONSPECTUS_SEMESTER_CODE_1]
    citation[CONSPECTUS_SEMESTER_CODE_2] = course[CONSPECTUS_SEMESTER_CODE_2]
    citation[CONSPECTUS_SEMESTER_CODE_3] = course[CONSPECTUS_SEMESTER_CODE_3]
    citation[CONSPECTUS_COURSE_YEAR] = course[CONSPECTUS_COURSE_YEAR]
    citation[CONSPECTUS_COURSE_SEMESTER] = course[CONSPECTUS_COURSE_SEMESTER]
    citation[CONSPECTUS_USE_UNIT_CODE] = course[CONSPECTUS_USE_UNIT_CODE]
    citation[CONSPECTUS_USE_UNIT_NAME_NB] = course[CONSPECTUS_USE_UNIT_NAME_NB]
    citation[CONSPECTUS_USE_UNIT_NAME_EN] = course[CONSPECTUS_USE_UNIT_NAME_EN]
    citation[UNIT_CODE] = course[UNIT_CODE]
    citation[SUB_UNIT_CODE] = course[SUB_UNIT_CODE]
    citation[SUB_SUB_UNIT_CODE] = course[SUB_SUB_UNIT_CODE]
    citation[UNIT_NAME_NB] = course[UNIT_NAME_NB]
    citation[SUB_UNIT_NAME_NB] = course[SUB_UNIT_NAME_NB]
    citation[SUB_SUB_UNIT_NAME_NB] = course[SUB_SUB_UNIT_NAME_NB]
    citation[UNIT_NAME_EN] = course[UNIT_NAME_EN]
    citation[SUB_UNIT_NAME_EN] = course[SUB_UNIT_NAME_EN]
    citation[SUB_SUB_UNIT_NAME_EN] = course[SUB_SUB_UNIT_NAME_EN]
    if PROCESSING_COMMENT in course.keys():
        citation[PROCESSING_COMMENT] = course[PROCESSING_COMMENT]
    else:
        citation[PROCESSING_COMMENT] = ''

    citation[COURSE_CODE_NAME] = course[CODE] + ' ' + course[NAME]
    citation[COURSE_YEAR] = course[YEAR]
    citation[CONSPECTUS_COURSE_CODE] = course[CONSPECTUS_COURSE_CODE]
    citation[CONSPECTUS_COURSE_NAME] = course[CONSPECTUS_COURSE_NAME]
    citation[COURSE_LEGANTO_CODE] = course[COURSE_LEGANTO_CODE]
    citation[LEGANTO_DEPT_CODE] = course[LEGANTO_DEPT_CODE]
    citation[LEGANTO_DEPT_NAME] = course[LEGANTO_DEPT_NAME]
    citation[READING_LIST_ID] = reading_list_id
    citation[DBH_INSTITUTION_CODE_EN] = course[DBH_INSTITUTION_CODE_EN]
    citation[DBH_INSTITUTION_NAME_EN] = course[DBH_INSTITUTION_NAME_EN]
    citation[DBH_DEPT_CODE_EN] = course[DBH_DEPT_CODE_EN]
    citation[DBH_DEPT_NAME_EN] = course[DBH_DEPT_NAME_EN]
    citation[DBH_DEPT_CODE_SSB_EN] = course[DBH_DEPT_CODE_SSB_EN]
    citation[DBH_YEAR_EN] = course[DBH_YEAR_EN]
    citation[DBH_SEMESTER_EN] = course[DBH_SEMESTER_EN]
    citation[DBH_SEMESTER_NAME_EN] = course[DBH_SEMESTER_NAME_EN]
    citation[DBH_COURSE_STUDY_PROGRAM_CODE_EN] = course[DBH_COURSE_STUDY_PROGRAM_CODE_EN]
    citation[DBH_COURSE_STUDY_PROGRAM_NAME_EN] = course[DBH_COURSE_STUDY_PROGRAM_NAME_EN]
    citation[DBH_COURSE_CODE_EN] = course[DBH_COURSE_CODE_EN]
    citation[DBH_COURSE_NAME_EN] = course[DBH_COURSE_NAME_EN]
    citation[DBH_LEVEL_CODE_EN] = course[DBH_LEVEL_CODE_EN]
    citation[DBH_LEVEL_NAME_EN] = course[DBH_LEVEL_NAME_EN]
    citation[DBH_COURSE_CREDIT_EN] = course[DBH_COURSE_CREDIT_EN]
    citation[DBH_NUS_CODE_EN] = course[DBH_NUS_CODE_EN]
    citation[DBH_STATUS_EN] = course[DBH_STATUS_EN]
    citation[DBH_STATUS_NAME_EN] = course[DBH_STATUS_NAME_EN]
    citation[DBH_LANGUAGE_EN] = course[DBH_LANGUAGE_EN]
    citation[DBH_NAME_EN] = course[DBH_NAME_EN]
    citation[DBH_AREA_CODE_EN] = course[DBH_AREA_CODE_EN]
    citation[DBH_AREA_NAME_EN] = course[DBH_AREA_NAME_EN]
    citation[DBH_TASK_EN] = course[DBH_TASK_EN]


def assign_term_to_course(course, conspectus_term_code, term_name, semester_code, index):
    course[conspectus_term_code] = course[TERM][index][VALUE]
    course[term_name] = course[TERM][index][DESC]
    course[semester_code] = course[YEAR] + "-" + course[conspectus_term_code]


def assign_blank_terms(course):
    course[CONSPECTUS_TERM_CODE_1] = ''
    course[CONSPECTUS_TERM_NAME_1] = ''
    course[CONSPECTUS_SEMESTER_CODE_1] = ''
    course[CONSPECTUS_TERM_CODE_2] = ''
    course[CONSPECTUS_TERM_NAME_2] = ''
    course[CONSPECTUS_SEMESTER_CODE_2] = ''
    course[CONSPECTUS_TERM_CODE_3] = ''
    course[CONSPECTUS_TERM_NAME_3] = ''
    course[CONSPECTUS_SEMESTER_CODE_3] = ''


def assign_course_dept(course):
    course[ACADEMIC_DEPARTMENT_CODE] = ''
    course[ACADEMIC_DEPARTMENT_NAME] = ''
    if ACADEMIC_DEPARTMENT in course.keys():
        if VALUE in course[ACADEMIC_DEPARTMENT].keys():
            course[ACADEMIC_DEPARTMENT_CODE] = course[ACADEMIC_DEPARTMENT][VALUE]
        if DESC in course[ACADEMIC_DEPARTMENT].keys():
            course[ACADEMIC_DEPARTMENT_NAME] = course[ACADEMIC_DEPARTMENT][DESC]


def assign_course_term(course):
    assign_blank_terms(course)
    if TERM in course.keys():
        if len(course[TERM]) == 0:
            logging.error(MISSING_TERM.format(course[CODE]))
            raise ValueError(MISSING_TERM.format(course[CODE]))
        elif len(course[TERM]) == 1:
            if VALUE in course[TERM][0]:
                assign_term_to_course(course, CONSPECTUS_TERM_CODE_1, CONSPECTUS_TERM_NAME_1,
                                      CONSPECTUS_SEMESTER_CODE_1, 0)
        elif len(course[TERM]) == 2:
            if VALUE in course[TERM][1]:
                assign_term_to_course(course, CONSPECTUS_TERM_CODE_1, CONSPECTUS_TERM_NAME_1,
                                      CONSPECTUS_SEMESTER_CODE_1, 0)
                assign_term_to_course(course, CONSPECTUS_TERM_CODE_2, CONSPECTUS_TERM_NAME_2,
                                      CONSPECTUS_SEMESTER_CODE_2, 1)
        elif len(course[TERM]) == 3:
            if VALUE in course[TERM][2]:
                assign_term_to_course(course, CONSPECTUS_TERM_CODE_1, CONSPECTUS_TERM_NAME_1,
                                      CONSPECTUS_SEMESTER_CODE_1, 0)
                assign_term_to_course(course, CONSPECTUS_TERM_CODE_2, CONSPECTUS_TERM_NAME_2,
                                      CONSPECTUS_SEMESTER_CODE_2, 1)
                assign_term_to_course(course, CONSPECTUS_TERM_CODE_3, CONSPECTUS_TERM_NAME_3,
                                      CONSPECTUS_SEMESTER_CODE_3, 2)
        else:
            logging.error(MULTIPLE_SEMESTER.format(course[CODE]))
            raise ValueError(MULTIPLE_SEMESTER.format(course[CODE]))

def get_noark_bsm_values_course(citations):
    bsm_values = '\t\t\t\t\t<leganto-v1:course_code>' + escape(citations[CONSPECTUS_COURSE_CODE])+ '</leganto-v1:course_code>\n'
    bsm_values = bsm_values + '\t\t\t\t\t<leganto-v1:course_name>' + escape( citations[CONSPECTUS_COURSE_NAME])+ '</leganto-v1:course_name>\n'
    bsm_values = bsm_values + '\t\t\t\t\t<leganto-v1:cristin_id>' + escape(citations[CONSPECTUS_USE_UNIT_CODE])+ '</leganto-v1:cristin_id>\n'
    if citations['course_year'] is not None:
         bsm_values = bsm_values + '\t\t\t\t\t<leganto-v1:course_year>' + escape(citations['course_year'])+ '</leganto-v1:course_year>\n'
    return bsm_values

def get_noark_bsm_values_citation(citation, citations):
    bsm_values = ''
    if get_string_value_dict(citations, CONSPECTUS_TERM_CODE_1) is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:semester_1>' + escape(get_string_value_dict(citations, CONSPECTUS_TERM_CODE_1))+ '</leganto-v1:semester_1>\n'
    if get_string_value_dict(citations, CONSPECTUS_TERM_CODE_2) is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:semester_2>' + escape(get_string_value_dict(citations, CONSPECTUS_TERM_CODE_2))+ '</leganto-v1:semester_2>\n'
    if get_string_value_dict(citations, CONSPECTUS_TERM_CODE_3) is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:semester_3>' + escape(get_string_value_dict(citations, CONSPECTUS_TERM_CODE_3))+ '</leganto-v1:semester_3>\n'

    if get_string_value_sub_dict(citation, 'metadata', 'title') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_title>' + escape(get_string_value_sub_dict(citation, 'metadata', 'title'))+ '</leganto-v1:metadata_title>\n'
    if get_string_value_sub_dict(citation, 'metadata', 'author') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_author>' + escape(get_string_value_sub_dict(citation, 'metadata', 'author'))+ '</leganto-v1:metadata_author>\n'
    if get_string_value_sub_dict(citation, 'metadata', 'publisher') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_publisher>' + escape(get_string_value_sub_dict(citation, 'metadata', 'publisher') )+ '</leganto-v1:metadata_publisher>\n'
    if get_string_value_sub_dict(citation, 'metadata', 'publication_date') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_publication_date>' + escape(get_string_value_sub_dict(citation, 'metadata', 'publication_date'))+ '</leganto-v1:metadata_publication_date>\n'
    if get_string_value_sub_dict(citation, 'metadata', 'isbn') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_isbn>' + escape(get_string_value_sub_dict(citation, 'metadata', 'isbn'))+ '</leganto-v1:metadata_isbn>\n'
    if get_string_value_sub_dict(citation, 'metadata', 'issn') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_issn>' + escape(get_string_value_sub_dict(citation, 'metadata', 'issn'))+ '</leganto-v1:metadata_issn>\n'
    if get_string_value_sub_dict(citation, 'metadata', 'mms_id') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_mms_id>' + escape(get_string_value_sub_dict(citation, 'metadata', 'mms_id'))+ '</leganto-v1:metadata_mms_id>\n'
    if get_string_value_sub_dict(citation, 'metadata','additional_person_name') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_additional_person_name>' + escape(get_string_value_sub_dict(citation, 'metadata','additional_person_name'))+ '</leganto-v1:metadata_additional_person_name>\n'
    if get_string_value_sub_dict(citation, 'metadata','place_of_publication') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_place_of_publication>' + escape(get_string_value_sub_dict(citation, 'metadata','place_of_publication'))+ '</leganto-v1:metadata_place_of_publication>\n'
    if get_string_value_sub_dict(citation, 'metadata', 'call_number') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_call_number>' + escape(get_string_value_sub_dict(citation, 'metadata', 'call_number'))+ '</leganto-v1:metadata_call_number>\n'
    if get_string_value_sub_dict(citation, 'metadata', 'note') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_note>' + escape(get_string_value_sub_dict(citation, 'metadata', 'note'))+ '</leganto-v1:metadata_note\n'
    if get_string_value_sub_dict(citation, 'metadata','journal_title') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_journal_title>' + escape(get_string_value_sub_dict(citation, 'metadata','journal_title'))+ '</leganto-v1:metadata_journal_title>\n'
    if get_string_value_sub_dict(citation, 'metadata', 'article_title') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_article_title>' + escape(get_string_value_sub_dict(citation, 'metadata', 'article_title'))+ '</leganto-v1:metadata_article_title>\n'
    if get_string_value_sub_dict(citation, 'metadata', 'issue') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_issue>' + escape(get_string_value_sub_dict(citation, 'metadata', 'issue'))+ '</leganto-v1:metadata_issue>\n'
    if get_string_value_sub_dict(citation, 'metadata', 'editor') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_editor>' + escape(get_string_value_sub_dict(citation, 'metadata', 'editor'))+ '</leganto-v1:metadata_editor>\n'
    if get_string_value_sub_dict(citation, 'metadata', 'chapter') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_chapter>' + escape(get_string_value_sub_dict(citation, 'metadata', 'chapter'))+ '</leganto-v1:metadata_chapter>\n'
    if get_string_value_sub_dict(citation, 'metadata','chapter_title') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_chapter_title>' + escape(get_string_value_sub_dict(citation, 'metadata','chapter_title'))+ '</leganto-v1:metadata_chapter_title>\n'
    if get_string_value_sub_dict(citation, 'metadata','chapter_author') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_chapter_author>' + escape(get_string_value_sub_dict(citation, 'metadata','chapter_author'))+ '</leganto-v1:metadata_chapter_author>\n'
    if get_string_value_sub_dict(citation, 'metadata', YEAR) is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_year>' + escape(get_string_value_sub_dict(citation, 'metadata', YEAR))+ '</leganto-v1:metadata_year>\n'
    if get_string_value_sub_dict(citation, 'metadata', 'pages') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_pages>' + escape(get_string_value_sub_dict(citation, 'metadata', 'pages'))+ '</leganto-v1:metadata_pages>\n'
    if get_string_value_sub_dict(citation, 'metadata', 'source') is not None:
        val2 = get_string_value_sub_dict(citation, 'metadata', 'source')
        val3 = escape(get_string_value_sub_dict(citation, 'metadata', 'source'))
        val1 = '\t\t\t\t\t\t<leganto-v1:metadata_source>' + escape(get_string_value_sub_dict(citation, 'metadata', 'source'))+ '</leganto-v1:metadata_source>\n'
        #bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_source>' + escape(get_string_value_sub_dict(citation, 'metadata', 'source'))+ '</leganto-v1:metadata_source>\n'
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_source> REDACTED TEMPORARILY. It causes too much trouble escaping</leganto-v1:metadata_source>\n'
    if get_string_value_sub_dict(citation, 'metadata','series_title_number') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_series_title_number>' + escape(get_string_value_sub_dict(citation, 'metadata','series_title_number'))+ '</leganto-v1:metadata_series_title_number>\n'
    if get_string_value_sub_dict(citation, 'metadata', 'pmid') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_pmid>' + escape(get_string_value_sub_dict(citation, 'metadata', 'pmid'))+ '</leganto-v1:metadata_pmid>\n'
    if get_string_value_sub_dict(citation, 'metadata', 'doi') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_doi>' + escape(get_string_value_sub_dict(citation, 'metadata', 'doi'))+ '</leganto-v1:metadata_doi>\n'
    if get_string_value_sub_dict(citation, 'metadata', 'volume') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_volume>' + escape(get_string_value_sub_dict(citation, 'metadata', 'volume'))+ '</leganto-v1:metadata_volume>\n'
    if get_string_value_sub_dict(citation, 'metadata', 'start_page') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_start_page>' + escape(get_string_value_sub_dict(citation, 'metadata', 'start_page'))+ '</leganto-v1:metadata_start_page>\n'
    if get_string_value_sub_dict(citation, 'metadata', 'end_page') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_end_page>' + escape(get_string_value_sub_dict(citation, 'metadata', 'end_page'))+ '</leganto-v1:metadata_end_page>\n'
    if get_string_value_sub_dict(citation, 'metadata', 'start_page2') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_start_page2>' + escape(get_string_value_sub_dict(citation, 'metadata', 'start_page2'))+ '</leganto-v1:metadata_start_page2>\n'
    if get_string_value_sub_dict(citation, 'metadata', 'end_page2') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_end_page2>' + escape(get_string_value_sub_dict(citation, 'metadata', 'end_page2'))+ '</leganto-v1:metadata_end_page2>\n'
    if get_string_value_sub_dict(citation, 'metadata','author_initials') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_author_initials>' + escape(get_string_value_sub_dict(citation, 'metadata','author_initials'))+ '</leganto-v1:metadata_author_initials>\n'
    if get_string_value_sub_dict(citation, 'metadata', 'part') is not None:
        bsm_values = bsm_values + '\t\t\t\t\t\t<leganto-v1:metadata_part>' + escape(get_string_value_sub_dict(citation, 'metadata', 'part'))+ '</leganto-v1:metadata_part>\n'

    return bsm_values
          #
          #
          # 'semester_year': citations['course_year'],
          #  'course_year': citations['course_year'],
          #  CONSPECTUS_SEMESTER_CODE_1: citations[CONSPECTUS_SEMESTER_CODE_1],
          #  CONSPECTUS_SEMESTER_CODE_2: citations[CONSPECTUS_SEMESTER_CODE_2],
          #  CONSPECTUS_SEMESTER_CODE_3: citations[CONSPECTUS_SEMESTER_CODE_3],
          #  CONSPECTUS_COURSE_YEAR: citations[CONSPECTUS_COURSE_YEAR],
          #  CONSPECTUS_COURSE_SEMESTER: citations[CONSPECTUS_COURSE_SEMESTER],
          #  'semester_1': get_string_value_dict(citations, CONSPECTUS_TERM_CODE_1),
          #  'semester_2': get_string_value_dict(citations, CONSPECTUS_TERM_CODE_2),
          #  'semester_3': get_string_value_dict(citations, CONSPECTUS_TERM_CODE_3),
          #  CONSPECTUS_SEMESTER_CODE_1: get_string_value_dict(citations, CONSPECTUS_SEMESTER_CODE_1),
          #  CONSPECTUS_SEMESTER_CODE_2: get_string_value_dict(citations, CONSPECTUS_SEMESTER_CODE_2),
          #  CONSPECTUS_SEMESTER_CODE_3: get_string_value_dict(citations, CONSPECTUS_SEMESTER_CODE_3),
          #  ACADEMIC_DEPARTMENT_NAME: get_string_value_dict(citations, ACADEMIC_DEPARTMENT_NAME),
          #  ACADEMIC_DEPARTMENT_CODE: get_string_value_dict(citations, ACADEMIC_DEPARTMENT_CODE),
          #  'citation_id': get_string_value_dict(citation, 'id'),
          #  'status_value': get_string_value_sub_dict(citation, 'status', VALUE),
          #  'status_desc': get_string_value_sub_dict(citation, 'status', 'desc'),
          #  'copyrights_status_value': get_string_value_sub_dict(citation, 'copyright_status',
          #                                                       VALUE),
          #  'copyrights_status_desc': get_string_value_sub_dict(citation, 'copyright_status',
          #                                                      'desc'),
          #  'secondary_type_value': get_string_value_sub_dict(citation, 'secondary_type', VALUE),
          #  'secondary_type_desc': get_string_value_sub_dict(citation, 'secondary_type', 'desc'),
          #  'type_value': get_string_value_sub_dict(citation, 'type', VALUE),
          #  'type_desc': get_string_value_sub_dict(citation, 'type', 'desc'),
          #  'metadata_title': get_string_value_sub_dict(citation, 'metadata', 'title'),
          #  'metadata_author': get_string_value_sub_dict(citation, 'metadata', 'author'),
          #  'metadata_publisher': get_string_value_sub_dict(citation, 'metadata', 'publisher'),
          #  'metadata_publication_date': get_string_value_sub_dict(citation, 'metadata',
          #                                                         'publication_date'),
          #  'metadata_isbn': get_string_value_sub_dict(citation, 'metadata', 'isbn'),
          #  'metadata_issn': get_string_value_sub_dict(citation, 'metadata', 'issn'),
          #  'metadata_mms_id': get_string_value_sub_dict(citation, 'metadata', 'mms_id'),
          #  'metadata_additional_person_name': get_string_value_sub_dict(citation, 'metadata',
          #                                                               'additional_person_name'),
          #  'metadata_place_of_publication': get_string_value_sub_dict(citation, 'metadata',
          #                                                             'place_of_publication'),
          #  'metadata_call_number': get_string_value_sub_dict(citation, 'metadata', 'call_number'),
          #  'metadata_note': get_string_value_sub_dict(citation, 'metadata', 'note'),
          #  'metadata_journal_title': get_string_value_sub_dict(citation, 'metadata',
          #                                                      'journal_title'),
          #  'metadata_article_title': get_string_value_sub_dict(citation, 'metadata',
          #                                                      'article_title'),
          #  'metadata_issue': get_string_value_sub_dict(citation, 'metadata', 'issue'),
          #  'metadata_editor': get_string_value_sub_dict(citation, 'metadata', 'editor'),
          #  'metadata_chapter': get_string_value_sub_dict(citation, 'metadata', 'chapter'),
          #  'metadata_chapter_title': get_string_value_sub_dict(citation, 'metadata',
          #                                                      'chapter_title'),
          #  'metadata_chapter_author': get_string_value_sub_dict(citation, 'metadata',
          #                                                       'chapter_author'),
          #  'metadata_year': get_string_value_sub_dict(citation, 'metadata', YEAR),
          #  'metadata_pages': get_string_value_sub_dict(citation, 'metadata', 'pages'),
          #  'metadata_source': get_string_value_sub_dict(citation, 'metadata', 'source'),
          #  'metadata_series_title_number': get_string_value_sub_dict(citation, 'metadata',
          #                                                            'series_title_number'),
          #  'metadata_pmid': get_string_value_sub_dict(citation, 'metadata', 'pmid'),
          #  'metadata_doi': get_string_value_sub_dict(citation, 'metadata', 'doi'),
          #  'metadata_volume': get_string_value_sub_dict(citation, 'metadata', 'volume'),
          #  'metadata_start_page': get_string_value_sub_dict(citation, 'metadata', 'start_page'),
          #  'metadata_end_page': get_string_value_sub_dict(citation, 'metadata', 'end_page'),
          #  'metadata_start_page2': get_string_value_sub_dict(citation, 'metadata', 'start_page2'),
          #  'metadata_end_page2': get_string_value_sub_dict(citation, 'metadata', 'end_page2'),
          #  'metadata_author_initials': get_string_value_sub_dict(citation, 'metadata',
          #                                                        'author_initials'),
          #  'metadata_part': get_string_value_sub_dict(citation, 'metadata', 'part'),
          #  'public_note': get_string_value_dict(citation, 'public_note'),
          #  'source1': get_string_value_dict(citation, 'source1'),
          #  'source2': get_string_value_dict(citation, 'source2'),
          #  'source3': get_string_value_dict(citation, 'source3'),
          #  'source4': get_string_value_dict(citation, 'source4'),
          #  'source5': get_string_value_dict(citation, 'source5'),
          #  'source6': get_string_value_dict(citation, 'source6'),
          #  'source7': get_string_value_dict(citation, 'source7'),
          #  'source8': get_string_value_dict(citation, 'source8'),
          #  'source9': get_string_value_dict(citation, 'source9'),
          #  'source10': get_string_value_dict(citation, 'source10'),
          #  'last_modified_date': date_remove_z(get_string_value_dict(citation,
          #                                                            'last_modified_date')),
          #  'section_info_id': get_string_value_sub_dict(citation, 'section_info', 'id'),
          #  'section_info_name': get_string_value_sub_dict(citation, 'section_info', 'name'),
          #  'section_info_description': get_string_value_sub_dict(citation, 'section_info',
          #                                                        'description'),
          #  'section_info_visibility': get_boolean_value_sub_dict(citation, 'section_info',
          #                                                        'visibility'),
          #  'section_info_tags_total_rec_cnt': get_string_value_sub_sub_dict(citation,
          #                                                                   'section_info',
          #                                                                   'section_tags',
          #                                                                   'total_record_count'),
          #  'section_info_section_tags_link': get_string_value_sub_sub_dict(citation,
          #                                                                  'section_info',
          #                                                                  'section_tags',
          #                                                                  'link'),
          #  'section_info_section_locked': get_boolean_value_sub_dict(citation, 'section_info',
          #                                                            'locked'),
          #  'file_link': get_string_value_dict(citation, 'file_link'),
          #  COURSE_LEGANTO_CODE: citations[COURSE_LEGANTO_CODE],
          #  DBH_INSTITUTION_CODE_EN: citations[DBH_INSTITUTION_CODE_EN],
          #  DBH_INSTITUTION_NAME_EN: citations[DBH_INSTITUTION_NAME_EN],
          #  DBH_DEPT_CODE_EN: citations[DBH_DEPT_CODE_EN],
          #  DBH_DEPT_NAME_EN: citations[DBH_DEPT_NAME_EN],
          #  DBH_DEPT_CODE_SSB_EN: citations[DBH_DEPT_CODE_SSB_EN],
          #  DBH_YEAR_EN: citations[DBH_YEAR_EN],
          #  DBH_SEMESTER_EN: citations[DBH_SEMESTER_EN],
          #  DBH_SEMESTER_NAME_EN: citations[DBH_SEMESTER_NAME_EN],
          #  DBH_COURSE_STUDY_PROGRAM_CODE_EN: citations[DBH_COURSE_STUDY_PROGRAM_CODE_EN],
          #  DBH_COURSE_STUDY_PROGRAM_NAME_EN: citations[DBH_COURSE_STUDY_PROGRAM_NAME_EN],
          #  DBH_COURSE_CODE_EN: citations[DBH_COURSE_CODE_EN],
          #  DBH_COURSE_NAME_EN: citations[DBH_COURSE_NAME_EN],
          #  DBH_LEVEL_CODE_EN: citations[DBH_LEVEL_CODE_EN],
          #  DBH_LEVEL_NAME_EN: citations[DBH_LEVEL_NAME_EN],
          #  DBH_COURSE_CREDIT_EN: citations[DBH_COURSE_CREDIT_EN],
          #  DBH_NUS_CODE_EN: citations[DBH_NUS_CODE_EN],
          #  DBH_STATUS_EN: citations[DBH_STATUS_EN],
          #  DBH_STATUS_NAME_EN: citations[DBH_STATUS_NAME_EN],
          #  DBH_LANGUAGE_EN: citations[DBH_LANGUAGE_EN],
          #  DBH_NAME_EN: citations[DBH_NAME_EN],
          #  DBH_AREA_CODE_EN: citations[DBH_AREA_CODE_EN],
          #  DBH_AREA_NAME_EN: citations[DBH_AREA_NAME_EN],
          #  DBH_TASK_EN: citations[DBH_TASK_EN]
          #  }

def get_es_values_citation(citation, citations):
    citation[CONSPECTUS_COURSE_CODE] = citations[CONSPECTUS_COURSE_CODE]
    citation[UNIT_CODE] = citations[UNIT_CODE]
    citation[CONSPECTUS_USE_UNIT_CODE] = citations[CONSPECTUS_USE_UNIT_CODE]
    citation[SUB_UNIT_CODE] = citations[SUB_UNIT_CODE]
    citation[SUB_SUB_UNIT_CODE] = citations[SUB_SUB_UNIT_CODE]
    citation[UNIT_NAME_NB] = citations[UNIT_NAME_NB]
    citation[UNIT_NAME_EN] = citations[UNIT_NAME_EN]
    citation[SUB_UNIT_NAME_NB] = citations[SUB_UNIT_NAME_NB]
    citation[SUB_UNIT_NAME_EN] = citations[SUB_UNIT_NAME_EN]
    citation[SUB_SUB_UNIT_NAME_NB] = citations[SUB_SUB_UNIT_NAME_NB]
    citation[SUB_SUB_UNIT_NAME_EN] = citations[SUB_SUB_UNIT_NAME_EN]

    val = {ORGANISATION: get_string_value_dict(citation, CONSPECTUS_USE_UNIT_CODE),
           CONSPECTUS_COURSE_CODE: citation[CONSPECTUS_COURSE_CODE],
           CONSPECTUS_COURSE_NAME: citation['name'],
           CONSPECTUS_USE_UNIT_CODE: get_string_value_dict(citation, CONSPECTUS_USE_UNIT_CODE),
           UNIT_CODE: get_string_value_dict(citation, UNIT_CODE),
           SUB_UNIT_CODE: get_string_value_dict(citation, SUB_UNIT_CODE),
           SUB_SUB_UNIT_CODE: get_string_value_dict(citation, SUB_SUB_UNIT_CODE),
           UNIT_NAME_NB: get_string_value_dict(citation, UNIT_NAME_NB),
           SUB_UNIT_NAME_NB: get_string_value_dict(citation, SUB_UNIT_NAME_NB),
           SUB_SUB_UNIT_NAME_NB: get_string_value_dict(citation, SUB_SUB_UNIT_NAME_NB),
           UNIT_NAME_EN: get_string_value_dict(citation, UNIT_NAME_EN),
           SUB_UNIT_NAME_EN: get_string_value_dict(citation, SUB_UNIT_NAME_EN),
           SUB_SUB_UNIT_NAME_EN: get_string_value_dict(citation, SUB_SUB_UNIT_NAME_EN),
           PROCESSING_COMMENT: get_string_value_dict(citations, PROCESSING_COMMENT),
           'semester_year': citations['course_year'],
           'course_year': citations['course_year'],
           CONSPECTUS_SEMESTER_CODE_1: citations[CONSPECTUS_SEMESTER_CODE_1],
           CONSPECTUS_SEMESTER_CODE_2: citations[CONSPECTUS_SEMESTER_CODE_2],
           CONSPECTUS_SEMESTER_CODE_3: citations[CONSPECTUS_SEMESTER_CODE_3],
           CONSPECTUS_COURSE_YEAR: citations[CONSPECTUS_COURSE_YEAR],
           CONSPECTUS_COURSE_SEMESTER: citations[CONSPECTUS_COURSE_SEMESTER],
           'semester_1': get_string_value_dict(citations, CONSPECTUS_TERM_CODE_1),
           'semester_2': get_string_value_dict(citations, CONSPECTUS_TERM_CODE_2),
           'semester_3': get_string_value_dict(citations, CONSPECTUS_TERM_CODE_3),
           CONSPECTUS_SEMESTER_CODE_1: get_string_value_dict(citations, CONSPECTUS_SEMESTER_CODE_1),
           CONSPECTUS_SEMESTER_CODE_2: get_string_value_dict(citations, CONSPECTUS_SEMESTER_CODE_2),
           CONSPECTUS_SEMESTER_CODE_3: get_string_value_dict(citations, CONSPECTUS_SEMESTER_CODE_3),
           ACADEMIC_DEPARTMENT_NAME: get_string_value_dict(citations, ACADEMIC_DEPARTMENT_NAME),
           ACADEMIC_DEPARTMENT_CODE: get_string_value_dict(citations, ACADEMIC_DEPARTMENT_CODE),
           'citation_id': get_string_value_dict(citation, 'id'),
           'status_value': get_string_value_sub_dict(citation, 'status', VALUE),
           'status_desc': get_string_value_sub_dict(citation, 'status', 'desc'),
           'copyrights_status_value': get_string_value_sub_dict(citation, 'copyright_status',
                                                                VALUE),
           'copyrights_status_desc': get_string_value_sub_dict(citation, 'copyright_status',
                                                               'desc'),
           'secondary_type_value': get_string_value_sub_dict(citation, 'secondary_type', VALUE),
           'secondary_type_desc': get_string_value_sub_dict(citation, 'secondary_type', 'desc'),
           'type_value': get_string_value_sub_dict(citation, 'type', VALUE),
           'type_desc': get_string_value_sub_dict(citation, 'type', 'desc'),
           'metadata_title': get_string_value_sub_dict(citation, 'metadata', 'title'),
           'metadata_author': get_string_value_sub_dict(citation, 'metadata', 'author'),
           'metadata_publisher': get_string_value_sub_dict(citation, 'metadata', 'publisher'),
           'metadata_publication_date': get_string_value_sub_dict(citation, 'metadata',
                                                                  'publication_date'),
           'metadata_isbn': get_string_value_sub_dict(citation, 'metadata', 'isbn'),
           'metadata_issn': get_string_value_sub_dict(citation, 'metadata', 'issn'),
           'metadata_mms_id': get_string_value_sub_dict(citation, 'metadata', 'mms_id'),
           'metadata_additional_person_name': get_string_value_sub_dict(citation, 'metadata',
                                                                        'additional_person_name'),
           'metadata_place_of_publication': get_string_value_sub_dict(citation, 'metadata',
                                                                      'place_of_publication'),
           'metadata_call_number': get_string_value_sub_dict(citation, 'metadata', 'call_number'),
           'metadata_note': get_string_value_sub_dict(citation, 'metadata', 'note'),
           'metadata_journal_title': get_string_value_sub_dict(citation, 'metadata',
                                                               'journal_title'),
           'metadata_article_title': get_string_value_sub_dict(citation, 'metadata',
                                                               'article_title'),
           'metadata_issue': get_string_value_sub_dict(citation, 'metadata', 'issue'),
           'metadata_editor': get_string_value_sub_dict(citation, 'metadata', 'editor'),
           'metadata_chapter': get_string_value_sub_dict(citation, 'metadata', 'chapter'),
           'metadata_chapter_title': get_string_value_sub_dict(citation, 'metadata',
                                                               'chapter_title'),
           'metadata_chapter_author': get_string_value_sub_dict(citation, 'metadata',
                                                                'chapter_author'),
           'metadata_year': get_string_value_sub_dict(citation, 'metadata', YEAR),
           'metadata_pages': get_string_value_sub_dict(citation, 'metadata', 'pages'),
           'metadata_source': get_string_value_sub_dict(citation, 'metadata', 'source'),
           'metadata_series_title_number': get_string_value_sub_dict(citation, 'metadata',
                                                                     'series_title_number'),
           'metadata_pmid': get_string_value_sub_dict(citation, 'metadata', 'pmid'),
           'metadata_doi': get_string_value_sub_dict(citation, 'metadata', 'doi'),
           'metadata_volume': get_string_value_sub_dict(citation, 'metadata', 'volume'),
           'metadata_start_page': get_string_value_sub_dict(citation, 'metadata', 'start_page'),
           'metadata_end_page': get_string_value_sub_dict(citation, 'metadata', 'end_page'),
           'metadata_start_page2': get_string_value_sub_dict(citation, 'metadata', 'start_page2'),
           'metadata_end_page2': get_string_value_sub_dict(citation, 'metadata', 'end_page2'),
           'metadata_author_initials': get_string_value_sub_dict(citation, 'metadata',
                                                                 'author_initials'),
           'metadata_part': get_string_value_sub_dict(citation, 'metadata', 'part'),
           'public_note': get_string_value_dict(citation, 'public_note'),
           'source1': get_string_value_dict(citation, 'source1'),
           'source2': get_string_value_dict(citation, 'source2'),
           'source3': get_string_value_dict(citation, 'source3'),
           'source4': get_string_value_dict(citation, 'source4'),
           'source5': get_string_value_dict(citation, 'source5'),
           'source6': get_string_value_dict(citation, 'source6'),
           'source7': get_string_value_dict(citation, 'source7'),
           'source8': get_string_value_dict(citation, 'source8'),
           'source9': get_string_value_dict(citation, 'source9'),
           'source10': get_string_value_dict(citation, 'source10'),
           'last_modified_date': date_remove_z(get_string_value_dict(citation,
                                                                     'last_modified_date')),
           'section_info_id': get_string_value_sub_dict(citation, 'section_info', 'id'),
           'section_info_name': get_string_value_sub_dict(citation, 'section_info', 'name'),
           'section_info_description': get_string_value_sub_dict(citation, 'section_info',
                                                                 'description'),
           'section_info_visibility': get_boolean_value_sub_dict(citation, 'section_info',
                                                                 'visibility'),
           'section_info_tags_total_rec_cnt': get_string_value_sub_sub_dict(citation,
                                                                            'section_info',
                                                                            'section_tags',
                                                                            'total_record_count'),
           'section_info_section_tags_link': get_string_value_sub_sub_dict(citation,
                                                                           'section_info',
                                                                           'section_tags',
                                                                           'link'),
           'section_info_section_locked': get_boolean_value_sub_dict(citation, 'section_info',
                                                                     'locked'),
           'file_link': get_string_value_dict(citation, 'file_link'),
           COURSE_LEGANTO_CODE: citations[COURSE_LEGANTO_CODE],
           DBH_INSTITUTION_CODE_EN: citations[DBH_INSTITUTION_CODE_EN],
           DBH_INSTITUTION_NAME_EN: citations[DBH_INSTITUTION_NAME_EN],
           DBH_DEPT_CODE_EN: citations[DBH_DEPT_CODE_EN],
           DBH_DEPT_NAME_EN: citations[DBH_DEPT_NAME_EN],
           DBH_DEPT_CODE_SSB_EN: citations[DBH_DEPT_CODE_SSB_EN],
           DBH_YEAR_EN: citations[DBH_YEAR_EN],
           DBH_SEMESTER_EN: citations[DBH_SEMESTER_EN],
           DBH_SEMESTER_NAME_EN: citations[DBH_SEMESTER_NAME_EN],
           DBH_COURSE_STUDY_PROGRAM_CODE_EN: citations[DBH_COURSE_STUDY_PROGRAM_CODE_EN],
           DBH_COURSE_STUDY_PROGRAM_NAME_EN: citations[DBH_COURSE_STUDY_PROGRAM_NAME_EN],
           DBH_COURSE_CODE_EN: citations[DBH_COURSE_CODE_EN],
           DBH_COURSE_NAME_EN: citations[DBH_COURSE_NAME_EN],
           DBH_LEVEL_CODE_EN: citations[DBH_LEVEL_CODE_EN],
           DBH_LEVEL_NAME_EN: citations[DBH_LEVEL_NAME_EN],
           DBH_COURSE_CREDIT_EN: citations[DBH_COURSE_CREDIT_EN],
           DBH_NUS_CODE_EN: citations[DBH_NUS_CODE_EN],
           DBH_STATUS_EN: citations[DBH_STATUS_EN],
           DBH_STATUS_NAME_EN: citations[DBH_STATUS_NAME_EN],
           DBH_LANGUAGE_EN: citations[DBH_LANGUAGE_EN],
           DBH_NAME_EN: citations[DBH_NAME_EN],
           DBH_AREA_CODE_EN: citations[DBH_AREA_CODE_EN],
           DBH_AREA_NAME_EN: citations[DBH_AREA_NAME_EN],
           DBH_TASK_EN: citations[DBH_TASK_EN]
           }
    return val


def get_csv_values_citation(citation):
    return {ORGANISATION: citation[ORGANISATION],
            CONSPECTUS_COURSE_CODE: citation[CONSPECTUS_COURSE_CODE],
            CONSPECTUS_COURSE_NAME: citation[CONSPECTUS_COURSE_NAME],
            CONSPECTUS_USE_UNIT_CODE: citation[CONSPECTUS_USE_UNIT_CODE],
            CONSPECTUS_USE_UNIT_NAME_NB: citation[CONSPECTUS_USE_UNIT_NAME_NB],
            CONSPECTUS_USE_UNIT_NAME_EN: citation[CONSPECTUS_USE_UNIT_NAME_EN],
            UNIT_CODE: citation[UNIT_CODE],
            SUB_UNIT_CODE: citation[SUB_UNIT_CODE],
            SUB_SUB_UNIT_CODE: citation[SUB_SUB_UNIT_CODE],
            UNIT_NAME_NB: citation[UNIT_NAME_NB],
            SUB_UNIT_NAME_NB: citation[SUB_UNIT_NAME_NB],
            SUB_SUB_UNIT_NAME_NB: citation[SUB_SUB_UNIT_NAME_NB],
            UNIT_NAME_EN: citation[UNIT_NAME_EN],
            SUB_UNIT_NAME_EN: citation[SUB_UNIT_NAME_EN],
            SUB_SUB_UNIT_NAME_EN: citation[SUB_SUB_UNIT_NAME_EN],
            PROCESSING_COMMENT: citation[PROCESSING_COMMENT],
            'semester_year': citation[CONSPECTUS_YEAR],
            'semester_1': citation[CONSPECTUS_TERM_CODE_1],
            'semester_2': citation[CONSPECTUS_TERM_CODE_2],
            'semester_3': citation[CONSPECTUS_TERM_CODE_3],
            CONSPECTUS_SEMESTER_CODE_1: citation[CONSPECTUS_SEMESTER_CODE_1],
            CONSPECTUS_SEMESTER_CODE_2: citation[CONSPECTUS_SEMESTER_CODE_2],
            CONSPECTUS_SEMESTER_CODE_3: citation[CONSPECTUS_SEMESTER_CODE_3],
            CONSPECTUS_COURSE_YEAR: citation[CONSPECTUS_COURSE_YEAR],
            CONSPECTUS_COURSE_SEMESTER: citation[CONSPECTUS_COURSE_SEMESTER],
            ACADEMIC_DEPARTMENT_NAME: citation[ACADEMIC_DEPARTMENT_NAME],
            ACADEMIC_DEPARTMENT_CODE: citation[ACADEMIC_DEPARTMENT_CODE],
            'citation_id': get_string_value_dict(citation, 'id'),
            'status_value': get_string_value_sub_dict(citation, 'status', VALUE),
            'status_desc': get_string_value_sub_dict(citation, 'status', 'desc'),
            'copyrights_status_value': get_string_value_sub_dict(citation, 'copyright_status',
                                                                 VALUE),
            'copyrights_status_desc': get_string_value_sub_dict(citation, 'copyright_status',
                                                                'desc'),
            'secondary_type_value': get_string_value_sub_dict(citation, 'secondary_type', VALUE),
            'secondary_type_desc': get_string_value_sub_dict(citation, 'secondary_type', 'desc'),
            'type_value': get_string_value_sub_dict(citation, 'type', VALUE),
            'type_desc': get_string_value_sub_dict(citation, 'type', 'desc'),
            'metadata_title': get_string_value_sub_dict(citation, 'metadata', 'title'),
            'metadata_author': get_string_value_sub_dict(citation, 'metadata', 'author'),
            'metadata_publisher': get_string_value_sub_dict(citation, 'metadata', 'publisher'),
            'metadata_publication_date': get_string_value_sub_dict(citation, 'metadata',
                                                                   'publication_date'),
            'metadata_isbn': get_string_value_sub_dict(citation, 'metadata', 'isbn'),
            'metadata_issn': get_string_value_sub_dict(citation, 'metadata', 'issn'),
            'metadata_mms_id': get_string_value_sub_dict(citation, 'metadata', 'mms_id'),
            'metadata_additional_person_name': get_string_value_sub_dict(citation, 'metadata',
                                                                         'additional_person_name'),
            'metadata_place_of_publication': get_string_value_sub_dict(citation, 'metadata',
                                                                       'place_of_publication'),
            'metadata_call_number': get_string_value_sub_dict(citation, 'metadata', 'call_number'),
            'metadata_note': get_string_value_sub_dict(citation, 'metadata', 'note'),
            'metadata_journal_title': get_string_value_sub_dict(citation, 'metadata',
                                                                'journal_title'),
            'metadata_article_title': get_string_value_sub_dict(citation, 'metadata',
                                                                'article_title'),
            'metadata_issue': get_string_value_sub_dict(citation, 'metadata', 'issue'),
            'metadata_editor': get_string_value_sub_dict(citation, 'metadata', 'editor'),
            'metadata_chapter': get_string_value_sub_dict(citation, 'metadata', 'chapter'),
            'metadata_chapter_title': get_string_value_sub_dict(citation, 'metadata',
                                                                'chapter_title'),
            'metadata_chapter_author': get_string_value_sub_dict(citation, 'metadata',
                                                                 'chapter_author'),
            'metadata_year': get_string_value_sub_dict(citation, 'metadata', YEAR),
            'metadata_pages': get_string_value_sub_dict(citation, 'metadata', 'pages'),
            'metadata_source': get_string_value_sub_dict(citation, 'metadata', 'source'),
            'metadata_series_title_number': get_string_value_sub_dict(citation, 'metadata',
                                                                      'series_title_number'),
            'metadata_pmid': get_string_value_sub_dict(citation, 'metadata', 'pmid'),
            'metadata_doi': get_string_value_sub_dict(citation, 'metadata', 'doi'),
            'metadata_volume': get_string_value_sub_dict(citation, 'metadata', 'volume'),
            'metadata_start_page': get_string_value_sub_dict(citation, 'metadata', 'start_page'),
            'metadata_end_page': get_string_value_sub_dict(citation, 'metadata', 'end_page'),
            'metadata_start_page2': get_string_value_sub_dict(citation, 'metadata', 'start_page2'),
            'metadata_end_page2': get_string_value_sub_dict(citation, 'metadata', 'end_page2'),
            'metadata_author_initials': get_string_value_sub_dict(citation, 'metadata',
                                                                  'author_initials'),
            'metadata_part': get_string_value_sub_dict(citation, 'metadata', 'part'),
            'public_note': get_string_value_dict(citation, 'public_note'),
            'source1': get_string_value_dict(citation, 'source1'),
            'source2': get_string_value_dict(citation, 'source2'),
            'source3': get_string_value_dict(citation, 'source3'),
            'source4': get_string_value_dict(citation, 'source4'),
            'source5': get_string_value_dict(citation, 'source5'),
            'source6': get_string_value_dict(citation, 'source6'),
            'source7': get_string_value_dict(citation, 'source7'),
            'source8': get_string_value_dict(citation, 'source8'),
            'source9': get_string_value_dict(citation, 'source9'),
            'source10': get_string_value_dict(citation, 'source10'),
            'last_modified_date': date_remove_z(get_string_value_dict(citation,
                                                                      'last_modified_date')),
            'section_info_id': get_string_value_sub_dict(citation, 'section_info', 'id'),
            'section_info_name': get_string_value_sub_dict(citation, 'section_info', 'name'),
            'section_info_description': get_string_value_sub_dict(citation, 'section_info',
                                                                  'description'),
            'section_info_visibility': get_boolean_value_sub_dict(citation, 'section_info',
                                                                  'visibility'),
            'section_info_tags_total_rec_cnt': get_string_value_sub_sub_dict(citation,
                                                                             'section_info',
                                                                             'section_tags',
                                                                             'total_record_count'),
            'section_info_section_tags_link': get_string_value_sub_sub_dict(citation,
                                                                            'section_info',
                                                                            'section_tags',
                                                                            'link'),
            'section_info_section_locked': get_boolean_value_sub_dict(citation, 'section_info',
                                                                      'locked'),
            'file_link': get_string_value_dict(citation, 'file_link'),
            'fk_reading_list_id': citation[READING_LIST_ID],
            'fk_course_id': citation[COURSE_ID],
            COURSE_LEGANTO_CODE: citation[COURSE_LEGANTO_CODE],
            DBH_INSTITUTION_CODE_EN: get_string_value_dict(citation, DBH_INSTITUTION_CODE_EN),
            DBH_INSTITUTION_NAME_EN: get_string_value_dict(citation, DBH_INSTITUTION_NAME_EN),
            DBH_DEPT_CODE_EN: get_string_value_dict(citation, DBH_DEPT_CODE_EN),
            DBH_DEPT_NAME_EN: get_string_value_dict(citation, DBH_DEPT_NAME_EN),
            DBH_DEPT_CODE_SSB_EN: get_string_value_dict(citation, DBH_DEPT_CODE_SSB_EN),
            DBH_YEAR_EN: get_string_value_dict(citation, DBH_YEAR_EN),
            DBH_SEMESTER_EN: get_string_value_dict(citation, DBH_SEMESTER_EN),
            DBH_SEMESTER_NAME_EN: get_string_value_dict(citation, DBH_SEMESTER_NAME_EN),
            DBH_COURSE_STUDY_PROGRAM_CODE_EN: get_string_value_dict(citation, DBH_COURSE_STUDY_PROGRAM_CODE_EN),
            DBH_COURSE_STUDY_PROGRAM_NAME_EN: get_string_value_dict(citation, DBH_COURSE_STUDY_PROGRAM_NAME_EN),
            DBH_COURSE_CODE_EN: get_string_value_dict(citation, DBH_COURSE_CODE_EN),
            DBH_COURSE_NAME_EN: get_string_value_dict(citation, DBH_COURSE_NAME_EN),
            DBH_LEVEL_CODE_EN: get_string_value_dict(citation, DBH_LEVEL_CODE_EN),
            DBH_LEVEL_NAME_EN: get_string_value_dict(citation, DBH_LEVEL_NAME_EN),
            DBH_COURSE_CREDIT_EN: get_string_value_dict(citation, DBH_COURSE_CREDIT_EN),
            DBH_NUS_CODE_EN: get_string_value_dict(citation, DBH_NUS_CODE_EN),
            DBH_STATUS_EN: get_string_value_dict(citation, DBH_STATUS_EN),
            DBH_STATUS_NAME_EN: get_string_value_dict(citation, DBH_STATUS_NAME_EN),
            DBH_LANGUAGE_EN: get_string_value_dict(citation, DBH_LANGUAGE_EN),
            DBH_NAME_EN: get_string_value_dict(citation, DBH_NAME_EN),
            DBH_AREA_CODE_EN: get_string_value_dict(citation, DBH_AREA_CODE_EN),
            DBH_AREA_NAME_EN: get_string_value_dict(citation, DBH_AREA_NAME_EN),
            DBH_TASK_EN: get_string_value_dict(citation, DBH_TASK_EN)
            }


def is_digit(string_to_check):
    return string_to_check.replace('.', '', 1).isdigit()


def get_checksum_note(note):
    values = note['content'] + note['creation_date']
    checksum = sha256(values.encode()).hexdigest()
    return checksum


def from_value_get_name_uit(organisation, dep_value, dep_name):
    # UiT id's appear to double a 0
    # Note a single 0 is extracted later so here we remove if it ends with 0000 or 00000, the 00 is
    # taken care of later
    if dep_value.endswith('000000'):
        dep_value = dep_value[:len(dep_value) - 2]
    elif dep_value.endswith('0000'):
        dep_value = dep_value[:len(dep_value) - 1]
    # In some cases the value is missing a 0, so we add it
    elif dep_value == '186000':
        dep_value = '1860000'
    # The course with academic:department.value == 18633150, needs to get an additional 0 added
    # so it can be processed below
    elif dep_value == '18633150':
        dep_value = '186331500'

    # In some cases, courses are listed with a faculty id, but no name
    if dep_value.count('_') == 2:
        dep_value = dep_value.replace('_', '')
        dep_value = dep_value + '00'
    with open('./json/' + organisation + '_cristin.json') as org_file:
        data = json.load(org_file)
        for i in range(len(data)):
            cristin_unit = data[i]
            stripped_cristin_dep_value = cristin_unit['cristin_unit_id'].replace('.', '')
            # There is a en extra 0 on UiT data
            length = len(dep_value)
            cut_dep_value = dep_value[:length - 1]
            if cut_dep_value == stripped_cristin_dep_value:
                if isinstance(cristin_unit['unit_name'], dict):
                    return cristin_unit['cristin_unit_id'], cristin_unit['unit_name']['nb']
                else:
                    return cristin_unit['cristin_unit_id'], cristin_unit['unit_name']
        return dep_value, dep_name


def fix_dept_value(organisation, dep_value, dep_name):
    correct_dep_value = dep_value
    potential_dep_value = dep_value
    potential_dep_name = dep_name

    with open('./json/' + organisation + '_cristin_id_list.json') as org_file:
        data = json.load(org_file)
        for i in range(len(data)):
            val = data[i]
            if dep_name in val.keys():
                correct_dep_value = val[dep_name]['id']
                # Just do a check to make sure the actual dep_value is similar to the identified one
                # i.e. 186331400 is similar to 186.33.14.00. We remove the '.' and check that
                # strings are contained within each other
                stripped_dep_value = correct_dep_value.replace('.', '')

                if stripped_dep_value not in dep_value:
                    potential_dep_value = val[dep_name]['id']
                    potential_dep_name = dep_name
                    print(
                        "\tFound potential match [{}] and [{}]".format(dep_value,
                                                                       correct_dep_value))
                else:
                    correct_dep_value = val[dep_name]['id']
                    print(
                        "\tFound actual match [{}] and [{}]".format(dep_value, correct_dep_value))

        # We did not find an actual match, check the potential match
        if correct_dep_value == dep_value:
            # We have a potential map
            if potential_dep_value != dep_value:
                correct_dep_value = potential_dep_value

        return correct_dep_value, potential_dep_name


def semester_to_english(semester_norwegian):
    if semester_norwegian is None:
        return ""
    elif 'høst' in semester_norwegian.lower():
        return 'autumn'
    elif 'vår' in semester_norwegian.lower():
        return 'spring'
    elif 'sommer' in semester_norwegian.lower():
        return 'summer'
    # The next three are to make sure that a semester always has a lower case
    elif 'autumn' in semester_norwegian.lower():
        return 'autumn'
    elif 'spring' in semester_norwegian.lower():
        return 'spring'
    elif 'summer' in semester_norwegian.lower():
        return 'summer'
    else:
        return 'unknown semester ' + semester_norwegian


def to_norwegian(semester_english):
    if semester_english is None:
        return ""
    elif 'autumn' in semester_english.lower():
        return 'høst'
    elif 'spring' in semester_english.lower():
        return 'vår'
    elif 'summer' in semester_english.lower():
        return 'sommer'
    else:
        return 'unknown semester ' + semester_english


# Lists of known keys in JSON payloads. Used so we can check if the API returns additional
# attributes that we are not aware of.
known_course_keys = ['id', CONSPECTUS_COURSE_CODE, 'name', 'section', 'status', START_DATE, END_DATE,
                     'weekly_hours', CODE, 'participants', YEAR, 'created_by', 'created_date', 'last_modified_by', 'last_modified_date',
                     'rolled_from', 'link', ACADEMIC_DEPARTMENT, 'processing_department', TERM, 'instructor',
                     'campus', 'searchable_id', 'note', COURSE_LEGANTO_CODE, CONSPECTUS_USE_UNIT_CODE,
                     UNIT_CODE, SUB_UNIT_CODE, SUB_SUB_UNIT_CODE, UNIT_NAME_NB, SUB_UNIT_NAME_NB,
                     SUB_SUB_UNIT_NAME_NB, UNIT_NAME_EN, SUB_UNIT_NAME_EN, SUB_SUB_UNIT_NAME_EN,
                     PROCESSING_COMMENT]

known_course_academic_department_keys = [VALUE, 'desc']
known_course_processing_department_keys = [VALUE, 'desc']
known_reading_list_keys = ['id', CONSPECTUS_COURSE_CODE, 'name', 'due_back_date', 'order', 'note', 'description',
                           'locked', 'last_modified_date', 'link', 'publishingStatus', 'syllabus',
                           'status', 'visibility', 'notes', 'citations', 'blocked', 'conspectus_term_code_1',
                           CONSPECTUS_TERM_NAME_1, CONSPECTUS_TERM_CODE_2, CONSPECTUS_TERM_NAME_2,
                           CONSPECTUS_TERM_CODE_3,
                           CONSPECTUS_TERM_NAME_3, YEAR]
known_reading_list_status_keys = [VALUE, 'desc']
known_reading_list_syllabus_keys = ['url', 'file']
known_reading_list_visibility_keys = [VALUE, 'desc']
known_reading_list_publishing_status_keys = [VALUE, 'desc']
known_citation_keys = ['id', 'open_url', 'leganto_permalink', 'public_note', 'source1', 'source2',
                       'source3', 'source4', 'source5', 'source6', 'source7', 'source8', 'source9',
                       'source10', 'last_modified_date', 'link', 'file_link']
known_citation_status_keys = [VALUE, 'desc']
known_citation_copyrights_status_keys = [VALUE, 'desc']
known_citation_type_keys = [VALUE, 'desc']
known_citation_metadata_keys = ['title', 'author', 'publisher', 'publication_date', 'edition',
                                'isbn', 'issn', 'mms_id', 'additional_person_name',
                                'place_of_publication', 'note', 'issue', 'editor', 'chapter',
                                YEAR,
                                'pages', 'source', 'pmid', 'doi', 'volume', 'part', 'call_number',
                                'journal_title', 'article_title', 'chapter_title', 'chapter_author',
                                'series_title_number', 'start_page', 'end_page', 'start_page2',
                                'end_page2', "author_initials"]
known_citation_section_info_keys = ['id', 'name', 'description', 'visibility', 'section_locked',
                                    'section_tags', START_DATE, 'end_date']

org_information = [
    {AHO: {'id': '189', 'name': ' Arkitektur- og designhøgskolen'}},
    {DMMH: {'id': '253', 'name': 'Dronning Mauds Minne Høgskole for barnehageutdanning'}},
    {'him': {'id': '211', 'name': 'Høgskolen i Molde'}},
    {'hio': {'id': '224', 'name': 'Høgskolen i Østfold'}},
    {'hk': {'id': '1615', 'name': 'Høyskolen Kristiania'}},
    {HVL: {'id': '203', 'name': 'Høgskulen på Vestlandet'}},
    {'hvo': {'id': '223', 'name': 'Høgskulen i Volda'}},
    {INN: {'id': '209', 'name': 'Høgskolen i innlandet'}},
    {LDH: {'id': '230', 'name': 'Lovisenberg diakonale høgskole'}},
    {MF: {'id': '190', 'name': 'MF vitenskapelig høyskole'}},
    {NHH: {'id': '191', 'name': 'Norges handelshøyskole'}},
    {NLA: {'id': '254', 'name': 'NLA Høgskolen'}},
    {NMBU: {'id': '192', 'name': 'Norges miljø- og biovitenskapelige universitet'}},
    {NORD: {'id': '204', 'name': 'Nord universitet'}},
    {NTNU: {'id': '194', 'name': 'NTNU'}},
    {OSLOMET: {'id': '215', 'name': 'OsloMet'}},
    {UiA: {'id': '201', 'name': 'Universitetet i Agder'}},
    {UiB: {'id': '184', 'name': 'Universitetet i Bergen'}},
    {UiO: {'id': '185', 'name': 'Universitetet i Oslo'}},
    {UiS: {'id': '217', 'name': 'Universitetet i Stavanger'}},
    {UiT: {'id': '186', 'name': 'Universitetet i Tromsø'}},
    {USN: {'id': '222', 'name': 'Universitetet i Sørøst-Norge'}},
    {VID: {'id': '251', 'name': 'VID vitenskapelige høgskole'}}]


def get_semester_name(semester_code):
    if semester_code.lower().startswith('h'):
        return LEGANTO_AUTUMN
    elif semester_code.lower().startswith('v'):
        return LEGANTO_SPRING
    elif semester_code.lower().startswith('y'): # Yearly
        return 'Årlig'
    elif semester_code.lower().startswith('t'): # Term
        return 'Nettkurs'
    elif semester_code.lower().startswith('s'): # Summer
        return 'Sommer'
    else:
        return UNKNOWN

