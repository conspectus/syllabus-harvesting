ORG_NAME = 'org_name'
VALUE = 'value'
KNOWN_LEGANTO_CODE = 'known_leganto_code'
AHO = 'aho'
DMMH = 'dmmh'
FORSVARET = 'forsvaret'
HIMOLDE = 'himolde'
HIOF = 'hiof'
HIVOLDA = 'hivolda'
HVL = 'hvl'
INN = 'inn'
KRISTIANIA = 'kristiania'
KRUS = 'krus'
LDH = 'ldh'
MF = 'mf'
NHH = 'nhh'
NLA = 'nla'
NMBU = 'nmbu'
NORD = 'nord'
NTNU = 'ntnu'
OSLOMET = 'oslomet'
PHS = 'phs'
SAMAS = 'samas'
UiA = 'uia'
UiB = 'uib'
UiO = 'uio'
UiS = 'uis'
UiT = 'uit'
USN = 'usn'
VID = 'vid'

ORGS = {
    AHO: {ORG_NAME: 'Arkitektur- og designhøgskolen i Oslo', VALUE: '189.0.0.0', KNOWN_LEGANTO_CODE: '189'},
    DMMH: {ORG_NAME: 'Dronning Mauds Minne Høgskole for barnehagelærerutdanning', VALUE: '253.0.0.0',
           KNOWN_LEGANTO_CODE: '253'},
    FORSVARET: {ORG_NAME: 'Forsvarets høgskole', VALUE: '1627.0.0.0', KNOWN_LEGANTO_CODE: '1627'},
    HIMOLDE: {ORG_NAME: 'Høgskolen i Molde - Vitenskapelig høgskole i logistikk', VALUE: '211.0.0.0',
              KNOWN_LEGANTO_CODE: '211'},
    HIOF: {ORG_NAME: 'Høgskolen i Østfold', VALUE: '224.0.0.0', KNOWN_LEGANTO_CODE: '224'},
    HIVOLDA: {ORG_NAME: 'Høgskulen i Volda', VALUE: '223.0.0.0', KNOWN_LEGANTO_CODE: '223'},
    HVL: {ORG_NAME: 'Høgskulen på Vestlandet', VALUE: '203.0.0.0', KNOWN_LEGANTO_CODE: '203'},
    INN: {ORG_NAME: 'Høgskolen i Innlandet', VALUE: '209.0.0.0', KNOWN_LEGANTO_CODE: '209'},
    KRISTIANIA: {ORG_NAME: 'Høyskolen Kristiania', VALUE: '1615.0.0.0', KNOWN_LEGANTO_CODE: '1615'},
    KRUS: {ORG_NAME: 'Kriminalomsorgens høgskole og utdanningssenter KRUS', VALUE: '1661.0.0.0',
           KNOWN_LEGANTO_CODE: '1661'},
    LDH: {ORG_NAME: 'Lovisenberg diakonale høgskole', VALUE: '230.0.0.0', KNOWN_LEGANTO_CODE: '230'},
    MF: {ORG_NAME: 'MF vitenskapelig høyskole for teologi, religion og samfunn', VALUE: '190.0.0.0',
         KNOWN_LEGANTO_CODE: '190'},
    NHH: {ORG_NAME: 'Norges Handelshøyskole', VALUE: '191.0.0.0', KNOWN_LEGANTO_CODE: '191'},
    NLA: {ORG_NAME: 'NLA Høgskolen', VALUE: '254.0.0.0', KNOWN_LEGANTO_CODE: '254'},
    NMBU: {ORG_NAME: 'Norges miljø- og biovitenskapelige universitet', VALUE: '192.0.0.0', KNOWN_LEGANTO_CODE: '192'},
    NORD: {ORG_NAME: 'Nord universitet', VALUE: '204.0.0.0', KNOWN_LEGANTO_CODE: '204'},
    NTNU: {ORG_NAME: 'Norges teknisk-naturvitenskapelige universitet', VALUE: '194.0.0.0', KNOWN_LEGANTO_CODE: '194'},
    OSLOMET: {ORG_NAME: 'OsloMet - storbyuniversitetet', VALUE: '215.0.0.0', KNOWN_LEGANTO_CODE: '215_0_0'},
    PHS: {ORG_NAME: 'Politihøgskolen', VALUE: '233.0.0.0', KNOWN_LEGANTO_CODE: '233'},
    SAMAS: {ORG_NAME: 'Sámi allaskuvla/Sámi University of Applied Sciences', VALUE: '231.0.0.0',
            KNOWN_LEGANTO_CODE: '231'},
    UiA: {ORG_NAME: 'Universitetet i Agder', VALUE: '201.0.0.0', KNOWN_LEGANTO_CODE: '201'},
    UiB: {ORG_NAME: 'Universitetet i Bergen', VALUE: '184.0.0.0', KNOWN_LEGANTO_CODE: '184'},
    UiO: {ORG_NAME: 'Universitetet i Oslo', VALUE: '185.90.0.0', KNOWN_LEGANTO_CODE: '185'},
    UiS: {ORG_NAME: 'Universitetet i Stavanger', VALUE: '217.0.0.0', KNOWN_LEGANTO_CODE: '217'},
    USN: {ORG_NAME: 'Universitetet i Sørøst-Norge', VALUE: '222.0.0.0', KNOWN_LEGANTO_CODE: '222_0_0'},
    UiT: {ORG_NAME: 'Universitetet i Tromsø - Norges arktiske universitet', VALUE: '186.0.0.0',
          KNOWN_LEGANTO_CODE: '186'},
    VID: {ORG_NAME: 'VID vitenskapelige høgskole', VALUE: '251.0.0.0', KNOWN_LEGANTO_CODE: '251'}
}


def get_string_value_dict(a_dict, key):
    """Get a string value from a dict with check key existence to avoid missing key problem."""
    if key in a_dict:
        return get_string_value_or_null(a_dict[key])
    return None


def get_string_value_sub_dict(a_dict, key_1, key_2):
    """Get a string value from a 2D dict with check key existence to avoid missing key problem."""
    if key_1 in a_dict and a_dict[key_1] is not None and key_2 in a_dict[key_1]:
        return get_string_value_or_null(a_dict[key_1][key_2])
    return None


def get_string_value_sub_sub_dict(a_dict, key_1, key_2, key_3):
    """Get a string value from a 3D dict with check key existence to avoid missing key problem."""
    if key_1 in a_dict and key_2 in a_dict[key_1] and key_3 in a_dict[key_1][key_2]:
        return get_string_value_or_null(a_dict[key_1][key_2][key_3])
    return None


def get_numeric_value_dict(a_dict, key_):
    """Get a numeric value from a dict with check key existence to avoid missing key problem."""
    if key_ in a_dict:
        return get_numeric_value_or_null(a_dict[key_])
    return None


def get_numeric_value_sub_dict(a_dict, key_1, key_2):
    """Get a numeric value from a 2D dict with check key existence to avoid missing key problem."""
    if key_1 in a_dict and key_2 in a_dict[key_1]:
        return get_numeric_value_or_null(a_dict[key_1][key_2])
    return None


def get_numeric_value_sub_sub_dict(a_dict, key_1, key_2, key_3):
    """Get a numeric value from a 3D dict with check key existence to avoid missing key problem."""
    if key_1 in a_dict and key_2 in a_dict[key_1] and key_3 in a_dict[key_1][key_2]:
        return get_numeric_value_or_null(a_dict[key_1][key_2][key_3])
    return None


def get_boolean_value_dict(a_dict, key_):
    """Get a boolean value from a dict with check key existence to avoid missing key problem."""
    if key_ in a_dict:
        value = get_numeric_value_or_null(a_dict[key_])
        if value is not None:
            return bool(value * 1)
    return None


def get_boolean_value_sub_dict(a_dict, key_1, key_2):
    """Get a boolean value from a 2D dict with check key existence to avoid missing key problem."""
    if key_1 in a_dict and key_2 in a_dict[key_1]:
        value = get_numeric_value_or_null(a_dict[key_1][key_2])
        if value is not None:
            return bool(value * 1)
    return None


def get_boolean_value_sub_sub_dict(a_dict, key_1, key_2, key_3):
    """Get a boolean value from a 3D dict with check key existence to avoid missing key problem."""
    if key_1 in a_dict and key_2 in a_dict[key_1] and key_3 in a_dict[key_1][key_2]:
        value = get_numeric_value_or_null(a_dict[key_1][key_2][key_3])
        if value is not None:
            return bool(value * 1)
    return None


def get_string_value_or_null(value):
    """Add a string value for an insert query. Some numbers are given as string."""
    if value is not None and value != '':
        return str(value)
    return None


def get_string_value_or_empty(dict_object, key_value):
    """Return value as string or empty string """
    if key_value in dict_object.keys():
        return str(dict_object[key_value])
    return ''


def get_numeric_value_or_null(value):
    """Add a numeric value for an insert query. Some numbers are given as string."""
    if value is not None:
        return value
    return 'null'


ORGANISATION = 'conspectus_organisation'
CONSPECTUS_USE_UNIT_CODE = 'conspectus_use_unit_code'
CONSPECTUS_USE_UNIT_NAME_NB = 'conspectus_use_unit_name_nb'
CONSPECTUS_USE_UNIT_NAME_EN = 'conspectus_use_unit_name_en'
UNIT_CODE = 'cristin_unit_code'
UNIT_NAME_NB = 'cristin_unit_name_nb'
UNIT_NAME_EN = 'cristin_unit_name_en'
SUB_UNIT_CODE = 'cristin_sub_unit_code'
SUB_UNIT_NAME_NB = 'cristin_sub_unit_name_nb'
SUB_UNIT_NAME_EN = 'cristin_sub_unit_name_en'
SUB_SUB_UNIT_CODE = 'cristin_sub_sub_unit_code'
SUB_SUB_UNIT_NAME_NB = 'cristin_sub_sub_unit_name_nb'
SUB_SUB_UNIT_NAME_EN = 'cristin_sub_sub_unit_name_en'
SUB_SUB_SUB_UNIT_CODE = 'cristin_sub_sub_sub_unit_code'
SUB_SUB_SUB_UNIT_NAME_NB = 'cristin_sub_sub_sub_unit_name_nb'
SUB_SUB_SUB_UNIT_NAME_EN = 'cristin_sub_sub_sub_unit_name_en'
PROCESSING_COMMENT = 'processing_comment'
LEGANTO_DEPT_CODE = 'leganto_dept_code'
LEGANTO_DEPT_NAME = 'leganto_dept_name'
COURSE_LEGANTO_CODE = 'course_leganto_code'
COURSE_YEAR = 'course_year'
CONSPECTUS_COURSE_YEAR = 'conspectus_course_year'
CONSPECTUS_COURSE_SEMESTER = 'conspectus_course_semester'
COURSE_CODE_NAME = 'course_code_name'
PROPOSED_CRISTIN_CODE = 'proposed_cristin_code'
CRISTIN_GROUP_CODE = 'cristin_group_code'
CRISTIN_DEPT_CODE = 'cristin_dept_code'
CRISTIN_SUB_UNIT_CODE = 'cristin_sub_unit_code'
CRISTIN_ORGANISATION_CODE = 'cristin_organisation_code'
CRISTIN_DEPT_NAME_EN = 'cristin_dept_name_en'
CRISTIN_DEPT_NAME_NB = 'cristin_dept_name_nb'
CRISTIN_DEPT_NAME_NN = 'cristin_dept_name_nn'
CRISTIN_FACULTY_NAME_EN = 'cristin_faculty_name_en'
CRISTIN_FACULTY_NAME_NB = 'cristin_faculty_name_nb'
CRISTIN_FACULTY_NAME_NN = 'cristin_faculty_name_nn'
CRISTIN_ORGANISATION_NAME_EN = 'cristin_organisation_name_en'
CRISTIN_ORGANISATION_NAME_NB = 'cristin_organisation_name_nb'
CRISTIN_ORGANISATION_NAME_NN = 'cristin_organisation_name_nn'
COMMENT = 'comment'

CONSPECTUS_COURSE_CODE = 'conspectus_course_code'
READING_LIST_ID = 'reading_list_id'

known_tables = ['course', 'course_reading_list', 'course_citation', 'counters',
                'course_searchable_id', 'course_term', 'organisations']

IDX_organisation = 0
IDX_COURSE_ID = 2
IDX_CITATIONS_HARVESTED = 18
PROCESS = 'process'
DROP = 'drop'
DROP_CREATE = 'drop-create'
CREATE = 'create'
COURSES = 'courses'
READING_LISTS = 'reading_lists'
CITATIONS = 'citations'
CITATION = 'citation'

PLANS = 'study_plans'
HTML = 'html'
RAW = 'raw'
PROCESSED = 'processed'
UNKNOWN = 'UNKNOWN'
ALL = 'all'
JSON = 'json'
ESBULK = 'esbulk'
CSV = 'csv'
DB = 'db'
CRISTIN_LEGANTO_MAPPING_FILE = 'resources/cristin-mappings/mapped/csv/{}_leganto_cristin_mapping.csv'

languages = ['en', 'nb']
URL = "https://api.nva.unit.no/cristin/organization/{}"
LABELS = 'labels'
NB = 'nb'
EN = 'en'
HAS_PART = 'hasPart'

LINK = 'link'
PROPOSED_CODE = 'proposed_code'
GROUP_CODE = 'sub_sub_sub_unit_code'
COURSE_ID = 'course_id'
UNIT_NAME = 'unit_name'
PARENT_UNITS = 'parent_units'
PARENT_UNIT = 'parent_unit'

ORG_NAME = 'org_name'
VALUE = 'value'
DESC = 'desc'

ACADEMIC_DEPARTMENT_NAME = 'academic_department_name'
ACADEMIC_DEPARTMENT_CODE = 'academic_department_code'
ACADEMIC_DEPARTMENT = 'academic_department'
COURSE_LEGANTO_CODE_EN = 'course_leganto_code'
DBH_INSTITUTION_CODE = 'Institusjonskode'
DBH_INSTITUTION_NAME = 'Institusjonsnavn'
DBH_DEPT_CODE = 'Avdelingskode'
DBH_DEPT_NAME = 'Avdelingsnavn'
DBH_DEPT_CODE_SSB = 'Avdelingskode_SSB'
DBH_YEAR = 'Årstall'
DBH_SEMESTER = 'Semester'
DBH_SEMESTER_NAME = 'Semesternavn'
DBH_COURSE_STUDY_PROGRAM_CODE = 'Studieprogramkode'
DBH_COURSE_STUDY_PROGRAM_NAME = 'Studieprogramnavn'
DBH_COURSE_CODE = 'Emnekode'
DBH_COURSE_NAME = 'Emnenavn'
DBH_LEVEL_CODE = 'Nivåkode'
DBH_LEVEL_NAME = 'Nivånavn'
DBH_COURSE_CREDIT = 'Studiepoeng'
DBH_NUS_CODE = 'NUS-kode'
DBH_STATUS = 'Status'
DBH_STATUS_NAME = 'Statusnavn'
DBH_LANGUAGE = 'Underv.språk'
DBH_NAME = 'Navn'
DBH_AREA_CODE = 'Fagkode'
DBH_AREA_NAME = 'Fagnavn'
DBH_TASK = 'Oppgave (ny fra h2012)'

DBH_INSTITUTION_CODE_EN = 'dbh_institution_code'
DBH_INSTITUTION_NAME_EN = 'dbh_institution_name'
DBH_DEPT_CODE_EN = 'dbh_dept_code'
DBH_DEPT_NAME_EN = 'dbh_dept_name'
DBH_DEPT_CODE_SSB_EN = 'dbh_dept_code_ssb'
DBH_YEAR_EN = 'dbh_year'
DBH_SEMESTER_EN = 'dbh_semester'
DBH_SEMESTER_NAME_EN = 'dbh_semester_name'
DBH_COURSE_STUDY_PROGRAM_CODE_EN = 'dbh_course_study_program_code'
DBH_COURSE_STUDY_PROGRAM_NAME_EN = 'dbh_course_study_program_name'
DBH_COURSE_CODE_EN = 'dbh_course_code'
DBH_COURSE_NAME_EN = 'dbh_course_name'
DBH_LEVEL_CODE_EN = 'dbh_level_code'
DBH_LEVEL_NAME_EN = 'dbh_level_name'
DBH_COURSE_CREDIT_EN = 'dbh_course_credit'
DBH_NUS_CODE_EN = 'dbh_nus_code'
DBH_STATUS_EN = 'dbh_status'
DBH_STATUS_NAME_EN = 'dbh_status_name'
DBH_LANGUAGE_EN = 'dbh_language'
DBH_NAME_EN = 'dbh_name'
DBH_AREA_CODE_EN = 'dbh_area_code'
DBH_AREA_NAME_EN = 'dbh_area_name'
DBH_TASK_EN = 'dbh_task'

EMPTY_DBH_OBJECT = {
    DBH_INSTITUTION_CODE: '', DBH_INSTITUTION_NAME: '', DBH_DEPT_CODE: '', DBH_DEPT_NAME: '',
    DBH_DEPT_CODE_SSB: '', DBH_YEAR: '', DBH_SEMESTER: '', DBH_SEMESTER_NAME: '', DBH_COURSE_STUDY_PROGRAM_CODE: '',
    DBH_COURSE_STUDY_PROGRAM_NAME: '', DBH_COURSE_CODE: '', DBH_COURSE_NAME: '', DBH_LEVEL_CODE: '',
    DBH_LEVEL_NAME: '', DBH_COURSE_CREDIT: '', DBH_NUS_CODE: '', DBH_STATUS: '', DBH_STATUS_NAME: '', DBH_LANGUAGE: '',
    DBH_NAME: '', DBH_AREA_CODE: '', DBH_AREA_NAME: '', DBH_TASK: ''
}

DBH_COLUMN_NAMES = [DBH_INSTITUTION_CODE, DBH_INSTITUTION_NAME, DBH_DEPT_CODE, DBH_DEPT_NAME, DBH_DEPT_CODE_SSB,
                    DBH_YEAR, DBH_SEMESTER, DBH_SEMESTER_NAME, DBH_COURSE_STUDY_PROGRAM_CODE,
                    DBH_COURSE_STUDY_PROGRAM_NAME, DBH_COURSE_CODE, DBH_COURSE_NAME, DBH_LEVEL_CODE, DBH_LEVEL_NAME,
                    DBH_COURSE_CREDIT, DBH_NUS_CODE, DBH_STATUS, DBH_STATUS_NAME, DBH_LANGUAGE, DBH_NAME, DBH_AREA_CODE,
                    DBH_AREA_NAME, DBH_TASK]

LEGANTO_COLUMN_NAMES = [LEGANTO_DEPT_CODE, LEGANTO_DEPT_NAME, PROPOSED_CODE, CONSPECTUS_USE_UNIT_CODE,
                        GROUP_CODE, SUB_SUB_UNIT_CODE, SUB_UNIT_CODE,
                        UNIT_CODE, SUB_SUB_UNIT_NAME_EN, SUB_SUB_UNIT_NAME_NB,
                        SUB_UNIT_NAME_EN, SUB_UNIT_NAME_NB,
                        UNIT_NAME_EN, UNIT_NAME_NB, COMMENT]

# Used to create local list of cristin identifiers
CRISTIN_COLUMN_NAMES = [CONSPECTUS_USE_UNIT_CODE, UNIT_CODE, UNIT_NAME_EN, UNIT_NAME_NB,
                        SUB_UNIT_CODE, SUB_UNIT_NAME_EN, SUB_UNIT_NAME_NB,
                        SUB_SUB_UNIT_CODE, SUB_SUB_UNIT_NAME_EN, SUB_SUB_UNIT_NAME_NB,
                        SUB_SUB_SUB_UNIT_CODE, SUB_SUB_SUB_UNIT_NAME_EN, SUB_SUB_SUB_UNIT_NAME_NB]

LEGANTO_CRISTIN_COLUMN_NAMES = [LEGANTO_DEPT_CODE, CONSPECTUS_USE_UNIT_CODE, LEGANTO_DEPT_NAME, UNIT_CODE, UNIT_NAME_EN,
                                UNIT_NAME_NB, SUB_UNIT_CODE, SUB_UNIT_NAME_EN, SUB_UNIT_NAME_NB,
                                SUB_SUB_UNIT_CODE, SUB_SUB_UNIT_NAME_EN, SUB_SUB_UNIT_NAME_NB]

# Not an ideal structure to map across arrays like shown below. It works but requires attention
# to detail if making a change. It makes coding easier elsewhere.
KNOWN_MANUAL_MAPPING = {
    OSLOMET: {
        LEGANTO_DEPT_CODE: [
            '215_0_0', '215_13_0', '215_13_10', '215_13_11', '215_13_4',
            '215_13_5', '215_13_8', '215_13_9', '215_14_0', '215_14_2',
            '215_14_3', '215_14_4', '215_14_5', '215_14_8', '215_15_0',
            '215_15_2', '215_15_3', '215_15_4', '215_15_5', '215_15_6',
            '215_15_8', '215_16_0', '215_16_2', '215_16_3', '215_16_4',
            '215_16_5', '215_16_6', '215_16_7', '215_18_1', '217_8_3'],
        LEGANTO_DEPT_NAME: [
            '', '', '', '', '',
            '', '', '', '', '',
            '', '', '', '', '',
            '', '', '', '', '',
            '', '', '', '', '',
            '', '', '', '', '',
        ],
        PROPOSED_CODE: [
            '215.0.0.0', '215.3.0.0', '215.3.11.0', '215.3.12.0', '215.3.12.0',
            '215.3.12.0', '215.3.2.0', '215.3.10.0', '215.4.0.0', '215.4.2.0',
            '215.4.3.0', '215.4.5.0', '215.4.4.0', '215.4.0.0', '215.5.0.0',
            '215.5.2.0', '215.5.3.0', '215.5.6.2', '215.5.5.0', '215.5.6.0',
            '215.5.6.3', '215.6.0.0', '215.6.2.0', '215.6.4.0', '215.6.5.0',
            '215.6.6.0', '215.6.3.0', '215.6.8.0', '215.9.0.0', '215.0.0.0']
    },
    NHH: {
        LEGANTO_DEPT_CODE: [
            "191_0_1", "191_02_5", "191_0_2", "191_1_1", "191_12_0",
            "191_1_2", "191_1_3", "191_14_0", "191_1_4", "191_21_0",
            "191_2_1", "191_31_0", "191_3_1", "null", "UNDEFINED"
        ],
        LEGANTO_DEPT_NAME: [
            'SEFU', '', 'NHH Executive', 'Department of Business and Management Science', '',
            'Department of Accounting, Auditing and Law', 'Department of Strategy and Management',
            '', 'Department of Finance', '',
            'Department of Economics', '',
            'Department of Professional and Intercultural Communication', '', ''
        ],
        PROPOSED_CODE: [
            "191.0.0.0", "191.0.0.0", "191.0.0.0", "191.10.0.0", "191.40.0.0",
            "191.40.0.0", "191.20.0.0", "191.0.0.0", "191.15.0.0", "191.30.0.0",
            "191.30.0.0", "191.50.0.0", "191.50.0.0", "191.0.0.0", "191.0.0.0"]
    },
    VID: {
        LEGANTO_DEPT_CODE: [
            "251_30_0", "251_30_10", "251_30_1", "251_30_20", "251_30_2",
            "251_31_0", "251_31_10", "251_31_1", "251_31_20", "251_31",
            "251_30", "251_30_3", "251_32_0", "251_32_10", "251_32_1",
            "251_32_20", "251_32_2", "251_32_30", "251_32_40", "251_32",
            "251_40_1", "251_40_2", "251_40", "251_60", "251_60_1",
            "null", "UNDEFINED"
        ],
        LEGANTO_DEPT_NAME: [
            '', '', '', '', '',
            '', '', '', '', 'Fakultet for sosialfag',
            'Fakultet for helsefag', '', '', '', '',
            '', '', '', '', 'Fakultet for teologi, diakoni og ledelsesfag',
            '', '', 'Forskning', '', '',
            '', ''
        ],
        PROPOSED_CODE: [
            "251.0.0.0", "251.3.1.0", "251.3.1.0", "251.3.2.0", "251.3.2.0",
            "251.2.0.0", "251.2.0.0", "251.2.0.0", "251.2.0.0", "251.2.0.0",
            "251.0.0.0", "251.0.0.0", "251.1.0.0", "251.1.0.0", "251.1.0.0",
            "251.1.0.0", "251.1.0.0", "251.1.0.0", "251.1.0.0", "251.1.0.0",
            "251.0.0.0", "251.0.0.0", "251.4.0.0", "251.0.0.0", "251.0.0.0",
            "251.0.0.0", "251.0.0.0"
        ]
    }
}
COURSE_CODE_NOT_PROCESSABLE = 'Course id [{}] has code [{}] not processable with regex [{}]'
COURSE_CODE_NOT_PROCESSABLE_REASON = 'Course id [{}] has code [{}] not processable because [{}]'
MULTIPLE_SEMESTER_FIRST_USED = 'Course [{}] has [{}] term/semesters. The first has been used'
ASSUMED_TEST_COURSE = 'course is assumed to be a test course'
ASSUMED_NOT_RELEVANT = 'course is assumed to not be relevant'
ASSUMED_DEFAULT_COURSE = 'course is assumed to be a default course'
COURSE_NAME_NOT_SAME = 'Course [{}] has different names for conspectus [{}] and DBH [{}]. It has been set to DBH value.'
PROCESSED_COURSE = 'Processed [{}] course with code [{}], year [{}], name [{}]'
NOT_PROCESSED_COURSE = 'Will NOT process [{}] course with code [{}], year [{}], name [{}]'
UNKNOWN_OUTPUT_FORMAT = 'Unknown outputformat [{}]. Cannot proceed'
SET_NAME = '\tSetting nva name [{}] for nva unit id [{}] for language [{}]'
PROCESSED_UNIT_MSG = 'Processing [{}], with id [{}]'
PROCESSED_SUB_UNIT_MSG = ' ..Processed subunit [{}], with id [{}]'
PROCESSED_SUB_SUB_UNIT_MSG = '  ...Processed subsubunit [{}], with id [{}]'
PROCESSED_SUB_SUB_SUB_UNIT_MSG = '    .....Processed subsubsubunit [{}], with id [{}]'
PROCESSED_READING_LIST = '\tProcessed [{}] reading list with id [{}], name [{}] '
NOT_PROCESSED_READING_LIST = '\tIgnored [{}] reading list with id [{}], name [{}] as course file does not exist'
PROCESSED_CITATIONS = '\tProcessed [{}] citations for course code [{}], name [{}] '
NOT_PROCESSED_CITATIONS = '\tCould not find course for citations'
PROCESSED_CITATIONS_CSV = '\tProcessed [{}] citations for [{}], course [{}], reading list[{}]'
NO_CITATIONS = '\tNo citations for [{}], course [{}], reading list[{}]'
NOT_PROCESSED_CITATIONS_CSV = '\tCould not process [{}] citations for [{}], course [{}], reading list[{}]'
NOTHING_TO_PROCESS = 'Nothing to process in {}'
PROCESSING = 'Processing [{}] for organisation [{}]'
PROCESSED_FILES = 'Processes [{}] {} for organisation [{}]'
DBH_NO_DATA = '[{}], [{}], [{}] has no DBH value'
FOUND_DBH_DATA = 'Found DBH data for [{}], [{}], [{}]'
NOT_FOUND_DBH_DATA = 'Could not find DBH data for [{}], [{}], [{}]'
CITATION_NO_CODE = 'Could not find \'code\' for citation with ID [{}]'
COURSE_NO_CODE = 'Could not find \'code\' for course with ID [{}]'
TRYING_DBH_DATA = 'Trying find DBH data for [{}], [{}], [{}]'
MISSING_TERM = 'No term in course with course code [{}]'
MULTIPLE_SEMESTER = 'More than 3 semesters in course with course code [{}]'
DOT_JSON = '.json'
RESET_COURSE_CODE = 'Resetting course code from [{}] to [{}]'
COURSE_FILE = 'course_{}.json'
COURSE_READING_LIST_FILE = 'course_{}_reading_list_{}.json'
COURSE_READING_LIST_CITATIONS_FILE = 'course_{}_reading_list_{}_citations.json'
CRISTIN_ORG_DATA_CSV = 'cristin_data.csv'
COURSE_MAPPING_FILE = '{}_course_mappings.json'
LEGANTO_CRISTIN_MAPPING_FILENAME = '{}_leganto_cristin_mapping.{}'
DBH_MAPPING_FILENAME = '{}_dbh_mapping.csv'
PROBLEM_ID_FROM_LINK = 'Stopping due to problem identifying [{}] id from [{}]'

unit_with_no_faculty = [NHH]

REGEX_UNIT_FILE_NAME = r"([a-zA-Z]+)_leganto_cristin_mapping\.csv"
REGEX_GROUP_GET_ID = "(\\d+.\\d+.\\d+.\\d+)$"
RESOURCES = 'resources'
CRISTIN_MAPPINGS = 'cristin-mappings'
COURSE_MAPPINGS = 'course-mappings'
DBH_MAPPINGS = 'dbh-mappings'
CSV_DIR = 'csv'
MAPPED = 'mapped'
INPUT = 'input'
OUTPUT = 'output'
SECTION = 'section'
CODE = 'code'
NAME = 'name'
YEAR = 'year'
TERM = 'term'
ID = 'id'
CONSPECTUS_COURSE_NAME = 'conspectus_course_name'
END_DATE = 'end_date'
START_DATE = 'start_date'
CONSPECTUS_YEAR = 'conspectus_year'
CONSPECTUS_SECTION = 'conspectus_section'
CONSPECTUS_TERM_CODE_1 = 'conspectus_term_code_1'
CONSPECTUS_TERM_NAME_1 = 'conspectus_term_name_1'
CONSPECTUS_TERM_CODE_2 = 'conspectus_term_code_2'
CONSPECTUS_TERM_NAME_2 = 'conspectus_term_name_2'
CONSPECTUS_TERM_CODE_3 = 'conspectus_term_code_3'
CONSPECTUS_TERM_NAME_3 = 'conspectus_term_name_3'
CONSPECTUS_SEMESTER_CODE_1 = 'conspectus_semester_code_1'
CONSPECTUS_SEMESTER_CODE_2 = 'conspectus_semester_code_2'
CONSPECTUS_SEMESTER_CODE_3 = 'conspectus_semester_code_3'

LEGANTO_AUTUMN = 'Høst'
LEGANTO_SPRING = 'Vår'

# DO_NOT_PROCESS is a list of course ids to not process. Examples are test courses.
DO_NOT_PROCESS = {
    AHO: ['569640910002237', '572613430002237', '596740890002237', '2007071880002237', '2141676340002237'],
    DMMH: ['3134551050002262', '3134551050002262', '2419490210002262', '2419506390002262', '2317187140002262',
           '2330626250002262', '2866296260002262', '2866296740002262'],
    FORSVARET: ['645366710002275', '2313417100002275', '2938411380002275'],
    HIMOLDE: ['1914954510002223', '2644256500002223', '2669331260002223', '2669331790002223', '1406428400002223',
              '5350391420002223'],
    HIOF: ['4911385340002218', '4962261970002218', '4966025370002218', '4971276030002218', '4974054800002218',
           '4980440190002218', '4980452990002218', '4980703460002218', '4980704420002218', '5166509700002218',
           '5631006710002218', '5653851520002218', '5657367300002218', '5658970680002218', '5658994630002218',
           '5661010030002218', '5662638080002218', '5663868810002218', '5664262920002218', '5666322910002218',
           '5666427670002218', '5667837420002218', '5668705740002218', '5669668240002218', '5683233990002218',
           '5695066580002218', '5696825760002218', '5708513240002218', '5711409730002218', '5715729140002218',
           '5750553990002218', '5753179640002218', '5837442710002218', '5907021280002218', '3133905640002218',
           '4192385400002218', '4548446860002218', '4633783780002218', '4786499970002218', '4786825380002218',
           '3115798370002218', '4185295890002218', '1071014730002218', '5176054500002218', '3133905640002218',
           '4250989080002218', '4250989070002218', '4242799970002218', '4707024590002218', '4707024610002218',
           '5005062600002218', '5005374410002218', '5435501480002218', '5435501650002218', '5708799570002218',
           '5708803700002218', '3030873730002218', '3133902920002218', '3133902920002218', '4250989070002218',
           '4250989080002218', '4707024590002218', '4192385400002218'],
    HIVOLDA: ['5072595430002220', '2719901040002220', '5103761500002220', '533677380002220', '2052735150002220',
              '1985462080002220', '3929764100002220', '4815388450002220', '4982986180002220', '4999962820002220',
              '5072634370002220', '5072595430002220', '2107947040002220', '2117426030002220'],
    HVL: [],
    INN: ['2772203570002214', '3266413950002214', '4922786910002214', '4924663170002214', '3175135950002214',
          '4368858390002214', '4868523060002214', '4868523060002214', '4868833260002214', '4868833260002214',
          '4868834770002214', '4868834770002214', '4921588230002214', '4921588230002214', '5066049570002214',
          '5751148120002214', '5837713420002214', '5941394260002214', '5941394260002214', '966318560002214',
          '3241909230002214', '5765791020002214', '5956061620002214', '5331820110002214', '3175135970002214',
          '3175135950002214', '5767408070002214', '5751148120002214'],
    KRISTIANIA: ['6891902170002296', '716567770002296', '2177979340002296', '6560554190002296', '9399737670002296',
                 '9153673170002296', '9153668220002296', '9183247890002296', '7047127180002296'],
    KRUS: ['2530975470002285', '1416087370002285', '2522131310002285'],
    LDH: ['1589412970002272', '1488171720002272', '599432330002272', '3050372460002272', '3050462180002272',
          '1595844220002272'],
    MF: ['2858397420002227', '2836428340002227', '3437735740002227', '2984487570002227', '2984487580002227',
         '3424988940002227', '1098993100002227'],
    NLA: ['2650619190002228', '2694946140002228', '2707258290002228', '619217650002228', '3261050150002228',
          '3499215470002228'],
    NHH: ['2577954960002216', '2368278250002216', '2636214030002216', '3709144210002216', '3709144210002216',
          '3761489290002216', '3761489290002216', '3761490800002216', '3787252190002216', '3787412680002216',
          '3795437780002216', '3795442620002216', '1342517860002216', '3230531980002216', '1745533850002216 ',
          '3836855060002216'],
    NMBU: ['3856896550002213', '3856896570002213', '2146369320002213', '495829010002213', '3856896570002213',
           '4175106050002213'],
    NORD: ['5938322210002211', '6755513580002211', '904226280002211', '5951715290002211', '5257212060002211',
           '5750679670002211', '4120833010002211'],
    NTNU: ['7343787900002203', '10851373670002203', '10146846720002203', '2625781850002203', '7343787900002203',
           '7408895480002203', '7448701500002203', '7539429220002203', '7539900440002203', '7645882470002203',
           '7962326650002203', '8487934890002203', '8496736830002203', '8576160000002203', '8599853940002203',
           '9726054390002203'],
    OSLOMET: ['1213510540002212', '4114108160002212', '4336984500002212', '10059092600002212', '5506767470002212',
              '5516145470002212', '5973515670002212', '9227364550002212', '9673031790002212', '6439774090002212',
              '6439839470002212', '9087024150002212'],
    PHS: ['2247170530002279', '2362105150002279', '559161130002279', '2247134540002279', '2673583630002279',
          '2362105150002279'],
    SAMAS: ['560179020002230', '2596666170002230', '2045379810002230', '2400043480002230', '2400043550002230'],
    UiA: ['2920101450002209', '6549976730002209', '2259187840002209', '2323120960002209', '2292516810002209',
          '2920101640002209', '2288252470002209', '2323888870002209', '2323890470002209', '3093727840002209',
          '3093727850002209', '3435908440002209', '2920101710002209', '2920101490002209', '2920101740002209',
          '2923564540002209', '2920101800002209', '2920101590002209', '2920101540002209', '3550216760002209',
          '2990157300002209', '3292841410002209', '2259187840002209', '1048176530002209', '5862608040002209'],
    UiB: ['1727199370002207', '4275929400002207', '4305647760002207', '4523730090002207', '4524860220002207',
          '4692454190002207', '5990311550002207', '7515537880002207', '7906654590002207', '7910688820002207',
          '7910688840002207', '7910688860002207', '7912168710002207', '7912169070002207', '7912169360002207',
          '7906654590002207', '4325875430002207', '4325886280002207', '4325844090002207', '4325845770002207',
          '4325851560002207', '4325808070002207', '4590407240002207', '4325866490002207', '7515537880002207',
          '7497003170002207', '4802211760002207', '5579407430002207', '5940645080002207', '6727527100002207',
          '8020791690002207', '8590701400002207', '9257703000002207', '9850079790002207', '11666085960002207'],
    UiO: ['8640450020002204', '17375638890002204', '15574422440002204', '17206458780002204', '18949902200002204',
          '8640450020002204', '2645571460002204', '9499619500002204', '12930679480002204', '14998042740002204',
          '8911312990002204', '17663637450002204', '13832705550002204', '13832577460002204', '12789831710002204',
          '12940609560002204', '12940609580002204', '12940609600002204', '12940609620002204', '12940609640002204',
          '12940609690002204', '12940609740002204', '12940609780002204', '12940609820002204', '12940609870002204',
          '12940609920002204', '12940609970002204', '12940610020002204', '12940610070002204', '12940610120002204',
          '12940610150002204', '12940610200002204', '12940610250002204', '12940610300002204', '12940610350002204',
          '12940610450002204', '13579854060002204', '13579854060002204', '17375660130002204'],
    UiS: ['4587932820002208', '1021674680002208', '5671467460002208', '5280017280002208', '4772717180002208',
          '3857039370002208', '4850881570002208', '4769068210002208'],
    UiT: ['1706515580002205', '4634223760002205', '4225319930002205', '4952964990002205', '4408193010002205',
          '9387752370002205'],
    USN: ['3536514520002210', '3568451310002210', '3578584990002210', '3786972380002210', '3786972400002210',
          '3786972430002210', '3786972450002210', '3786972470002210', '3787070360002210', '3787070380002210',
          '4589693920002210', '5703338120002210', '7224516350002210', '8337410570002210', '999908260002210'],
    VID: ['5718725780002247', '3116120120002247', '982909840002247', '2662647680002247', '5718725780002247',
          '5498062920002247']
}

# Match the following types of course names
# Specialization Project - ING501 HØST 2023
# The European Union - Institutions and Politics - ST-202 VÅR 2024
REGEX_NAME_DASH_COURSE_CONSPECTUS_TERM_YEAR = r'(.*)-\s*({}).*\d+$'
# Match the following types of course names
# PED534 - Barnehagens læringsmiljø(2021 - H)
# HI-132 - "Games of History": Games and gaming as historical sources (2020-V)
# ORG453 - Communication, Cooperation and Research Methods (HØST 2022)
REGEX_COURSE_DASH_NAME_PAR_ANY_PAR = r'^({})\s*-\s*(.+)(\(.*\))$'
# Match the following types of course names
# Discourse Analysis EN-467
REGEX_COURSE_SPACE_CODE = r'(.*){}$'
REGEX_CODE_YEAR_SEMESTER_NAME = r'(.*)\s+(\d\d)(V|H)\s+(.*)?'
# Two groups to hold ID for courses and reading-lists
# https://example.com/courses/4715029150002209/reading-lists/4719094390002209"
REGEX_ID_FROM_LINK = r'^.*/courses/(\d+)/reading-lists/(\d+)$'
# UE_194_YR6013_1_2019_HØST_1
# UE_186_ALI-1110_1_2020_HØST_1
REGEX_PRE_CRISTIN_CODE_SECTION_YEAR_SEMESTER = r'(UA|UE)_\d+_(.*)_([a-zA-Z]|\d)_(\d\d\d\d)_(VÅR|HØST)_(\d+)_?\d*-?\d*'
# 2021H-EXPHIL-JUSEM-0
REGEX_YEAR_SEMESTER_CODE_SECTION = r'(\d\d\d\d)(H|V)-(.+)-(\d)$'
REGEX_PRE_CRISTIN_CODE_SECTION_SEMESTER_YEAR = r'(UA|UE)_\d+_(.*)_([a-zA-Z]|\d)_(V|H)(\d\d)'
# 194_MGLU2504_1_HØST_2020_1
REGEX_CRISTIN_CODE_SECTION_YEAR_SEMESTER = r'\d+_(.*)_([a-zA-Z]|\d)_(VÅR|HØST)_(\d\d\d\d)_\d+_?\d*-?\d*'
# UE_FI5206_1_2023_HØST_1"
REGEX_PRE_CODE_SECTION_YEAR_SEMESTER = \
    r'(UA|UE)_([\\u0000-\\u007F]+|\w+)_([a-zA-Z]|\d)_(\d\d\d\d)_(VÅR|HØST)_\d+_?\d*-?\d*'
REGEX_CODE_SECTION_YEAR_SEMESTER = r'([\\u0000-\\u007F]+|\w+)_([a-zA-Z]|\d)_(\d\d\d\d)_(VÅR|HØST)_\d+_.+'
# MERGE_SYT3002_SYG3002_SYA3002_H21
REGEX_MERGE_COURSES_SEMESTER_YEAR = r'MERGE_([a-zA-Z\d]+)_([a-zA-Z\d]+)_([a-zA-Z\d]+)*_?([a-zA-Z\d])*_?(H|V)(\d\d)'
# KK_ENGAGEPED_2024-VÅR, KL_50SEN_2021_HØST_B KL_BABLUD_2022_HØST_NES
REGEX_KLKU_PRE_CODE_YEAR_SEMESTER = r'^(KL|KK|KU)_(.*)_(\d\d\d\d)(_|-)(VÅR|HØST)_?\w*(-\w)?'
# EVU_11-RESA2702V_2023H-2024V, "EVU_11-TEOL4538V_2024V-2024H", "EVU_11-TEOL4541V_2023H", "EVU_11-TEOL4551_2023H"
# Note -2024V is ignored in EVU_11-RESA2702V_2023H-2024V
REGEX_UIO_PRE_CODE_SEMESTER_YEAR = r'EVU_\d\d-(.*)_(\d\d\d\d)(V|H)'
# "40 140(2020)", 70 600 (2020), 54321
# "40 521/40 522"
REGEX_AHO_CODE = r'^(\d\d\s?\d\d\d)'
# "NECEC2201_1_2023_VÅR_1", "NECEC2300_1_2023_HØST_1",
# "NECEC2201_1_2024_VÅR_1",  "NECEC2300_1_2022_HØST"
REGEX_DMMH_CODE_A = r'^([\\u0000-\\u007F]+|\w)(_\d)?_(\d\d\d\d)_(HØST|VÅR|Høst|Vår|høst|vår)_?\d?'
# "NECEC2201 2020 HØST" NECEC2201_2020_HØST "NECEC2201_2022_VÅR", "NECEC2300_2021_HØST"
REGEX_DMMH_CODE_B = r'^([\\u0000-\\u007F]+|\w)(\s+|_)(\d\d\d\d)(\s+|_)(HØST|VÅR|Høst|Vår|høst|vår)'
# "NECEC2201"
REGEX_DMMH_CODE_C = r'^([\\u0000-\\u007F]+|\w)'
# ING 2318
# LED 3202 (Kull 23-26) (2024V-26V)
# MAVE4210
# MAVE4220  2024 VÅR
REGEX_FORSVARET_CODE = r'^(\w\w\w(\s?\w?)\s?\d\d\d\d)'
REGEX_HIMOLDE_CODE = r'^(\p{L}+\d+-?\d?\s?\p{L}*)'
REGEX_HIMOLDE_CODE_SINGLE_WORD = r'^(\p{L}+)'
REGEX_CODE_SEMESTER_YEAR = r'([\\u0000-\\u007F]+|\w+)_(V|H)(\d\d)'
REGEX_SIS = r'(\d\d\d\d)(\s+-)sis-kommer'
