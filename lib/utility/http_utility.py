import http
import json
import logging
import urllib
from urllib.request import urlopen


def do_request(url):
    try:
        connection = urlopen(url)
        return json.loads(connection.read().decode("utf-8"))
    except (http.client.IncompleteRead, urllib.error.HTTPError) as e:
        message = "No data available for " + url + " " + str(e)
        logging.error(message)
        raise ValueError(message)
