"""Basic file handling support for json/csv"""
import errno
import os
from os.path import join

from lib.transformer.out.OutWriter import OutWriter
from lib.utility.constants import COURSES, READING_LISTS, CITATIONS, COURSE_READING_LIST_CITATIONS_FILE


def create_directory(directory_name):
    try:
        if not os.path.exists(directory_name):
            os.makedirs(directory_name)
    except OSError:
        raise


class File(OutWriter):

    def __init__(self, organisation, working_dir, stage):
        self.working_dir = join(working_dir, organisation, stage)
        self.organisation = organisation
        self.course_dir = COURSES
        self.reading_list_dir = READING_LISTS
        self.citation_dir = CITATIONS
        self.check_directory_exists_or_make()

    def write_course(self, course):
        pass

    def write_reading_list(self, reading_list):
        pass

    def write_citation(self, citation):
        pass

    def write_citations(self, citations):
        pass

    def citations_json_file_exists(self, course_id, reading_list_id):
        return os.path.exists(self.get_json_file_path(course_id, reading_list_id))

    def get_json_file_path(self, course_id, reading_list_id):
        return join(self.working_dir, self.citation_dir,
                    COURSE_READING_LIST_CITATIONS_FILE.format(course_id, reading_list_id))

    def get_list_citation_files(self):
        citations_dir = os.path.join(self.working_dir, self.citation_dir)
        if not os.path.isdir(citations_dir):
            print(NOTHING_TO_PROCESS.format(citations_dir))
            return
        return citations_dir, os.listdir(citations_dir)

    def check_directory_exists_or_make(self):
        try:
            create_directory(self.working_dir)
            create_directory(join(self.working_dir, self.course_dir))
            create_directory(join(self.working_dir, self.reading_list_dir))
            create_directory(join(self.working_dir, self.citation_dir))
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

    def get_course_directory(self):
        return join(self.working_dir, self.course_dir)

    def get_reading_list_directory(self):
        return join(self.working_dir, self.reading_list_dir)

    def get_organisation(self):
        return self.organisation
