import json
from os.path import join

from lib.file.file import File
from lib.utility.constants import READING_LIST_ID, COURSE_READING_LIST_FILE, ID, COURSE_FILE


class JsonFile(File):

    def __init__(self, organisation, working_dir, stage):
        super().__init__(organisation, working_dir, stage)

    def write_course(self, course):
        """write contents of a course to a json file"""
        json_path = join(self.working_dir, self.course_dir, COURSE_FILE.format(course['id']))
        with open(json_path, 'w') as course_file:
            json.dump(course, course_file, indent=4)
            course_file.flush()
            course_file.close()

    def write_reading_list(self, reading_list):
        """write contents of a reading list to a json file"""
        json_path = join(self.working_dir, self.reading_list_dir,
                         COURSE_READING_LIST_FILE.format(reading_list[ID], reading_list[ID]))
        with open(json_path, 'w') as reading_list_file:
            json.dump(reading_list, reading_list_file, indent=4)
            reading_list_file.flush()
            reading_list_file.close()

    def write_citation(self, citation):
        pass

    def write_citations(self, citations):
        """write contents of a reading list to a json file"""
        json_path = self.get_json_file_path(citations[ID], citations[READING_LIST_ID])
        with open(json_path, 'w') as citation_file:
            json.dump(citations, citation_file, indent=4)
            citation_file.flush()
            citation_file.close()
