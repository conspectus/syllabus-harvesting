from abc import ABC, abstractmethod


class SyllabusDatabase(ABC):

    def __init__(self):
        pass

    @abstractmethod
    def truncate_organisation(self, organisation):
        pass

    @abstractmethod
    def course_exists(self, course, organisation):
        """Check if a course exists."""
        pass

    @abstractmethod
    def course_searchable_id_exists(self, course_id, searchable_id, organisation):
        """Check if a searchable id has previously been associated with a course."""
        pass

    @abstractmethod
    def course_reading_list_exists(self, reading_list_id, organisation):
        """Check if a reading list has previously been persisted to the database."""
        pass

    @abstractmethod
    def course_citation_exists(self, citation_id, organisation):
        """Check if a citation has previously been persisted to the database."""
        pass

    @abstractmethod
    def get_courses(self, offset, limit, organisation):
        """Get all courses for a given organisation, with a limit."""
        pass

    @abstractmethod
    def get_reading_lists(self, course_id, organisation):
        """Get all reading_lists for a given organisation and course."""

    pass

    @abstractmethod
    def add_course(self, course, organisation):
        """Add a course"""
        pass

    # If the course didn't have a reading list return false, otherwise return true
    @abstractmethod
    def add_course_reading_list(self, course_id, reading_list, organisation):
        """Add single course reading_list associated with a course."""
        pass

    @abstractmethod
    def add_course_citation(self, course_id, course_as_tuple, reading_list_id, citation,
                            organisation):
        """Add single citations associated with a course."""
        pass

    @abstractmethod
    def add_searchable_id(self, course, organisation):
        """Add a searchable id (e.g. ARK2200) associated with a course."""
        pass
