import os
import sqlite3
import sys

from lib.utility.constants import known_tables
from lib.db.syllabus_db import SyllabusDatabase

from lib.utility.leganto_constants import get_course_column_names, get_course_value_placeholder, \
    get_course_values, get_reading_list_value_placeholder, \
    get_course_reading_list_column_names, get_course_reading_list_values, \
    get_course_citation_column_names, get_citation_value_placeholder, get_course_citation_values_as_tuple


class SQLiteDb(SyllabusDatabase):
    """
       Hold connection to sqlite3 database and exposes relevant functionality to the caller.
    """

    def __init__(self, db_filename):
        super().__init__()
        self.db = sqlite3.connect(db_filename)
        # Setting the following property ensures that a row of data is returned as a dict
        self.db.row_factory = sqlite3.Row

    def __check_structure_exists(self):
        # Note if you are getting a problem with sqlite_master table not being found, check what the
        # table is called. It seems to have various names
        select_statement = "SELECT name FROM sqlite_master WHERE type ='table' AND name NOT LIKE " \
                           "'sqlite_%';"

        cursor = self.db.cursor()
        results = cursor.execute(select_statement)
        results = results.fetchall()

        tables_problem = False

        # Assuming the database is empty at startup
        if len(results) == 0:
            return False
        else:
            for result in results:
                if result['name'] not in known_tables:
                    print("Table [{}] is not known".format(result['name']))
                    tables_problem = True
            if tables_problem:
                print("Error with sqlite tables. Exiting!")
                sys.exit(2)

        return True

    def build_db_structure(self):
        """Build database structure with details in syllabus_db_sqlite.ddl"""
        print("Creating all tables")
        with open(os.path.join('db', 'syllabus_db_sqlite.ddl'), 'r') as sql_file:
            ddl = sql_file.read()
        cursor = self.db.cursor()
        cursor.executescript(ddl)
        self.db.commit()

    def truncate_organisation(self, organisation):
        print("Truncating organisation [{}]".format(organisation))
        delete_dml = [
            "delete from course_searchable_id where organisation = '{}';".format(organisation),
            "delete from course_citation where organisation = '{}';".format(organisation),
            "delete from course_reading_list where organisation = '{}';".format(organisation),
            "delete from course where organisation = '{}';".format(organisation),
            "delete from organisations where organisation = '{}';".format(organisation)]
        cursor = self.db.cursor()
        for delete_command in delete_dml:
            cursor.execute(delete_command)
        self.db.commit()

    def drop_tables(self):
        print("Dropping all tables")
        delete_ddl = [
            "drop table if exists course_searchable_id;",
            "drop table if exists course_citation;",
            "drop table if exists course_reading_list;",
            "drop table if exists course;",
            "drop table if exists organisations;"]
        cursor = self.db.cursor()
        for drop_command in delete_ddl:
            cursor.execute(drop_command)
        self.db.commit()

    def course_exists(self, course, organisation):
        """Check if a course exists."""
        return self.query_for_one("SELECT * FROM course WHERE course_id='" + course['id'] + "'")

    def course_searchable_id_exists(self, course_id, searchable_id, organisation):
        """Check if a searchable id has previously been associated with a course."""
        pass

    def course_reading_list_exists(self, reading_list_id, organisation):
        """Check if a reading list has previously been persisted to the database."""
        if self.query_for_one("SELECT * FROM course_reading_list WHERE organisation='" +
                              organisation + "' and reading_list_id='" +
                              reading_list_id + "'") is None:
            return False
        return True

    def course_citation_exists(self, citation_id, organisation):
        """Check if a given citation has previously been persisted to the database."""
        if self.query_for_one("SELECT * FROM course_citation WHERE organisation='" +
                              organisation + "' and citation_id='" +
                              citation_id + "'") is None:
            return False
        return True

    def get_courses(self, offset, limit, organisation):
        """Get all courses for a given organisation, with a limit."""
        return self.query_for_many("SELECT * FROM course WHERE organisation='" +
                                   organisation + "' LIMIT {}, {}".format(offset, limit))

    def get_reading_lists(self, course_id, organisation):
        """Get all reading_lists for a given organisation and course."""

    pass

    def add_course(self, course, organisation):
        """Add a course"""
        if self.course_exists(course, organisation) is None:
            self.insert("INSERT INTO course (" + get_course_column_names() + ") VALUES (" +
                        get_course_value_placeholder() + ")",
                        get_course_values(organisation, course))

    # If the course didn't have a reading list return false, otherwise return true

    def add_course_reading_list(self, course_id, reading_list, organisation):
        """Add single course reading_list associated with a course."""
        if not self.course_reading_list_exists(reading_list['id'], organisation):
            self.insert("INSERT INTO course_reading_list (" +
                        get_course_reading_list_column_names() +
                        ") VALUES (" + get_reading_list_value_placeholder() + ")",
                        get_course_reading_list_values(organisation, course_id,
                                                       reading_list))
            return True
        return False

    def add_course_citation(self, course, reading_list_id, citation, organisation):
        """Add single citation associated with a course."""
        if not self.course_citation_exists(citation['id'], organisation):
            self.insert("INSERT INTO course_citation (" +
                        get_course_citation_column_names() +
                        ") VALUES (" + get_citation_value_placeholder() + ")",
                        get_course_citation_values_as_tuple(organisation, course,
                                                   reading_list_id, citation))

    def add_searchable_id(self, course, organisation):
        """Add a searchable id (e.g. ARK2200) associated with a course."""
        pass

    def query_for_one(self, select_statement):
        cursor = self.db.cursor()
        cursor.execute(select_statement)
        result = cursor.fetchone()
        return result

    def query_for_many(self, sql):
        """Run a query likely to return multiple tuples."""
        cursor = self.db.cursor()
        cursor.execute(sql)
        return cursor.fetchall()

    def insert(self, sql, values):
        """Run a DML to insert/update a tuple."""
        cursor = self.db.cursor()
        cursor.execute(sql, values)
        self.db.commit()
