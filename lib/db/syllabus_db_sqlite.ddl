create table organisations
(
    organisation   TEXT,
    PRIMARY KEY (organisation)
);

create table course
(
    organisation             TEXT,
    course_id                TEXT,
    course_code              TEXT,
    course_name              TEXT,
    section                  TEXT,
    academic_department_code TEXT,
    academic_department_name TEXT,
    term_code_1              TEXT,
    term_name_1              TEXT,
    term_code_2              TEXT,
    term_name_2              TEXT,
    term_code_3              TEXT,
    term_name_3              TEXT,
    start_date               TEXT,
    end_date                 TEXT,
    weekly_hours             INTEGER,
    participants             INTEGER,
    year                     TEXT,
    created_date             TEXT,
    last_modified_date       TEXT,
    rolled_from              TEXT,
    citations_harvested      INTEGER DEFAULT 0,
    course_leganto_code      TEXT,
    unit_code                 TEXT,
    sub_unit_code             TEXT,
    sub_sub_unit_code         TEXT,
    sub_sub_sub_unit_code         TEXT,
    status                   TEXT,
    FOREIGN KEY (organisation) REFERENCES organisations (organisation),
    PRIMARY KEY (organisation, course_id)
);

create table course_reading_list
(
    organisation            TEXT,
    reading_list_id         TEXT,
    code                    TEXT,
    name                    TEXT,
    reading_list_order      TEXT,
    description             TEXT,
    last_modified_date      TEXT,
    status_value            TEXT,
    status_desc             TEXT,
    visibility_value        TEXT,
    visibility_desc         TEXT,
    publishing_status_value TEXT,
    publishing_status_desc  TEXT,
    term_code_1             TEXT,
    term_name_1             TEXT,
    term_code_2             TEXT,
    term_name_2             TEXT,
    term_code_3             TEXT,
    term_name_3             TEXT,
    year                    TEXT,
    fk_course_id            TEXT,
    blocked                 INTEGER DEFAULT 0,
    FOREIGN KEY (organisation) REFERENCES organisations (organisation),
    FOREIGN KEY (fk_course_id) REFERENCES course (course_id),
    PRIMARY KEY (organisation, reading_list_id)
);

create table course_citation
(
    organisation                    TEXT,
    citation_id                     TEXT,
    status_value                    TEXT,
    status_desc                     TEXT,
    copyrights_status_value         TEXT,
    copyrights_status_desc          TEXT,
    secondary_type_value            TEXT,
    secondary_type_desc             TEXT,
    type_value                      TEXT,
    type_desc                       TEXT,
    metadata_title                  TEXT,
    metadata_author                 TEXT,
    metadata_publisher              TEXT,
    metadata_publication_date       TEXT,
    metadata_edition                TEXT,
    metadata_isbn                   TEXT,
    metadata_issn                   TEXT,
    metadata_mms_id                 TEXT,
    metadata_additional_person_name TEXT,
    metadata_place_of_publication   TEXT,
    metadata_call_number            TEXT,
    metadata_note                   TEXT,
    metadata_issue                  TEXT,
    metadata_journal_title          TEXT,
    metadata_article_title          TEXT,
    metadata_editor                 TEXT,
    metadata_chapter                TEXT,
    metadata_chapter_title          TEXT,
    metadata_chapter_author         TEXT,
    metadata_year                   TEXT,
    metadata_pages                  TEXT,
    metadata_source                 TEXT,
    metadata_series_title_number    TEXT,
    metadata_pmid                   TEXT,
    metadata_doi                    TEXT,
    metadata_volume                 TEXT,
    metadata_start_page             TEXT,
    metadata_end_page               TEXT,
    metadata_start_page2            TEXT,
    metadata_end_page2              TEXT,
    metadata_author_initials        TEXT,
    metadata_part                   TEXT,
    public_note                     TEXT,
    source1                         TEXT,
    source2                         TEXT,
    source3                         TEXT,
    source4                         TEXT,
    source5                         TEXT,
    source6                         TEXT,
    source7                         TEXT,
    source8                         TEXT,
    source9                         TEXT,
    source10                        TEXT,
    last_modified_date              TEXT,
    section_info_id                 TEXT,
    section_info_name               TEXT,
    section_info_description        TEXT,
    section_info_visibility         INTEGER,
    section_info_tags_total_rec_cnt INTEGER,
    section_info_section_tags_link  TEXT,
    section_info_section_locked     INTEGER,
    file_link                       TEXT,
    fk_reading_list_id              TEXT,
    fk_course_id                    TEXT,
    unit_code                        TEXT,
    sub_unit_code                    TEXT,
    sub_sub_unit_code                TEXT,
    sub_sub_sub_unit_code                TEXT,
    semester_code_1                 TEXT,
    semester_code_2                 TEXT,
    semester_code_3                 TEXT,
    term_code_1                     TEXT,
    term_name_1                     TEXT,
    term_code_2                     TEXT,
    term_name_2                     TEXT,
    term_code_3                     TEXT,
    term_name_3                     TEXT,
    year                            TEXT,
    FOREIGN KEY (organisation) REFERENCES organisations (organisation),
    FOREIGN KEY (fk_course_id) REFERENCES course (course_id),
    FOREIGN KEY (fk_reading_list_id) REFERENCES course_reading_list (reading_list_id),
    PRIMARY KEY (organisation, citation_id)
);

create table course_searchable_id
(
    organisation  TEXT,
    fk_course_id  TEXT,
    searchable_id TEXT,
    FOREIGN KEY (organisation) REFERENCES organisations (organisation),
    FOREIGN KEY (fk_course_id) REFERENCES course (course_id),
    PRIMARY KEY (organisation, fk_course_id, searchable_id)
);

CREATE INDEX idx_course_reading_list_id ON course_reading_list (reading_list_id);
CREATE INDEX idx_course_citation_id ON course_citation (citation_id);
CREATE INDEX idx_course_citation_organisation ON course_citation (citation_id);
