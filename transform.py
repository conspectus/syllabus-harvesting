import getopt
import logging
import os
import sys
from datetime import datetime

from lib.db.org_info_db import OrganisationDatabase
from lib.transformer.institution.AHOTransformer import AHOTransformer
from lib.transformer.institution.DMMHTransformer import DMMHTransformer
from lib.transformer.institution.ForsvaretTransformer import ForsvaretTransformer
from lib.transformer.institution.HiMoldeTransformer import HiMoldeTransformer
from lib.transformer.institution.HiOfTransformer import HIOFTransformer
from lib.transformer.institution.HiVoldaTransformer import HIVoldaTransformer
from lib.transformer.institution.INNTransformer import INNTransformer
from lib.transformer.institution.KristianiaTransformer import KristianiaTransformer
from lib.transformer.institution.LDHTransformer import LDHTransformer
from lib.transformer.institution.MFTransformer import MFTransformer
from lib.transformer.institution.NHHTransformer import NHHTransformer
from lib.transformer.institution.NLATransformer import NLATransformer
from lib.transformer.institution.NMBUTransformer import NMBUTransformer
from lib.transformer.institution.NTNUTransformer import NTNUTransformer
from lib.transformer.institution.NordTransformer import NordTransformer
from lib.transformer.institution.USNTransformer import USNTransformer
from lib.transformer.institution.UiATransformer import UiATransformer
from lib.transformer.institution.UiBTransformer import UiBTransformer
from lib.transformer.institution.UiOTransformer import UiOTransformer
from lib.transformer.institution.VIDTransformer import VIDTransformer
from lib.transformer.transformer import Transformer

sys.path.insert(0, 'lib/transformer/course/*')
from lib.utility.constants import CITATIONS, READING_LISTS, COURSES, ALL, JSON, USN, UiB, NTNU, UiA, \
    NORD, UiO, AHO, DMMH, FORSVARET, HIMOLDE, HIOF, HIVOLDA, KRISTIANIA, INN, MF, NHH, NLA, NMBU, LDH, VID  # noqa: E402

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)


def get_options_retrieve(argv):
    try:
        # Set some default values that will be used unless they are overridden by command line
        entity = COURSES
        # Assume all organisations to be processed
        organisation = ALL
        # Using year/month as directory as download processing of everything can go over a number
        # of days. Expected use of the script i bi-yearly.
        working_dir = str(datetime.today().year) + "-" + str(datetime.today().month)
        # Assume syllabus.sqlite as default filename
        db_file = 'syllabus.sqlite'
        # Assume we will want to transform to cleaned JSON
        file_format = JSON
        opts, args = getopt.getopt(argv, 'he:o:f:d:w:')
        for opt, arg in opts:
            if opt == '-h':
                print(
                    'transform.py -e <entity> -o <organisation> -f <format> -d <database-file> -w '
                    '<working-dir>')
                sys.exit()
            if opt in '-e':
                entity = arg
            if opt in '-o':
                organisation = arg
            if opt in '-f':
                file_format = arg
            if opt in '-d':
                db_file = arg
            if opt in '-w':
                working_dir = arg

        return entity, organisation, file_format, db_file, working_dir

    except getopt.GetoptError:
        print('transform.py -e <entity> -o <organisation> -f <format> -d <database-file> -w '
              '<working-dir>')
        sys.exit(2)


# oslomet/uit can be processed without a subclass
def get_transformer(organisation, output_format, entity, working_dir):
    if organisation == AHO:
        return AHOTransformer(output_format, entity, working_dir)
    elif organisation == DMMH:
        return DMMHTransformer(output_format, entity, working_dir)
    elif organisation == HIMOLDE:
        return HiMoldeTransformer(output_format, entity, working_dir)
    elif organisation == HIOF:
        return HIOFTransformer(output_format, entity, working_dir)
    elif organisation == HIVOLDA:
        return HIVoldaTransformer(output_format, entity, working_dir)
    elif organisation == INN:
        return INNTransformer(output_format, entity, working_dir)
    elif organisation == FORSVARET:
        return ForsvaretTransformer(output_format, entity, working_dir)
    elif organisation == KRISTIANIA:
        return KristianiaTransformer(output_format, entity, working_dir)
    elif organisation == LDH:
        return LDHTransformer(output_format, entity, working_dir)
    elif organisation == MF:
        return MFTransformer(output_format, entity, working_dir)
    elif organisation == NHH:
        return NHHTransformer(output_format, entity, working_dir)
    elif organisation == NLA:
        return NLATransformer(output_format, entity, working_dir)
    elif organisation == NMBU:
        return NMBUTransformer(output_format, entity, working_dir)
    elif organisation == NORD:
        return NordTransformer(output_format, entity, working_dir)
    elif organisation == NTNU:
        return NTNUTransformer(output_format, entity, working_dir)
    elif organisation == UiA:
        return UiATransformer(output_format, entity, working_dir)
    elif organisation == UiB:
        return UiBTransformer(output_format, entity, working_dir)
    elif organisation == UiO:
        return UiOTransformer(output_format, entity, working_dir)
    elif organisation == USN:
        return USNTransformer(output_format, entity, working_dir)
    elif organisation == VID:
        return VIDTransformer(output_format, entity, working_dir)
    else:
        return Transformer(organisation, output_format, entity, working_dir)


def main(argv):
    entity_to_process, organisation_to_process, file_format, db_file, working_dir = \
        get_options_retrieve(argv)

    org_db = OrganisationDatabase(os.path.join(working_dir, 'organisation.sqlite'))

    # We assume any directory in the working directory corresponds to a third level organisation
    if organisation_to_process == ALL:
        organisations = os.listdir(working_dir)
    else:
        organisations = [organisation_to_process]

    for organisation in organisations:
        transformer = get_transformer(organisation, file_format, entity_to_process, working_dir)
        if entity_to_process == COURSES or entity_to_process is None:
            logging.info("Processing courses for organisation [{}]".format(organisation))
            transformer.process_courses()
        elif entity_to_process == READING_LISTS:
            logging.info("Processing reading lists for organisation [{}]".format(organisation))
            transformer.process_reading_lists()
        elif entity_to_process == CITATIONS:
            logging.info("Processing citations for organisation [{}]".format(organisation))
            transformer.process_all_citations()
        else:
            logging.info("Unknown option [{}]. Unable to continue.".format(entity_to_process))

    sys.exit()


if __name__ == '__main__':
    main(sys.argv[1:])
