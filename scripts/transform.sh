#!/bin/bash

organisation_list=(aho dmmh forsvaret himolde hivolda hiof hvl inn kristiania krus ldh mf nla nhh nmbu nord ntnu oslomet phs samas uia uib uio uis uit usn vid)
what_list=(courses reading_lists citations)

for organisation in "${organisation_list[@]}"; do
  for what in "${what_list[@]}"; do
    python3 transform.py $what organisation=$organisation format=csv
  done
done