#! /bin/bash
# Requires python version 3 installed
if python --version | grep -q '^Python 3\.'; then
  echo "Transforming courses from raw JSON to cleaned JSON"
  python transform.py -f json -w /data/conspectus/data -e course -w
  echo "Transforming courses from raw JSON to CSV"
  python transform.py -f csv -w /data/conspectus/data -e course -w
  echo "Transforming reading lists from raw JSON to cleaned JSON"
  python transform.py -f json -w /data/conspectus/data -e reading-list -w
  echo "Transforming reading lists from raw JSON to CSV"
  python transform.py -f csv -w /data/conspectus/data -e reading-list -w
  echo "Transforming citations from raw JSON to cleaned JSON"
  python transform.py -f json -w /data/conspectus/data -e citation -w
  echo "Transforming citations from raw JSON to CSV"
  python transform.py -f csv -w /data/conspectus/data -e citation -w
else
  echo "Python 3 not found. Will not run!"
fi
