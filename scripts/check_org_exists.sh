#!/bin/bash

# Simple script to quickly check if an organisation has a leganto instance
# Useful as we have observed organisation names changing

result_array_ok=()
result_array_not_ok=()
org_list="aho dmmh forsvaret himolde hivolda hiof hvl inn kristiania krus ldh mf nla nhh nmbu nord ntnu oslomet phs samas uia uib uio uis uit usn vid"

for org in $org_list; do
  curl_result=$(curl -s -o /dev/null -w "%{http_code}" https://gw-unit.intark.uh-it.no/leganto-open-api/courses?institution=$org)

  if [ "$curl_result" -eq "200" ]; then
    result_array_ok+=("$org")
    echo "$org" "$curl_result" "OK"
  else
    result_org_http_status=$org" "$curl_result
    result_array_not_ok+=("$result_org_http_status")
    echo "$result_org_http_status" "NOT OK"
  fi
  sleep 1
done

echo "Organisations that return 200 OK"

for result in "${result_array_ok[@]}"; do
  echo -n "'$result', "
done

echo ""

echo "Organisations that do not return 200 OK"
for result in "${result_array_not_ok[@]}"; do
  echo "$result"
done
exit
