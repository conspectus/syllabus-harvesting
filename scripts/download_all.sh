#! /bin/bash
# Requires python version 3 installed
if python --version | grep -q '^Python 3\.'; then
  python downloader.py -w /data/conspectus/data -e courses
  python downloader.py -w /data/conspectus/data -e reading_lists
  python downloader.py -w /data/conspectus/data -e citations
else
  echo "Python 3 not found. Will not run!"
fi
