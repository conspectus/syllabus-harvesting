#!/usr/bin/python3

import os
import json
import regex as re

dir = './leganto/uia/raw/courses/'
list = {}

for file in os.listdir(dir):
    file_name = dir + file
    #print(file_name)
    fh = open(file_name)

    data = json.load(fh)

    value = ''
    desc = ''
    if 'academic_department' in data:
        if 'value' in data['academic_department']:
            value = data['academic_department']['value']
        if 'desc' in data['academic_department']:
            desc = data['academic_department']['desc']

    course_code = ''
    # UE_222_MG2EN5_1_2017_H\u00d8ST_1
    match = re.search(r"U[A|E]_\d+_([A-Za-z0-9]+\-*[A-Za-z0-9]*-*[A-Za-z0-9]*)_(.+)_(.+)_(.+)", data['code'])
    if match is not None:
        course_code = match.group(1)

    created_date = ''

    if 'created_date' in data:
        created_date = data['created_date']


#     if value != '':
#         if course_code in list.keys():
#             exists = False
#             for code in list[course_code]:
#                 if code['value'] == value:
#                     exists = True
#             if not exists:
#                 list[course_code].append({'value': value, 'desc': desc})
#         else:
#             list[course_code] = [{'value': value, 'desc': desc}]
#
# print(json.dumps(list))
