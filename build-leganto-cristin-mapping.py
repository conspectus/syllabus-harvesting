import csv
import json
import logging
import os
import regex as re
import sys
from os.path import exists

import pandas as pd

from lib.utility.constants import KNOWN_MANUAL_MAPPING, LEGANTO_COLUMN_NAMES, LEGANTO_DEPT_CODE, \
    UNIT_NAME_EN, UNIT_NAME_NB, SUB_UNIT_NAME_EN, SUB_UNIT_NAME_NB, \
    SUB_SUB_UNIT_NAME_EN, SUB_SUB_UNIT_NAME_NB, UNIT_CODE, SUB_UNIT_CODE, \
    SUB_SUB_UNIT_CODE, PROPOSED_CODE, REGEX_UNIT_FILE_NAME, \
    CRISTIN_COLUMN_NAMES, CONSPECTUS_USE_UNIT_CODE, LEGANTO_DEPT_NAME, LEGANTO_CRISTIN_COLUMN_NAMES, RESOURCES, INPUT, \
    CRISTIN_MAPPINGS, MAPPED, CSV, JSON, LEGANTO_CRISTIN_MAPPING_FILENAME, CRISTIN_ORG_DATA_CSV

INPUT_DIRECTORY_CSV = os.path.join(RESOURCES, CRISTIN_MAPPINGS, INPUT)
OUTPUT_DIRECTORY_CSV = os.path.join(RESOURCES, CRISTIN_MAPPINGS, MAPPED, CSV)

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)


def get_value(series):
    for key in series:
        return series[key]


# Get a list of organisations to process. The source is either a hard coded list in mappings or a
# csv file in the input_csv directory
def get_list_organisations():
    organisations = []
    for key in KNOWN_MANUAL_MAPPING.keys():
        organisations.append(key)
    files = os.listdir(INPUT_DIRECTORY_CSV)
    for file in files:
        re_groups = re.search(REGEX_UNIT_FILE_NAME, file)
        organisations.append(re_groups.group(1))
    return organisations


def process_organisation(organisation):
    input_cristin_csv_file = os.path.join(RESOURCES, CRISTIN_MAPPINGS, CRISTIN_ORG_DATA_CSV)
    df_input_cristin = pd.read_csv(input_cristin_csv_file, sep=',', quotechar='"', names=CRISTIN_COLUMN_NAMES)
    if organisation in KNOWN_MANUAL_MAPPING.keys():
        df_input_leganto = pd.DataFrame.from_dict(KNOWN_MANUAL_MAPPING[organisation])
        # Add remaining of required columns
        for column in LEGANTO_COLUMN_NAMES:
            if column not in df_input_leganto:
                df_input_leganto[column] = ''
    else:
        input_leganto_csv_file = os.path.join(INPUT_DIRECTORY_CSV,
                                              LEGANTO_CRISTIN_MAPPING_FILENAME.format(organisation, 'csv'))
        df_input_leganto = pd.read_csv(input_leganto_csv_file, header=None, sep=';', index_col=False, engine='python',
                                       quotechar='"', names=LEGANTO_COLUMN_NAMES) \
            .replace('"', '', regex=True).replace({"^\\s*|\\s*$": ""}, regex=True)
        df_input_leganto = df_input_leganto.astype('object')

    df_output_cristin = pd.DataFrame(columns=LEGANTO_CRISTIN_COLUMN_NAMES)

    for index in df_input_leganto.index:
        unit_id = str(df_input_leganto.at[index, PROPOSED_CODE])
        if unit_id == 'nan':
            unit_id = str(df_input_leganto.at[index, CONSPECTUS_USE_UNIT_CODE])
        logging.info('Processing {}'.format(unit_id.strip()))
        row = df_input_cristin.loc[df_input_cristin[CONSPECTUS_USE_UNIT_CODE] == unit_id]
        cristin_object = json.loads(row.to_json())
        df_output_cristin.at[index, LEGANTO_DEPT_CODE] = df_input_leganto.at[index, LEGANTO_DEPT_CODE]
        df_output_cristin.at[index, CONSPECTUS_USE_UNIT_CODE] = get_value(cristin_object[CONSPECTUS_USE_UNIT_CODE])
        df_output_cristin.at[index, LEGANTO_DEPT_NAME] = df_input_leganto.at[index, LEGANTO_DEPT_NAME]
        df_output_cristin.at[index, UNIT_CODE] = get_value(cristin_object[UNIT_CODE])
        df_output_cristin.at[index, UNIT_NAME_NB] = get_value(cristin_object[UNIT_NAME_NB])
        df_output_cristin.at[index, UNIT_NAME_EN] = get_value(cristin_object[UNIT_NAME_EN])
        df_output_cristin.at[index, SUB_UNIT_CODE] = get_value(cristin_object[SUB_UNIT_CODE])
        df_output_cristin.at[index, SUB_UNIT_NAME_NB] = get_value(cristin_object[SUB_UNIT_NAME_NB])
        df_output_cristin.at[index, SUB_UNIT_NAME_EN] = get_value(cristin_object[SUB_UNIT_NAME_EN])
        df_output_cristin.at[index, SUB_SUB_UNIT_CODE] = get_value(cristin_object[SUB_SUB_UNIT_CODE])
        df_output_cristin.at[index, SUB_SUB_UNIT_NAME_NB] = get_value(cristin_object[SUB_SUB_UNIT_NAME_NB])
        df_output_cristin.at[index, SUB_SUB_UNIT_NAME_EN] = get_value(cristin_object[SUB_SUB_UNIT_NAME_EN])

    # If you want to regenerate a mapping csv file, delete from csv directory
    output_cristin_csv_file = os.path.join(OUTPUT_DIRECTORY_CSV,
                                           LEGANTO_CRISTIN_MAPPING_FILENAME.format(organisation, 'csv'))

    # Save to CSV
    if not exists(output_cristin_csv_file):
        df_output_cristin.to_csv(output_cristin_csv_file, encoding='utf-8', index=False, header=True,
                                 quoting=csv.QUOTE_ALL)


def main():
    for organisation in get_list_organisations():
        logging.info("Start processing of {}".format(organisation))
        process_organisation(organisation)
        logging.info("End processing of {}".format(organisation))


if __name__ == '__main__':
    main()
    sys.exit(0)
