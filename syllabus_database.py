""" Wrapper to expose database related functionality for courses and associated child
    objects as well as reading lists and citations
"""
from lib.utility.leganto_constants import get_course_column_names, get_course_value_placeholder, \
    get_course_values, get_course_reading_list_column_names, get_course_reading_list_values, \
    get_reading_list_value_placeholder, get_course_citation_column_names, \
    get_citation_value_placeholder, get_course_citation_values_as_tuple, \
    get_course_searchable_id_column_names, get_course_searchable_id_values
from database import Database


class SyllabusDatabase(Database):
    """
       Hold connection to database and expose functionality to the caller.

    Note connection_parameters takes values in the following order:
    [0]host, [1]username, [2]password, [3]database
    """

    def __init__(self, connection_parameters):
        super().__init__(connection_parameters)

    def course_exists(self, course, organisation):
        """Check if a course exists."""
        return self.query_for_one("SELECT * FROM course WHERE course_id='" + course['id'] + "'" +
                                  " and organisation ='" + organisation + "'")

    def course_searchable_id_exists(self, course_id, searchable_id, organisation):
        """Check if a searchable id has previously been associated with a course."""
        return self.query_for_one("SELECT * FROM course_searchable_id WHERE organisation='" +
                                  organisation +
                                  "' and fk_course_id='" + course_id +
                                  "' and searchable_id='" + searchable_id + "'")

    def course_reading_list_exists(self, reading_list_id, organisation):
        """Check if a reading list has previously been persisted to the database."""
        return self.query_for_one("SELECT * FROM course_reading_list WHERE organisation='" +
                                  organisation +
                                  "' and reading_list_id='" + reading_list_id + "'")

    def __course_citation_exists(self, citation_id, organisation):
        """Check if a citation has previously been persisted to the database."""
        return self.query_for_one("SELECT * FROM course_citation WHERE organisation='" +
                                  organisation + "' and citation_id='" + citation_id + "'")

    def get_courses(self, offset, limit, organisation):
        """Get all courses for a given organisation, with a limit."""
        return super().query_for_many("SELECT * FROM course WHERE organisation='" +
                                      organisation + "' LIMIT {}, {}".format(offset, limit))

    def get_reading_lists(self, course_id, organisation):
        """Get all reading_lists for a given organisation and course."""
        return super().query_for_many("SELECT * FROM course_reading_list WHERE organisation='" +
                                      organisation + "' and fk_course_id='{}'".format(course_id))

    def add_course(self, course, organisation):
        """Add a course"""
        if self.course_exists(course, organisation) is None:
            super().insert("INSERT INTO course (" + get_course_column_names() + ") VALUES (" +
                           get_course_value_placeholder() + ")",
                           get_course_values(organisation, course))

    # If the course didn't have a reading list return false, otherwise return true
    def add_course_reading_list(self, course_id, reading_list, organisation):
        """Add single course reading_list associated with a course."""
        if self.course_reading_list_exists(reading_list['id'], organisation) is None:
            super().insert("INSERT INTO course_reading_list (" +
                           get_course_reading_list_column_names() +
                           ") VALUES (" + get_reading_list_value_placeholder() + ")",
                           get_course_reading_list_values(organisation, course_id,
                                                          reading_list))
            return True
        return False

    def add_course_citation(self, course_as_tuple, reading_list_id, citation,
                            organisation):
        """Add single citations associated with a course."""
        if self.__course_citation_exists(citation['id'], organisation) is None:
            super().insert("INSERT INTO course_citation (" +
                           get_course_citation_column_names() +
                           ") VALUES (" + get_citation_value_placeholder() + ")",
                           get_course_citation_values_as_tuple(organisation,
                                                      course_as_tuple,
                                                      reading_list_id, citation))

    def add_searchable_id(self, course, organisation):
        """Add a searchable id (e.g. ARK2200) associated with a course."""
        if 'searchable_id' in course:
            for searchable_id in course['searchable_id']:
                if self.course_searchable_id_exists(course['id'], searchable_id,
                                                    organisation) is None:
                    super().insert("INSERT INTO course_searchable_id (" +
                                   get_course_searchable_id_column_names() +
                                   ") VALUES (%s, %s, %s)",
                                   get_course_searchable_id_values(organisation,
                                                                   course['id'],
                                                                   searchable_id))
