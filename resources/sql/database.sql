# Note! Do not change the order of columns as mysql-connector returns a tuple where values are
# indexed from column 0 onwards. Changing the order will likely have unintended consequences on
# processing of data!
# For mysql 8 use utf8mb4 as it supports 4-byte UTF
# create database pensum CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
# 4 byte UTF-8 support has been problematic. If you have utf-8 problems with mysql 5 upgrade to
# mysql 8 and remember that the character set must be defined as utf8mb4 for the connection

drop database if exists pensum;
create database pensum CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
use pensum;

create table institutions
(
    institution                varchar(30),
    date_harvested             date,
    total_courses              int,
    PRIMARY KEY (institution)
) ENGINE = INNODB;

create table course
(
    institution                varchar(30),           #  0
    local_id                   int,                   # id/count to help track harvesting
    course_id                  varchar(40),           #  2
    course_code                text,                  #  3
    course_name                text,                  #  4
    section                    text,                  #  5
    academic_department_code   text,                  #  6
    academic_department_name   text,                  #  7
    term_code                  text,                  #  8
    term_name                  text,                  #  9
    start_date                 date,                  # 10
    end_date                   date,                  # 11
    weekly_hours               int,                   # 12
    participants               int,                   # 13
    year                       text,                  # 14
    created_date               date,                  # 15
    last_modified_date         varchar(25),           # 16
    rolled_from                text,                  # 17
    citations_harvested        boolean default false, # 18
    course_leganto_code        text,                  # 19
    unit_code                   varchar(15),           # 20
    sub_unit_code               varchar(15),           # 21
    sub_sub_unit_code           varchar(15),           # 22
    sub_sub_sub_unit_code           varchar(15),           # 23
    status                     text,                  # 24
    PRIMARY KEY (institution, course_id)
) ENGINE = INNODB;

create table course_reading_list
(
    institution             varchar(30),
    reading_list_id         varchar(25),
    code                    text,
    name                    text,
    reading_list_order      text,
    description             text,
    last_modified_date      datetime,
    status_value            text,
    status_desc             text,
    visibility_value        text,
    visibility_desc         text,
    publishing_status_value text,
    publishing_status_desc  text,
    fk_course_id            varchar(40),
    blocked                 boolean default false,
    primary key (institution, reading_list_id)
) ENGINE = INNODB;

create table course_citation
(
    institution                     varchar(30),
    citation_id                     varchar(25),
    status_value                    text,
    status_desc                     text,
    copyrights_status_value         text,
    copyrights_status_desc          text,
    secondary_type_value            text,
    secondary_type_desc             text,
    type_value                      text,
    type_desc                       text,
    metadata_title                  text,
    metadata_author                 text,
    metadata_publisher              text,
    metadata_publication_date       text,
    metadata_edition                text,
    metadata_isbn                   text,
    metadata_issn                   text,
    metadata_mms_id                 text,
    metadata_additional_person_name text,
    metadata_place_of_publication   text,
    metadata_call_number            text,
    metadata_note                   text,
    metadata_issue                  text,
    metadata_journal_title          text,
    metadata_article_title          text,
    metadata_editor                 text,
    metadata_chapter                text,
    metadata_chapter_title          text,
    metadata_chapter_author         text,
    metadata_year                   text,
    metadata_pages                  text,
    metadata_source                 text,
    metadata_series_title_number    text,
    metadata_pmid                   text,
    metadata_doi                    text,
    metadata_volume                 text,
    metadata_start_page             text,
    metadata_end_page               text,
    metadata_start_page2            text,
    metadata_end_page2              text,
    metadata_author_initials        text,
    metadata_part                   text,
    open_url                        text,
    leganto_permalink               text,
    public_note                     text,
    source1                         text,
    source2                         text,
    source3                         text,
    source4                         text,
    source5                         text,
    source6                         text,
    source7                         text,
    source8                         text,
    source9                         text,
    source10                        text,
    last_modified_date              datetime,
    section_info_id                 text,
    section_info_name               text,
    section_info_description        text,
    section_info_visibility         boolean,
    section_info_tags_total_rec_cnt int,
    section_info_section_tags_link  text,
    section_info_section_locked     boolean,
    citation_link                   text,
    file_link                       text,
    fk_reading_list_id              varchar(25),
    fk_course_id                    varchar(25),
    unit_code                        varchar(15),
    sub_unit_code                    varchar(15),
    sub_sub_unit_code                varchar(15),
    sub_sub_sub_unit_code                varchar(15),
    primary key (institution, citation_id)
) ENGINE = INNODB;

create table course_term
(
    institution  varchar(30),
    fk_course_id varchar(40),
    term_code    varchar(15),
    term_name    varchar(50),
    PRIMARY KEY (institution, fk_course_id, term_code)
) ENGINE = INNODB;

create table course_note
(
    institution   varchar(30),
    fk_course_id  varchar(40),
    content       text,
    creation_date date,
    type          text,
    note_checksum  varchar(64),
    PRIMARY KEY (institution, fk_course_id, note_checksum, creation_date)
) ENGINE = INNODB;

create table course_searchable_id
(
    institution   varchar(30),
    fk_course_id  varchar(40),
    searchable_id varchar(120),
    PRIMARY KEY (institution, fk_course_id, searchable_id)
) ENGINE = INNODB;

create table course_instructor
(
    institution   varchar(30),
    fk_course_id  varchar(40),
    instructor_id varchar(25),
    first_name    varchar(1500),
    last_name     varchar(1500),
    year          varchar(10),
    primary key (institution, fk_course_id, instructor_id)
) ENGINE = INNODB;

create table counters
(
    institution  varchar(30),
    course       int,
    reading_list int,
    citations    int,
    blocked_reading_list int,
    PRIMARY KEY (institution)
) ENGINE = INNODB;

create table reading_list_note
(
    institution        varchar(30),
    fk_reading_list_id varchar(40),
    content            text,
    creation_date      date,
    type               text,
    note_checksum  varchar(64),
    PRIMARY KEY (institution, fk_reading_list_id, note_checksum, creation_date)
) ENGINE = INNODB;

create table citation_note
(
    institution    varchar(30),
    fk_citation_id varchar(40),
    content        text,
    creation_date  date,
    type           text,
    note_checksum  varchar(64),
    PRIMARY KEY (institution, fk_citation_id, creation_date, note_checksum)
) ENGINE = INNODB;

alter table course_reading_list add index (reading_list_id);
alter table course_citation add index (citation_id);
alter table course_citation add index (institution);

alter table course_instructor
    add foreign key (institution, fk_course_id) references course (institution, course_id);
alter table course_citation
    add foreign key (institution, fk_reading_list_id) references course_reading_list
        (institution, reading_list_id);
alter table course_citation
    add foreign key (institution, fk_course_id) references course (institution, course_id);
alter table course_term
    add foreign key (institution, fk_course_id) references course (institution, course_id);
alter table course_note
    add foreign key (institution, fk_course_id) references course (institution, course_id);
alter table course_searchable_id
    add foreign key (institution, fk_course_id) references course (institution, course_id);
alter table course_reading_list
    add foreign key (institution, fk_course_id) references course (institution, course_id);
alter table reading_list_note
    add foreign key (institution, fk_reading_list_id) references course_reading_list
        (institution, reading_list_id);
alter table citation_note
    add foreign key (institution, fk_citation_id) references course_citation
        (institution, citation_id);
