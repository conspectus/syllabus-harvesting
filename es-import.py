# Script to bulk import citation json files to an elasticsearch instance
import getopt
import json
import logging

import urllib3 as urllib3
import os
import sys
from urllib.error import HTTPError, URLError
from lib.file.JsonFile import JsonFile
from lib.utility.constants import PROCESS, ALL, CONSPECTUS_COURSE_NAME, NAME, ID
from lib.utility.leganto_constants import get_es_values_citation

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)

index = 'citation'
http = urllib3.PoolManager()

UPLOADED_OK = 'For {}, file # {}. Uploaded {} citations'
UPLOADED_NOT_OK = 'For {}, file {}. Problem uploading {} citations, result is {}'
PROCESS_REPORT = 'Processed {} citations in {} citation files for {}'


def import_organisation(organisation, working_dir, url_es):
    file = JsonFile(organisation, working_dir, PROCESS)
    citations_dir, citation_files = file.get_list_citation_files()
    file_count = 1
    citations_count = 0
    for citation_file in citation_files:
        with open(os.path.join(citations_dir, citation_file)) as current_file:
            citations = json.load(current_file)
            citation_data = ''

            if 'citation' in citations['citations'].keys():
                for citation in citations['citations']['citation']:
                    citation[NAME] = citations[CONSPECTUS_COURSE_NAME]
                    citation_data += json.dumps({'index': {'_id': citation[ID]}}) + '\n'
                    citation_data += json.dumps(get_es_values_citation(citation, citations)) + '\n'
                    citations_count += 1
                result = do_post_request(url_es, citation_data)
                if result.status != 200:
                    logging.info(UPLOADED_NOT_OK.format(organisation, citation_file,
                                                        str(len(citations['citations']['citation'])),
                                                        str(result.status)))
                else:
                    logging.info(UPLOADED_OK.format(organisation, str(file_count),
                                                    str(len(citations['citations']['citation']))))
            file_count += 1
        current_file.close()

    logging.info(PROCESS_REPORT.format(str(citations_count), str(file_count), organisation))


def get_options_retrieve(argv):
    try:
        working_dir = 'leganto'
        organisation_to_process = ALL
        url_es = 'http://localhost:9200/'
        opts, args = getopt.getopt(argv, 'h:w:o:u:i')
        for opt, arg in opts:
            if opt == '-h':
                logging.info('es-import.py -w <working-dir> -o <organisation> -u <url-elasticsearch> -i <index-name>')
                sys.exit()
            if opt in '-w':
                working_dir = arg
            if opt in '-o':
                organisation_to_process = arg
            if opt in '-u':
                url_es = arg
        return organisation_to_process, working_dir, url_es
    except getopt.GetoptError:
        logging.info('es-import.py -w <working-dir> -o <organisation> -u <url-elasticsearch> -i <index-name>')
        sys.exit(2)


def main(argv):
    organisation_to_process, working_dir, url_es = get_options_retrieve(argv)
    # We assume any directory in the working directory corresponds to a third level organisation
    if organisation_to_process == ALL:
        organisations = os.listdir(working_dir)
    else:
        organisations = [organisation_to_process]
    url_es_index = url_es + index + '/_bulk'
    for organisation in organisations:
        import_organisation(organisation, working_dir, url_es_index)


def do_post_request(url_es, body):
    headers = {'Content-Type': 'application/json'}
    try:
        result = http.request('POST', url_es, headers=headers, body=body)
        return result
    except HTTPError as error:
        logging.info(error.status, error.reason)
    except URLError as error:
        logging.info(error.reason)
    except TimeoutError:
        logging.info('Request timed out')


if __name__ == '__main__':
    main(sys.argv[1:])
    sys.exit(0)
