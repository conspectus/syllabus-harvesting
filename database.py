""" Wrapper to expose database related functionality to subclasses"""
import mysql.connector


class Database(object):
    """
       Hold connection to database and expose functionality to the caller.
    Note connection_parameters takes values in the following order:
    [0]host, [1]username, [2]password, [3]database
    """

    def __init__(self, connection_parameters):
        self.database = mysql.connector.connect(host=connection_parameters[0],
                                                user=connection_parameters[1],
                                                password=connection_parameters[2],
                                                database=connection_parameters[3],
                                                charset="utf8mb4")

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type or exc_tb or exc_tb:
            self.database.close()
        self.database.commit()
        self.database.close()

    def close(self):
        """Commit any open transactions and close the database."""
        self.database.commit()
        self.database.close()

    def query_for_one(self, sql):
        """Run a query that should return a single tuple."""
        cursor = self.database.cursor()
        cursor.execute(sql)
        return cursor.fetchone()

    def query_for_many(self, sql):
        """Run a query likely to return multiple tuples."""
        cursor = self.database.cursor()
        cursor.execute(sql)
        return cursor.fetchall()

    def insert(self, sql, values):
        """Run a DML to insert/update a tuple."""
        cursor = self.database.cursor()
        cursor.execute(sql, values)
        self.database.commit()
