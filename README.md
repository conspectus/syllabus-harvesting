# Syllabus Harvesting

A set of python scripts to download and process data from a Leganto API (Not the official one, 
but a dissemination one). Note, this has only been tested on Norwegian instances. It is likely 
the dataquality problems the script deals with may be a problem for instances in other countries. 

## Running

### Downloading

The script interacts with the leganto API in the following way.
 1. Retrieve all courses and store each course as an individual json file
 2. Using the courses json file, retrieve and store reading lists json files 
 3. Using the courses / reading lists json files, retrieve and store citations files (Note: one 
    json file of citations per reading list)

For (2) and (3)  to be run, you need to have first downloaded all course information.

#### Before running

The script is developed using python3.10. 

The script requires the existence of a _working directory_. Unless you override this, the 
directory will be a combination of year and month e.g. 2024-1. Under the working directory the 
script will create a directory for each known leganto organisation. Each organisation will have a 
raw directory that contains 'courses', 'reading_lists' and 'citations' directories. A normal run 
can go over a couple of days, so be careful if you starting running during the last days of the 
month, you may end up with data in different directories. 

For all actions the scripts assume a particular working directory. This is for simplicity purposes.
Omitting -o implies ALL (script) known leganto courses will be retrieved. To 
download courses for individual organisations see the 'List of known organisations'  later in this 
document to get the correct name of an organisation. In some cases the code has support to 
download a range of data (start, stop), however the current approach is to (re) download all data 
at the end of every semester. 

#### Retrieve courses

 ```python downloader.py -o=oslomet -e=course```

#### Retrieve reading lists

```python downloader.py -o=oslomet -e=reading-list```

#### Retrieve citations

```python downloader.py -o=oslomet -e=citation```

### Transform courses

The courses that are downloaded are the raw contents of the JSON payloads from the leganto API. 
There are some data quality issues in these data and there is a bit of redundent data that is 
likely of no interest to most users. If you want to use the raw data, you can simply stop here 
and go ahead and work with the data. If you want a cleaned up version of the data you can run 
the scripts detailed shortly. These scripts run offline and make use of the downloaded data. the 
following actions can be undertaken:

1. Convert from json to json
2. Convert from json to csv
3. Convert from json to SQL

Process 1. converts the source json files to destination json files, maintaining the same name 
as the original file. Process 2 converts the contents of the json files to a single a CSV file. 
Process 3 converts the json files to a relational database structure. The script will have to be 
called multiple times (course, reading lists, and citations), or you can simply use the 
convert_all.sh script and this will take care of the conversion. _Note: Do not attempt to use 
transform.py unless you have first downloaded the raw files. Also, the script assumes the same 
working directory when transforming. There is no distinction for a different working directory 
base for output_. 

#### json2json

To run a transformation from (raw) json to (cleaned) json use the following commands:   

```
python transform.py -f=json -e=courses
python transform.py -f=json -e=reading-list
python transform.py -f=json -e=citations
```

#### json2csv
To run a transformation from (raw) json to (cleaned) csv use the following commands:   

```
python transform.py -f=csv -e=courses
python transform.py -f=csv -e=reading-list
python transform.py -f=csv -e=citations
```

#### json2sql

Before doing anything here, make sure you have run the db-utils.py script described later in the 
document. To run a transformation from (raw) json to (cleaned) db/sql use the following 
commands:   

```
python transform.py -f=db -e=courses
python transform.py -f=db -e=reading-list
python transform.py -f=db -e=citations
```
This will place  acopy of the contents in a file called syllabus.sqlite. It is possible to 
override the name of the file using the `--db-file` option. It should not be too 
difficult to get the db/sql code to save to other relational database systems, but currently we 
are not concerned with that. 

Note. The script uses the cleaned json when working with reading lists and citations. Makesure 
you have run all transform to json first. 

### Database operations 

### Utility scripts

There are two utility bash shell scrips that can run all the commands for you. download_all.sh 
and transform_all.sh. They check that python 3 is installed before running.

#### download_all.sh

This bash script calls the download script in the correct order 

#### transform_all.sh

This bash script calls the transform script and converts to all _to_ supported formats 

### Cleaning up

To keep things simple, if you want to download everything again, simply delete the working 
directory and the scripts will build the required structures. Running a new harvesting/transform 
process with existing data may result inconsistent data, as the API we interact with may change 
payload contents over time. The approach here does not attempt to augment already downloaded 
material with new material.

## List of known organisations

The following is a list of know organisations that make their Leganto data available:
  - 
  - aho 
  - dmmh 
  - forsvaret  
  - himolde  
  - hiof  
  - hivolda  
  - hvl 
  - inn 
  - kristiania  
  - krus  
  - ldh  
  - mf  
  - nhh  
  - nla  
  - nmbu 
  - nord 
  - ntnu
  - oslomet
  - phs  
  - samas  
  - uia  
  - uib  
  - uio  
  - uis  
  - uit  
  - usn  
  - vid


```

python transform.py -f json -w ./leganto-2024/ -e courses -o uib
python transform.py -f json -w ./leganto-2024/ -e reading_lists -o uib
python transform.py -f json -w ./leganto-2024/ -e citations -o uib
python transform.py -f csv -w ./leganto-2024/ -e courses -o uib
python transform.py -f csv -w ./leganto-2024/ -e reading_lists -o uib
python transform.py -f csv -w ./leganto-2024/ -e citations -o uib

python transform.py -f json -w /data/conspectus/data/2024-3 -e courses -o uib; python transform.py -f json -w /data/conspectus/data/2024-3 -e reading_lists -o uib; python transform.py -f json -w /data/conspectus/data/2024-3 -e citations -o uib
python transform.py -f json -w /data/conspectus/data/2024-3 -e courses -o uio; python transform.py -f json -w /data/conspectus/data/2024-3 -e reading_lists -o uio; python transform.py -f json -w /data/conspectus/data/2024-3 -e citations -o uio
python transform.py -f json -w /data/conspectus/data/2024-3 -e courses -o oslomet; python transform.py -f json -w /data/conspectus/data/2024-3 -e reading_lists -o oslomet; python transform.py -f json -w /data/conspectus/data/2024-3 -e citations -o oslomet
python transform.py -f json -w /data/conspectus/data/2024-3 -e courses -o hvl; python transform.py -f json -w /data/conspectus/data/2024-3 -e reading_lists -o hvl; python transform.py -f json -w /data/conspectus/data/2024-3 -e citations -o hvl
python transform.py -f json -w /data/conspectus/data/2024-3 -e courses -o ntnu; python transform.py -f json -w /data/conspectus/data/2024-3 -e reading_lists -o ntnu; python transform.py -f json -w /data/conspectus/data/2024-3 -e citations -o ntnu
python transform.py -f json -w /data/conspectus/data/2024-3 -e courses -o usn; python transform.py -f json -w /data/conspectus/data/2024-3 -e reading_lists -o usn; python transform.py -f json -w /data/conspectus/data/2024-3 -e citations -o usn

python transform.py -f json -w /data/conspectus/data/2024-3 -e courses -o uia; python transform.py -f json -w /data/conspectus/data/2024-3 -e reading_lists -o uia; python transform.py -f json -w /data/conspectus/data/2024-3 -e citations -o uia
python transform.py -f csv -w /data/conspectus/data/2024-3 -e courses -o uia; python transform.py -f csv -w /data/conspectus/data/2024-3 -e reading_lists -o uia; python transform.py -f csv -w /data/conspectus/data/2024-3 -e citations -o uia

python transform.py -f json -w /data/conspectus/data/2024-3 -e courses -o oslomet; python transform.py -f json -w /data/conspectus/data/2024-3 -e reading_lists -o oslomet; python transform.py -f json -w /data/conspectus/data/2024-3 -e citations -o oslomet
python transform.py -f csv -w /data/conspectus/data/2024-3 -e courses -o oslomet; python transform.py -f csv -w /data/conspectus/data/2024-3 -e reading_lists -o oslomet; python transform.py -f csv -w /data/conspectus/data/2024-3 -e citations -o oslomet

VERTSGUID-
python transform.py -f json -w ./leganto -e courses -o ntnu; python3 transform.py -f json -w ./leganto -e reading_lists -o ntnu; python3 transform.py -f json -w ./leganto -e citations -o ntnu
python3 transform.py -f csv -w ./leganto -e courses -o ntnu; python3 transform.py -f csv -w ./leganto -e reading_lists -o ntnu; python3 transform.py -f csv -w ./leganto -e citations -o ntnu

python3 transform.py -f json -w ./leganto -e courses -o usn; python3 transform.py -f json -w ./leganto -e reading_lists -o usn; python3 transform.py -f json -w ./leganto -e citations -o usn
python3 transform.py -f csv -w ./leganto -e courses -o usn; python3 transform.py -f csv -w ./leganto -e reading_lists -o usn; python3 transform.py -f csv -w ./leganto -e citations -o usn

python transform.py -f json -w ./leganto -e courses -o uit; python transform.py -f json -w ./leganto -e reading_lists -o uit; python transform.py -f json -w ./leganto -e citations -o uit
python transform.py -f csv -w ./leganto -e courses -o uit; python transform.py -f csv -w ./leganto -e reading_lists -o uit; python transform.py -f csv -w ./leganto -e citations -o uit

python transform.py -f csv -w ./leganto -e courses -o uit; python transform.py -f csv -w ./leganto -e reading_lists -o uit; python transform.py -f csv -w ./leganto -e citations -o uit

python transform.py -f json -w ./leganto -e courses -o uib; python transform.py -f json -w ./leganto -e reading_lists -o uib; python transform.py -f json -w ./leganto -e citations -o uib
python transform.py -f json -w ./leganto -e courses -o uio; python transform.py -f json -w ./leganto -e reading_lists -o uio; python transform.py -f json -w ./leganto -e citations -o uio
python transform.py -f json -w ./leganto -e courses -o oslomet; python transform.py -f json -w ./leganto -e reading_lists -o oslomet; python transform.py -f json -w ./leganto -e citations -o oslomet
python transform.py -f json -w ./leganto -e courses -o hvl; python transform.py -f json -w ./leganto -e reading_lists -o hvl; python transform.py -f json -w ./leganto -e citations -o hvl
python transform.py -f json -w ./leganto -e courses -o ntnu; python transform.py -f json -w ./leganto -e reading_lists -o ntnu; python transform.py -f json -w ./leganto -e citations -o ntnu
python transform.py -f json -w ./leganto -e courses -o usn; python transform.py -f json -w ./leganto -e reading_lists -o usn; python transform.py -f json -w ./leganto -e citations -o usn




# The following are missing organisational identfiers for processing

python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o nhh
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o usn
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o himolde

python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o nhh
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o usn
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o himolde

python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o nhh
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o usn
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o himolde

python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o nhh
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o usn
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o himolde

python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o nhh
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o usn
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o himolde

python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o nhh
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o usn
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o himoldepython3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o nhh
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o usn
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o himolde

python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o nhh
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o usn
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o himolde

python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o nhh
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o usn
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o himolde

python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o nhh
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o usn
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o himolde

python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o nhh
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o usn
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o himolde

python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o nhh
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o usn
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o himolde


# JSON : COURSES

python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o aho
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o dmmh
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o forsvaret
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o hvl
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o himolde
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o hiof
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o hivolda
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o inn
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o kristiania
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o krus
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o ldh
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o mf
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o nhh
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o nla
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o nmbu
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o nord
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o ntnu
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o oslomet
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o phs
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o samas
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o uia
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o uib
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o uio
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o uis
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o uit
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o usn
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e courses -o vid

# JSON : READING LISTS

python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o aho
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o dmmh
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o forsvaret
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o hvl
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o himolde
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o hiof
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o hivolda
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o inn
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o kristiania
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o krus
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o ldh
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o mf
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o nhh
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o nla
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o nmbu
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o nord
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o ntnu
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o oslomet
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o phs
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o samas
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o uia
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o uib
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o uio
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o uis
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o uit
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o usn
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e reading_lists -o vid

# JSON : CITATIONS

python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o aho
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o dmmh
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o forsvaret
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o hvl
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o himolde
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o hiof
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o hivolda
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o inn
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o kristiania
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o krus
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o ldh
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o mf
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o nhh
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o nla
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o nmbu
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o nord
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o ntnu
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o oslomet
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o phs
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o samas
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o uia
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o uib
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o uio
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o uis
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o uit
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o usn
python3.9 transform.py -f json -w /data/conspectus/data/2024-3/ -e citations -o vid

# CSV : COURSES

python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o aho
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o dmmh
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o forsvaret
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o hvl
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o himolde
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o hiof
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o hivolda
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o inn
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o kristiania
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o krus
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o ldh
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o mf
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o nhh
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o nla
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o nmbu
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o nord
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o ntnu
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o oslomet
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o phs
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o samas
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o uia
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o uib
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o uio
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o uis
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o uit
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o usn
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e courses -o vid

# CSV : READING LISTS

python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o aho
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o dmmh
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o forsvaret
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o hvl
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o himolde
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o hiof
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o hivolda
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o inn
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o kristiania
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o krus
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o ldh
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o mf
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o nhh
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o nla
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o nmbu
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o nord
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o ntnu
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o oslomet
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o phs
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o samas
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o uia
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o uib
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o uio
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o uis
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o uit
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o usn
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e reading_lists -o vid

# CSV : CITATIONS

python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o aho
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o dmmh
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o forsvaret
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o hvl
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o himolde
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o hiof
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o hivolda
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o inn
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o kristiania
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o krus
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o ldh
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o mf
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o nhh
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o nla
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o nmbu
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o nord
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o ntnu
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o oslomet
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o phs
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o samas
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o uia
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o uib
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o uio
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o uis
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o uit
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o usn
python3.9 transform.py -f csv -w /data/conspectus/data/2024-3/ -e citations -o vid


```

There are three sets of files in use whetransforming data.

* cristin-mappings\input
* cristin-mappings\output
* cristin-mappings\cristin-org-data.csv


cristin-org-datais a courtesy filewherewhave downloadedcristin data for the organisations that use leganto
The purpose is to save timeand have a localcopyof cristin data. The file cristin-org-data.csv is built using
build-cristin-indexes-py