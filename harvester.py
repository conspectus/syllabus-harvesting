import json
import logging
import time
from random import randint
from urllib.error import HTTPError, URLError
from urllib.request import urlopen



class Harvester(object):
    MAX_ATTEMPTS = 8

    def __init__(self, base_url, organisation):
        self.logger = logging.getLogger(__name__)
        self.courses_url = base_url + '?institution=' + organisation
        self.reading_list_url = base_url + '/{}/reading-lists?institution=' + organisation
        self.citations_url = base_url + '/{}/reading-lists/{}?institution=' + organisation + \
                             '&view=full'

    def get_courses(self, start, how_many, attempt=1):
        """Get a list of courses within a range defined by start and how many to retrieve.
           This method constructs a URL for the reading list of a given course ID,
           makes an HTTP request to retrieve the data, and returns the parsed JSON
           payload along with the HTTP status code. If a 400 Bad Request, 502 Bad Gateway error or
           504 Gateway Time-out is encountered is, it will retry the request with
           an increasing backoff.

            Args:
                start (int): The starting index for course retrieval.
                how_many (int): The number of courses to retrieve.
                attempt (int): The current attempt number (default is 1).

           Returns:
               tuple: A tuple containing the parsed JSON payload and the HTTP status code.

           Raises:
               RuntimeError: If an HTTP error occurs (other than 400/502/504) or if the server cannot be reached.
               RuntimeError: If an unexpected error occurs while accessing the URL.

           Notes:
               - If a 400/502/504 error is encountered, the method will log a warning, sleep for a random
                 backoff period, and retry the request up to a maximum number of attempts.
               - Other HTTP errors and URL errors will be logged as errors, and a RuntimeError
                 will be raised with details about the failure.
        """
        course_url_with_offset = self.courses_url + '&limit={}&offset={}'
        if attempt > self.MAX_ATTEMPTS:
            raise RuntimeError(f"Stopping after [{attempt}] attempts on [{self.courses_url}] as it returns 400/502/504")

        try:
            connection = urlopen(course_url_with_offset.format(how_many, start))
            payload = json.loads(connection.read().decode("utf-8"))
            return payload, connection.status
        except HTTPError as e:
            if e.code in (400, 502, 504):
                self.logger.warning('Endpoint giving a %s: %s', str(e.code), e)
                self.random_backoff_sleep(attempt)
                return self.get_courses(start, how_many, attempt + 1)
            else:
                self.logger.error('HTTP error: %s', e)
                raise RuntimeError(
                    f'HTTP error {e.code} occurred while accessing {self.courses_url}. Error message: {e}') from e
        except URLError as e:
            self.logger.error('Cannot reach the server [%s]: %s', self.courses_url, e)
            raise RuntimeError(f'Failed to reach the server at {self.courses_url}. Error message: {e}') from e
        except Exception as e:
            self.logger.error('An unexpected error occurred: %s', e)
            raise RuntimeError(
                f'An unexpected error occurred while accessing {self.courses_url}. Error message: {e}') from e

    def get_reading_lists(self, course_id, attempt=1):
        """Retrieve the reading lists for a specified course.

           This method constructs a URL for the reading list of a given course ID,
           makes an HTTP request to retrieve the data, and returns the parsed JSON
           payload along with the HTTP status code. If a 400 Bad Request, 502 Bad Gateway error or
           504 Gateway Time-out is encountered, it will retry the request with an
           increasing backoff.

           Args:
               course_id (str): The ID of the course for which to retrieve the reading lists.
               attempt (int): The current attempt number for the request (default is 1).

           Returns:
               tuple: A tuple containing the parsed JSON payload and the HTTP status code.

           Raises:
               RuntimeError: If an HTTP error occurs (other than 400/502/504) or if the server cannot be reached.
               RuntimeError: If an unexpected error occurs while accessing the URL.

           Notes:
               - If a 400/502/504 error is encountered, the method will log a warning, sleep for a random
                 backoff period, and retry the request up to a maximum number of attempts.
               - Other HTTP errors and URL errors will be logged as errors, and a RuntimeError
                 will be raised with details about the failure.
           """
        url = self.reading_list_url.format(course_id)
        if attempt > self.MAX_ATTEMPTS:
            raise RuntimeError(f"Stopping after [{attempt}] attempts on [{url}] as it returns 400/502/504")

        try:
            connection = urlopen(url)
            payload = json.loads(connection.read().decode("utf-8"))
            return payload, connection.status
        except HTTPError as e:
            if e.code in (400, 502, 504):
                self.logger.warning('Endpoint giving a %s: %s', str(e.code), e)
                self.random_backoff_sleep(attempt)
                return self.get_reading_lists(course_id, attempt + 1)
            else:
                self.logger.error('HTTP error: %s', e)
                raise RuntimeError(
                    f'HTTP error {e.code} occurred while accessing {url}. Error message: {e}') from e
        except URLError as e:
            self.logger.error('Cannot reach the server [%s]: %s', url, e)
            raise RuntimeError(f'Failed to reach the server at {url}. Error message: {e}') from e
        except Exception as e:
            self.logger.error('An unexpected error occurred: %s', e)
            raise RuntimeError(
                f'An unexpected error occurred while accessing {url}. Error message: {e}') from e

    def get_citations(self, course_id, reading_list_id, attempt=1):
        """Retrieve the citations for a specified reading list.

           This method constructs a URL for the citations using a given course ID,
           and reading list ID and makes an HTTP request to retrieve the data, and
           returns the parsed JSON payload along with the HTTP status code. If a
           400 Bad Request, 502 Bad Gateway or 504 Gateway Time-out is encountered, it will retry the
           request with an increasing backoff.

           Args:
               course_id (str): The ID of the course that has the associated reading list.
               reading_list_id (str): The ID of the reading lists for which to retrieve the citations.
               attempt (int): The current attempt number for the request (default is 1).

           Returns:
               tuple: A tuple containing the parsed JSON payload and the HTTP status code.

           Raises:
               RuntimeError: If an HTTP error occurs (other than 400/502/504) or if the server cannot be reached.
               RuntimeError: If an unexpected error occurs while accessing the URL.

           Notes:
               - If a 400/502/504 error is encountered, the method will log a warning, sleep for a random
                 backoff period, and retry the request up to a maximum number of attempts.
               - Other HTTP errors and URL errors will be logged as errors, and a RuntimeError
                 will be raised with details about the failure.
           """
        """Retrieve the citations for a specified reading list."""
        url = self.citations_url.format(course_id, reading_list_id)
        self.logger.debug('Requesting URL: %s', url)

        if attempt > self.MAX_ATTEMPTS:
            raise RuntimeError(f"Stopping after [{attempt}] attempts on [{url}] as it returns 400/502/504")

        try:
            connection = urlopen(url)
            payload = json.loads(connection.read().decode("utf-8"))
            return payload, connection.status  # Successful response
        except HTTPError as e:
            if e.code in (400, 502, 504):
                self.logger.warning('Endpoint giving a %s: %s', str(e.code), e)
                self.random_backoff_sleep(attempt)
                self.logger.debug('Retrying request... Attempt [%d]', attempt + 1)
                return self.get_citations(course_id, reading_list_id, attempt + 1)  # Retry the request
            else:
                self.logger.error('HTTP error: %s', e)
                raise RuntimeError(
                    f'HTTP error {e.code} occurred while accessing {url}. Error message: {e}') from e
        except URLError as e:
            self.logger.error('Cannot reach the server [%s]: %s', url, e)
            raise RuntimeError(f'Failed to reach the server at {url}. Error message: {e}') from e
        except Exception as e:
            self.logger.error('An unexpected error occurred: %s', e)
            raise RuntimeError(
                f'An unexpected error occurred while accessing {url}. Error message: {e}') from e

    def get_courses_total(self):
        with urlopen(self.courses_url) as connection:
            courses = json.loads(connection.read().decode("utf-8"))
            return courses['total_record_count']


    def random_backoff_sleep(self, attempt):
        """
        Introduces a random sleep time that increases with each attempt.

        Args:
            attempt (int): The current attempt number, used to calculate the wait time.

        Raises:
            ValueError: If the attempt is less than 1.
        """
        if attempt < 1:
            raise ValueError("Attempt must be a positive integer.")

        # Generate a random wait time between 4 and 4 + (attempt * 3)
        wait = randint(4, 4 + (attempt * 3))
        self.logger.warning(f'Backing off. Waiting [{wait}] seconds. Attempt [{attempt}]')
        time.sleep(wait)
        self.logger.warning(f'Finished planed backing off.')
