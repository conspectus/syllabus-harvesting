# Simple script to build a local copy of all leganto organisations as a csv file. It produces a file called
# 'cristin_data.csv' in the resources/cristin-mappings directory. This file is used when building the
# leganto/cristin mappings
import csv
import json
import logging
import os.path
import sys

import pandas as pd
from time import sleep

from lib.utility.constants import \
    CRISTIN_COLUMN_NAMES, ORGS, URL, HAS_PART, ORG_NAME, VALUE, RESOURCES, CRISTIN_MAPPINGS, CRISTIN_ORG_DATA_CSV, \
    SUB_SUB_UNIT_NAME_NB, SUB_UNIT_CODE, SUB_UNIT_NAME_NB, SUB_SUB_UNIT_CODE, PROCESSED_UNIT_MSG, \
    PROCESSED_SUB_UNIT_MSG, PROCESSED_SUB_SUB_UNIT_MSG, PROCESSED_SUB_SUB_SUB_UNIT_MSG, SUB_SUB_SUB_UNIT_CODE, \
    SUB_SUB_SUB_UNIT_NAME_NB
from lib.utility.http_utility import do_request
from lib.utility.leganto_utility import get_org_object_unit, get_org_object_sub_unit, get_org_object_sub_sub_unit, \
    get_org_object_sub_sub_sub_unit

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)


def do_process(cristin_id, df):
    if cristin_id == '186.0.0.0':
        with open(os.path.join(RESOURCES, 'other', 'uit_processed_cristin.json'), 'r') as uit_file:
            org_data = json.loads(uit_file.read())
    else:
        org_data = do_request(URL.format(cristin_id))
        sleep(0.8)

    org_row = get_org_object_unit(org_data)
    df.loc[len(df)] = org_row
    if HAS_PART in org_data.keys():
        for sub_unit_data in org_data[HAS_PART]:
            sub_unit_row = get_org_object_sub_unit(org_data, sub_unit_data)
            logging.info(PROCESSED_SUB_UNIT_MSG.format(sub_unit_row[SUB_UNIT_CODE],
                                                       sub_unit_row[SUB_UNIT_NAME_NB]))
            df.loc[len(df)] = sub_unit_row
            if HAS_PART in sub_unit_data.keys():
                for sub_sub_unit_data in sub_unit_data[HAS_PART]:
                    sub_sub_unit_row = get_org_object_sub_sub_unit(org_data, sub_unit_data, sub_sub_unit_data)
                    logging.info(PROCESSED_SUB_SUB_UNIT_MSG.format(sub_sub_unit_row[SUB_SUB_UNIT_CODE],
                                                                   sub_sub_unit_row[SUB_SUB_UNIT_NAME_NB]))
                    df.loc[len(df)] = sub_sub_unit_row
                    if HAS_PART in sub_sub_unit_data.keys():
                        for sub_sub_sub_unit_data in sub_sub_unit_data[HAS_PART]:
                            sub_sub_sub_unit_row = get_org_object_sub_sub_sub_unit(
                                org_data, sub_unit_data, sub_sub_unit_data, sub_sub_sub_unit_data)
                            logging.info(PROCESSED_SUB_SUB_SUB_UNIT_MSG.format(sub_sub_sub_unit_row[SUB_SUB_SUB_UNIT_CODE],
                                                                           sub_sub_sub_unit_row[SUB_SUB_SUB_UNIT_NAME_NB]))
                            df.loc[len(df)] = sub_sub_sub_unit_row


def build_cristin_list():
    df = pd.DataFrame(columns=CRISTIN_COLUMN_NAMES)
    for organisation in ORGS:
        logging.info(PROCESSED_UNIT_MSG.format(ORGS[organisation][ORG_NAME],
                                        ORGS[organisation][VALUE]))
        cristin_id = ORGS[organisation][VALUE]
        do_process(cristin_id, df)

    df.to_csv(os.path.join(RESOURCES, CRISTIN_MAPPINGS, CRISTIN_ORG_DATA_CSV),
              encoding='utf-8', index=False, header=True, quoting=csv.QUOTE_ALL)


if __name__ == '__main__':
    build_cristin_list()
    sys.exit(0)
