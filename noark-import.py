# Script to bulk convert citation json files to a noark5 arkivstruktur.xml
import getopt
import json
import logging
import os
import sys
import uuid

from xml.sax.saxutils import escape

import urllib.parse
from lib.file.JsonFile import JsonFile
from lib.utility.constants import PROCESS, ALL, CONSPECTUS_COURSE_NAME, NAME
from lib.utility.leganto_constants import get_noark_bsm_values_citation, get_noark_bsm_values_course
from datetime import datetime
from zoneinfo import ZoneInfo

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)

PROCESS_REPORT = 'Processed {} citations in {} citation files for {}'
DATE_FORMAT = '%Y-%m-%dT%H:%M:%S'
#DATE_FORMAT = '%Y-%m-%dT%H:%M:%S%z'
ZONE = ZoneInfo('Europe/Oslo')


def get_datetime():
    return datetime.now(ZONE).strftime(DATE_FORMAT)
def get_user():
    return 'leganto-uio-example-user@example.com'


def import_organisation(organisation, working_dir):
    with open("arkivstruktur.xml", "w") as as_file:
        create_arkivstruktur_with_basic_content(as_file)
        file = JsonFile(organisation, working_dir, PROCESS)
        citations_dir, citation_files = file.get_list_citation_files()
        file_count = 1
        citations_count = 0
        for citation_file in citation_files:
            with open(os.path.join(citations_dir, citation_file)) as current_file:
                citations = json.load(current_file)
                as_file.write('\t\t<mappe>\n')
                as_file.write('\t\t\t<systemID>' + str(uuid.uuid4()) + '</systemID>\n')
                as_file.write('\t\t\t<mappeID>' + escape(citations['name']) + '</mappeID>\n')
                as_file.write('\t\t\t<tittel>' + escape(citations['name']) + '</tittel>\n')
                as_file.write('\t\t\t<opprettetDato>' + get_datetime() +'</opprettetDato>\n')
                as_file.write('\t\t\t<opprettetAv>' + urllib.parse.quote(get_user(), safe=':/?&=') + '</opprettetAv>\n')
                as_file.write('\t\t\t<avsluttetDato>' + get_datetime() + '</avsluttetDato>\n')
                as_file.write('\t\t\t<avsluttetAv>REDACTED</avsluttetAv>\n')
                as_file.write('\t\t\t<virksomhetsspesifikkeMetadata>\n')
                as_file.write('\t\t\t\t<leganto-v1:course>\n')
                as_file.write(get_noark_bsm_values_course(citations))
                as_file.write('\t\t\t\t</leganto-v1:course>\n')
                as_file.write('\t\t\t</virksomhetsspesifikkeMetadata>\n')

                if 'citation' in citations['citations'].keys():
                    for citation in citations['citations']['citation']:
                        as_file.write('\t\t\t<registrering>\n')
                        as_file.write('\t\t\t<systemID>' + str(uuid.uuid4()) + '</systemID>\n')
                        as_file.write('\t\t\t\t<opprettetDato>' + get_datetime() + '</opprettetDato>\n')
                        as_file.write('\t\t\t\t<opprettetAv>' + urllib.parse.quote(get_user(), safe=':/?&=') + '</opprettetAv>\n')
                        as_file.write('\t\t\t\t<arkivertDato>' + get_datetime() + '</arkivertDato>\n')
                        as_file.write('\t\t\t\t<arkivertAv>' + urllib.parse.quote(get_user(), safe=':/?&=') + '</arkivertAv>\n')
                        as_file.write('\t\t\t\t<tittel>MANGLER - usikker</tittel>\n')
                        as_file.write('\t\t\t\t<virksomhetsspesifikkeMetadata>\n')
                        as_file.write('\t\t\t\t\t<leganto-v1:citation>\n')
                        citation[NAME] = citations[CONSPECTUS_COURSE_NAME]
                        as_file.write(get_noark_bsm_values_citation(citation, citations))
                        citations_count += 1
                        as_file.write('\t\t\t\t\t</leganto-v1:citation>\n')
                        as_file.write('\t\t\t\t</virksomhetsspesifikkeMetadata>\n')
                        as_file.write('\t\t\t</registrering>\n')
                file_count += 1
                as_file.write('\t\t</mappe>\n')
            current_file.close()
        close_arkivstruktur(as_file)

    logging.info(PROCESS_REPORT.format(str(citations_count), str(file_count), organisation))

def create_arkivstruktur_with_basic_content(as_file):
    as_file.write('<?xml version="1.0" encoding="UTF-8"?>\n')
    as_file.write('<arkiv xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n')
    as_file.write('xmlns="http://www.arkivverket.no/standarder/noark5/arkivstruktur"\n')
    as_file.write('xmlns:n5mdk="http://www.arkivverket.no/standarder/noark5/metadatakatalog"\n')
    as_file.write('xmlns:leganto-v1="http://www.example.com/leganto"\n')
    as_file.write('xsi:schemaLocation="http://www.arkivverket.no/standarder/noark5/arkivstruktur arkivstruktur.xsd ')
    as_file.write('http://www.example.com/leganto leganto_noark.xsd">\n')
    as_file.write('\t<systemID>1690bff1-2c06-4571-9689-a4768ef64c9d</systemID>\n')
    as_file.write('\t<tittel>UiO Bibliotektjenester - pensum</tittel>\n')
    as_file.write('\t<opprettetDato>' + urllib.parse.quote(get_datetime(), safe=':/?&=') + '</opprettetDato>\n')
    as_file.write('\t<opprettetAv>' + urllib.parse.quote(get_user(), safe=':/?&=') + '</opprettetAv>\n')
    as_file.write('\t<avsluttetDato>' + urllib.parse.quote(get_datetime(), safe=':/?&=') + '</avsluttetDato>\n')
    as_file.write('\t<avsluttetAv>' + urllib.parse.quote(get_user(), safe=':/?&=') + '</avsluttetAv>\n')
    as_file.write('\t<arkivskaper>\n')
    as_file.write('\t\t<arkivskaperID>00000000000</arkivskaperID>\n')
    as_file.write('\t\t<arkivskaperNavn>Universitetet i Oslo</arkivskaperNavn>\n')
    as_file.write('\t</arkivskaper>\n')
    as_file.write('\t<arkivdel>\n')
    as_file.write('\t\t<systemID>cadf26f4-751a-4482-9f7e-4a3eb03ad441</systemID>\n')
    as_file.write('\t\t<tittel>Leganto pensum data</tittel>\n')
    as_file.write('\t\t<arkivdelstatus>Aktiv periode</arkivdelstatus>\n')
    as_file.write('\t\t<opprettetDato>' + urllib.parse.quote(get_datetime(), safe=':/?&=') + '</opprettetDato>\n')
    as_file.write('\t\t<opprettetAv>' + urllib.parse.quote(get_user(), safe=':/?&=') + '</opprettetAv>\n')
    as_file.write('\t\t<avsluttetDato>' + urllib.parse.quote(get_datetime(), safe=':/?&=') + '</avsluttetDato>\n')
    as_file.write('\t\t<avsluttetAv>' + urllib.parse.quote(get_user(), safe=':/?&=') + '</avsluttetAv>\n')


def close_arkivstruktur(as_file):
    as_file.write('\t</arkivdel>\n')
    as_file.write('</arkiv>\n')

def get_options_retrieve(argv):
    try:
        working_dir = 'leganto'
        organisation_to_process = ALL
        opts, args = getopt.getopt(argv, 'h:w:o:')
        for opt, arg in opts:
            if opt == '-h':
                logging.info('noark-import.py -w <working-dir> -o <organisation>')
                sys.exit()
            if opt in '-w':
                working_dir = arg
            if opt in '-o':
                organisation_to_process = arg
        return organisation_to_process, working_dir
    except getopt.GetoptError:
        logging.info('noark-import.py -w <working-dir> -o <organisation>')
        sys.exit(2)


def main(argv):
    organisation_to_process, working_dir = get_options_retrieve(argv)
    # We assume any directory in the working directory corresponds to a third level organisation
    if organisation_to_process == ALL:
        organisations = os.listdir(working_dir)
    else:
        organisations = [organisation_to_process]

    for organisation in organisations:
        import_organisation(organisation, working_dir)


def create_xsd_file():
    with open("leganto_noark.xsd", "w") as xsd_file:
        xsd_file.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        xsd_file.write('<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"\n')
        xsd_file.write('           xmlns:leganto-v1="http://www.example.com/leganto"\n')
        xsd_file.write('           targetNamespace="http://www.example.com/leganto"\n')
        xsd_file.write('           elementFormDefault="qualified">\n\n\n')
        xsd_file.write('\t<xs:element name="citation" type="leganto:CitationType"/>\n\n')
        xsd_file.write('\t<xs:complexType name="CitationType">\n')
        xsd_file.write('\t\t<xs:sequence>\n')
        xsd_file.write('\t\t\t<xs:element name="semester_1" type="xs:string" minOccurs="0"/>\n')
        xsd_file.write('\t\t\t<xs:element name="metadata_title" type="xs:string" minOccurs="0"/>\n')
        xsd_file.write('\t\t\t<xs:element name="metadata_author" type="xs:string" minOccurs="0"/>\n')
        xsd_file.write('\t\t\t<xs:element name="metadata_publisher" type="xs:string" minOccurs="0"/>\n')
        xsd_file.write('\t\t\t<xs:element name="metadata_publication_date" type="xs:string" minOccurs="0"/>\n')
        xsd_file.write('\t\t\t<xs:element name="metadata_isbn" type="xs:string" minOccurs="0"/>\n')
        xsd_file.write('\t\t\t<xs:element name="metadata_mms_id" type="xs:string" minOccurs="0"/>\n')
        xsd_file.write('\t\t\t<xs:element name="metadata_place_of_publication" type="xs:string" minOccurs="0"/>\n')
        xsd_file.write('\t\t\t<xs:element name="metadata_year" type="xs:string" minOccurs="0"/>\n')
        xsd_file.write('\t\t\t <xs:element name="metadata_pages" type="xs:string" minOccurs="0"/>\n')
        xsd_file.write('\t\t</xs:sequence>\n')
        xsd_file.write('\t</xs:complexType>\n')
        xsd_file.write('\t</xs:schema>\n')

if __name__ == '__main__':
    main(sys.argv[1:])
    sys.exit(0)
